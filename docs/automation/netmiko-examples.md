
# Netmiko 
La automatización de redes con Netmiko ofrece una solución eficaz para la gestión y configuración de dispositivos intermedios de red a través de Python. Estos scripts ilustran cómo utilizar Netmiko para realizar tareas como la configuración de direcciones IP, la creación de VLANs y la visualización de configuraciones en múltiples dispositivos de manera simultánea. 

## Scripts
A continuación se listan scripts de ejemplo para gestionar un router Mikrotik con Python Netmiko. 

### Aprovisionamiento y visualización básica de un dispositivo
Este script se conecta a un dispositivo de red y realiza operaciones básicas de configuración y visualización utilizando la biblioteca Netmiko de Python. Primero, configura una dirección IP en una interfaz `ether2` y luego añade dos VLANs (vlan10 y vlan20) a la misma interfaz. Finalmente, imprime la configuración establecida y cierra la conexión con el dispositivo.


=== "Router (Cisco) - Ejemplo"

    ```python title="netmiko_cisco_1.py"
    from netmiko import ConnectHandler

    router_cisco = {
        'device_type': 'cisco_ios',
        'host':   '10.0.0.56',
        'username': 'admin',
        'password': 'admin',
        'port': 22,            # optional, defaults to 22
        'secret': '',          # optional, defaults to ''
    }

    conexion = ConnectHandler(**router_cisco)

    # Definir comandos a ejecutar
    configurar = [
        'interface Ethernet0/1',
        'ip address 192.168.123.1 255.255.255.0',
        'no shutdown',
        'exit',
        'interface Ethernet0/1.10',
        'encapsulation dot1Q 10',
        'ip address 192.168.10.1 255.255.255.0',
        'exit',
        'interface Ethernet0/1.20',
        'encapsulation dot1Q 20',
        'ip address 192.168.20.1 255.255.255.0',
        'exit',
    ]

    # Ejecutar comandos (send_config_set - para enviar comandos de configuración)
    accion1 = conexion.send_config_set(configurar)
    print(accion1)

    # Cerrar la conexión
    conexion.disconnect()

    ```
=== "Router (Mikrotik) - Ejemplo"

    ```python title="netmiko_mikrotik_1.py"
    from netmiko import ConnectHandler

    router_mikrotik = {
        'device_type': 'mikrotik_routeros',
        'host':   '10.0.0.50',
        'username': 'admin',
        'password': 'admin',
        'port' : 22,            # optional, defaults to 22
        'secret': '',           # optional, defaults to ''
    }

    conexion = ConnectHandler(**router_mikrotik)

    # Definir comandos a ejecutar
    configurar = [
        '/ip address add address=192.168.123.1/24 interface=ether2',
        '/interface vlan add name=vlan10 vlan-id=10 interface=ether2',
        '/interface vlan add name=vlan20 vlan-id=20 interface=ether2',
    ]

    # Ejecutar comandos (send_config_set - para enviar comandos de configuración)
    accion1 = conexion.send_config_set(configurar)
    print(accion1)

    # Visualizar comandos (send_command - para enviar comandos de visualización)
    accion2 = conexion.send_command('/ip address print')
    print(accion2)

    # Cerrar la conexión
    conexion.disconnect()

    ```

### Aprovisionamiento y visualización detallada de un dispositivo
En este script, se conecta a un dispositivo MikroTik utilizando Netmiko. Realiza las mismas operaciones de configuración inicial que el script anterior: asignación de dirección IP y configuración de VLANs en la interfaz `ether2`. Además de eso, este script también realiza operaciones de visualización más detalladas. Imprime las configuraciones de IP, interfaces y rutas usando los comandos `/ip address print`, `/interface print` y `/ip route print`, respectivamente. Cada salida de comando se muestra en la consola antes de cerrar la conexión con el dispositivo.

```python title="netmiko_mikrotik_2.py"

from netmiko import ConnectHandler

router_mikrotik = {
    'device_type': 'mikrotik_routeros',
    'host':   '10.0.0.50',
    'username': 'admin',
    'password': 'admin',
    'port' : 22,            # optional, defaults to 22
    'secret': '',           # optional, defaults to ''
}

conexion = ConnectHandler(**router_mikrotik)

# Definir comandos a ejecutar
configurar = [
    '/ip address add address=192.168.123.1/24 interface=ether2',
    '/interface vlan add name=vlan10 vlan-id=10 interface=ether2',
    '/interface vlan add name=vlan20 vlan-id=20 interface=ether2',
]

visualizar = [
    '/ip address print',
    '/interface print',
    '/ip route print',
]

# Ejecutar comandos (send_config_set - para enviar comandos de configuración)
accion1 = conexion.send_config_set(configurar)
print(accion1)

# Ejecutar comandos de visualización (send_command - para enviar comandos de visualización)
for command in visualizar:
    accion2 = conexion.send_command(command)
    print(f"Salida del comando: '{command}':\n{accion2}\n")

# Cerrar la conexión
conexion.disconnect()

```

### Aprovisionamiento de múltiples dispositivos con configuraciones compartidas
Este script está diseñado para automatizar la configuración en múltiples dispositivos MikroTik simultáneamente. 

- Cada dispositivo tiene un diccionario con los detalles de conexión y una dirección IP única (`ip_address`) que se asignará a `ether2`.
- `configurar_comun` contiene comandos que se aplicarán a ambos routers, excepto la configuración de la dirección IP.
- Función `configure_device`:
    - Establece la conexión SSH al dispositivo.
    - Combina comandos específicos (dirección IP) y comunes, y los envía al dispositivo.
    - Ejecuta comandos de visualización para verificar la configuración.
    - Maneja excepciones para capturar y reportar errores.
    - Iterando sobre la lista de dispositivos se aplica la configuración a cada uno llamando a `configure_device`.

```python title="netmiko_mikrotik_3.py"

from netmiko import ConnectHandler

# Definir los detalles de conexión para ambos dispositivos Mikrotik
router_mikrotik = [
    {
        'device_type': 'mikrotik_routeros',
        'host': '10.0.0.50',  # IP del primer dispositivo Mikrotik
        'username': 'admin',  # Usuario del primer dispositivo Mikrotik
        'password': 'admin',  # Contraseña del primer dispositivo Mikrotik
        'port': 22,           # Puerto SSH, 22 es el predeterminado
        'secret': '',         # Si tienes una contraseña de enable/secret, la puedes agregar aquí
        'ip_address': '192.168.123.1/24'  # Dirección IP específica para este router
    },
    {
        'device_type': 'mikrotik_routeros',
        'host': '10.0.0.55',  # IP del segundo dispositivo Mikrotik
        'username': 'admin',  # Usuario del segundo dispositivo Mikrotik
        'password': 'admin',  # Contraseña del segundo dispositivo Mikrotik
        'port': 22,           # Puerto SSH, 22 es el predeterminado
        'secret': '',         # Si tienes una contraseña de enable/secret, la puedes agregar aquí
        'ip_address': '192.168.124.1/24'  # Dirección IP específica para este router
    }
]

# Comandos de configuración comunes (sin incluir la dirección IP)
configurar_comun = [
    '/interface vlan add name=vlan10 vlan-id=10 interface=ether2',
    '/interface vlan add name=vlan20 vlan-id=20 interface=ether2',
]

# Comandos de visualización
visualizar = [
    '/ip address print',
    '/interface print',
    '/ip route print',
]

# Función para aplicar configuraciones a un dispositivo
def configure_device(dispositivo):
    # Extraer la dirección IP específica del dispositivo
    ip_address = dispositivo.pop('ip_address')

    try:
        # Establecer la conexión
        conexion = ConnectHandler(**dispositivo)

        # Agregar comando de configuración específico (dirección IP)
        configurar_especifico = [f'/ip address add address={ip_address} interface=ether2']

        # Ejecutar los comandos de configuración (específico + común)
        salida_configurar = conexion.send_config_set(configurar_especifico + configurar_comun)
        print(f"Salida de {dispositivo['host']}:\n{salida_configurar}")

        # Ejecutar comandos de visualización
        for command in visualizar:
            salida_visualizar = conexion.send_command(command)
            print(f"Salida de {dispositivo['host']} para '{command}':\n{salida_visualizar}\n")

        # Cerrar la conexión
        conexion.disconnect()
    except Exception as e:
        print(f"Error al configurar el dispositivo {dispositivo['host']}: {e}")

# Aplicar configuraciones a cada dispositivo
for dispositivo in router_mikrotik:
    # Crear una copia del diccionario para no modificar el original
    dispositivo_copy = dispositivo.copy()
    configure_device(dispositivo_copy)

```

### Aprovisionamiento de múltiples dispositivos con configuraciones diferentes
Este script es más flexible que el anterior y puede manejar configuraciones variadas para diferentes routers Mikrotik, aplicando configuraciones únicas de VLANs y direccionamiento IP a cada dispositivo.

- Cada entrada ahora tiene una clave `config` que contiene configuraciones específicas para cada router, incluyendo la dirección IP (`ip_address`), la interfaz (`interface`) y una lista de VLANs (`vlans`).
- Dentro de la función `configure_device`, se extraen las configuraciones específicas y se generan los comandos necesarios para configurar la dirección IP y las VLANs en la interfaz designada.
- Se recorre la lista de VLANs y se agregan los comandos necesarios para crear cada VLAN en la interfaz especificada.
- Se captura cualquier excepción que pueda ocurrir durante la conexión o configuración y se muestra un mensaje de error específico para el dispositivo afectado.


```python title="netmiko_mikrotik_4.py"

from netmiko import ConnectHandler

# Definir los detalles de conexión y configuración específica para cada dispositivo Mikrotik
router_mikrotik = [
    {
        'device_type': 'mikrotik_routeros',
        'host': '10.0.0.50',  # IP del primer dispositivo Mikrotik
        'username': 'admin',  # Usuario del primer dispositivo Mikrotik
        'password': 'admin',  # Contraseña del primer dispositivo Mikrotik
        'port': 22,           # Puerto SSH, 22 es el predeterminado
        'secret': '',         # Si tienes una contraseña de enable/secret, la puedes agregar aquí
        'config': {
            'ip_address': '192.168.123.1/24',  # Dirección IP específica
            'interface': 'ether2',             # Interfaz específica
            'vlans': [
                {'name': 'vlan10', 'vlan_id': 10},
                {'name': 'vlan20', 'vlan_id': 20}
            ]
        }
    },
    {
        'device_type': 'mikrotik_routeros',
        'host': '10.0.0.55',  # IP del segundo dispositivo Mikrotik
        'username': 'admin',  # Usuario del segundo dispositivo Mikrotik
        'password': 'admin',  # Contraseña del segundo dispositivo Mikrotik
        'port': 22,           # Puerto SSH, 22 es el predeterminado
        'secret': '',         # Si tienes una contraseña de enable/secret, la puedes agregar aquí
        'config': {
            'ip_address': '192.168.124.1/24',  # Dirección IP específica
            'interface': 'ether3',             # Interfaz específica
            'vlans': [
                {'name': 'vlan30', 'vlan_id': 30},
                {'name': 'vlan40', 'vlan_id': 40}
            ]
        }
    }
]

# Comandos de visualización
visualizar = [
    '/ip address print',
    '/interface print',
    '/ip route print',
]

# Función para aplicar configuraciones a un dispositivo
def configure_device(dispositivo):
    config = dispositivo.pop('config')
    ip_address = config['ip_address']
    interface = config['interface']
    vlans = config['vlans']

    try:
        # Establecer la conexión
        conexion = ConnectHandler(**dispositivo)

        # Comandos específicos de configuración (dirección IP e interfaces VLAN)
        configurar_especifico = [f'/ip address add address={ip_address} interface={interface}']
        for vlan in vlans:
            configurar_especifico.append(f'/interface vlan add name={vlan["name"]} vlan-id={vlan["vlan_id"]} interface={interface}')

        # Ejecutar los comandos de configuración específicos
        salida_configurar = conexion.send_config_set(configurar_especifico)
        print(f"Salida de {dispositivo['host']}:\n{salida_configurar}")

        # Ejecutar comandos de visualización
        for command in visualizar:
            salida_visualizar = conexion.send_command(command)
            print(f"Salida de {dispositivo['host']} para '{command}':\n{salida_visualizar}\n")

        # Cerrar la conexión
        conexion.disconnect()
    except Exception as e:
        print(f"Error al configurar el dispositivo {dispositivo['host']}: {e}")

# Aplicar configuraciones a cada dispositivo
for dispositivo in router_mikrotik:
    # Crear una copia del diccionario para no modificar el original
    dispositivo_copy = dispositivo.copy()
    configure_device(dispositivo_copy)


```