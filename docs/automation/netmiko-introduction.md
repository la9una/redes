[Netmiko](https://github.com/ktbyers/netmiko) emerge como una biblioteca de Python robusta y versátil, diseñada para simplificar la automatización de tareas en dispositivos de red. Su facilidad de uso, junto a su amplia gama de funcionalidades, la convierten en una herramienta invaluable para administradores de redes que buscan optimizar la gestión de sus infraestructuras.

<div style="margin:0 auto; text-align:center; font-size: 0.7rem" >
<img src="../imgAutomation/netmiko_logo.png" alt="Python Netmiko" style="width: 80%; height: auto;">
<figcaption>Logo de Netmiko</figcaption>
</div>

## Orígenes
[Netmiko](https://github.com/ktbyers/netmiko) surge como una evolución de la biblioteca [Paramiko](https://www.paramiko.org/), extendiendo sus capacidades para abarcar la interacción con dispositivos de red de manera más completa. Su creador, Kirk McElhinney, un reconocido experto en redes y automatización, la desarrolló con el objetivo de brindar a los administradores de redes una herramienta intuitiva y poderosa para automatizar tareas repetitivas y complejas.


## Alcance
Netmiko se destaca por su compatibilidad con una amplia gama de dispositivos Cisco, incluyendo routers, switches y firewalls. Además, soporta dispositivos MikroTik, expandiendo su alcance a entornos diversos. Entre sus funcionalidades clave se encuentran:

- **Conexión a dispositivos de red**: Establece conexiones seguras a través de SSH, Telnet y NX-OS.
- **Ejecución de comandos**: Envía y ejecuta comandos CLI en dispositivos de red, obteniendo resultados y procesando la información extraída.
- **Búsqueda de patrones**: Permite identificar patrones específicos dentro de los resultados de los comandos, facilitando el análisis y la extracción de datos relevantes.
- **Transferencia de archivos**: Sube y descarga archivos a dispositivos de red, agilizando la gestión de configuraciones y actualizaciones de software.
- **Operaciones con VLANs**: Crea, elimina y modifica VLANs en switches Cisco, simplificando la administración de redes segmentadas.
- **Gestión de interfaces**: Configura y administra interfaces de red, incluyendo la asignación de direcciones IP, la configuración de VLANs y la activación/desactivación de interfaces.

## Primeros pasos

Para comenzar a explorar el potencial de Netmiko, se recomienda seguir estos pasos:

- **Instalación**: Instalar Netmiko utilizando el comando `pip install netmiko` en una terminal o entorno de Python.
- **Importación**: Importar la biblioteca Netmiko en los scripts de Python utilizando la instrucción `import netmiko`.
- **Conexión a un dispositivo**: Establecer una conexión a un dispositivo de red mediante la clase `ConnectHandler` de Netmiko, especificando la dirección IP, el nombre de usuario y la contraseña del dispositivo.
- **Ejecución de comandos**: Enviar comandos CLI al dispositivo conectado utilizando el método `send_command()` de la clase `ConnectHandler`.
- **Análisis de resultados**: Procesar los resultados obtenidos de los comandos utilizando expresiones regulares o técnicas de análisis de texto.