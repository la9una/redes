Si bien existen múltiples proveedores de hardware de red, es esta sección abordaremos la gestión de dispositivos de la marca [Cisco Systems](https://www.cisco.com/c/es_ar/index.html), debido a su trayectoria y uso extendido en el sector.	

<div style="margin:0 auto; text-align:center;" >
<img src="../imgCisco/cisco-logo.png" alt="Logo CISCO" style="width: 50%;
  height: auto;">
  <figcaption>Logo de CISCO</figcaption>
</div>

Cisco Systems tiene productos para routing (redes), seguridad, colaboración(telefonía IP y sistemas de videoconferencia), data center, cloud y movilidad (wireless). La mayoría de sus productos se basan en hardware; no obstante, durante los últimos años la empresa está rotando sus productos hacia el software:

* Dispositivos de conexión para redes informáticas: routers (enrutadores, encaminadores o ruteadores), switches (conmutadores) y hubs (concentradores).
* Dispositivos de seguridad como cortafuegos y concentradores para VPN.
* Productos de telefonía IP como teléfonos y el CallManager (una PBX IP).
* Software de gestión de red como CiscoWorks.
* Equipos para redes de área de almacenamiento, entre otros.