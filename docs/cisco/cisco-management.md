# La CLI en Cisco IOS

El uso de la interfaz de línea de comandos (CLI _Command Line Interface_) facilita y agiliza las tareas de gestión, mantenimiento y monitoreo de los dispositivos de red.  

La gran mayoría de dispositivos Cisco emplean el sistema [Cisco IOS](https://es.wikipedia.org/wiki/Cisco_IOS). La gestión de dicho sistema se realiza mediante CLI, la cual posee una estructura jerárquica, con diversos modos. Cada uno de estos modos, está relacionado a un conjunto de tareas determinadas. 

## Modos de CLI en dispositivos CISCO
Antes de comenzar a trabajar con comandos de terminal, es preciso reconocer 3 modos de usuario: 

![Cisco User Modes](imgCisco/cisco-user-access-modes.png)

1. EXEC Usuario
2. EXEC Privilegiado
3. Configuración Global
    * _Sub-modos_


| Modo de operación    | ON            | OFF         |
| -------------------- | ------------------ | -------------- |
| EXEC Usuario         | Por defecto        | `logout`       |
| EXEC Privilegiado    | `enable`           | `disable`      |
| Configuración Global | `configure terminal` | `end` ó <kbd>CTRL</kbd> + <kbd>Z</kbd> |
| Sub-Modo (Configuración Global) | `interface <nombre-interfaz>`<br />`vlan <nombre-vlan>`<br />`line <nombre-line>`<br />etc | `exit` |

### 1. EXEC Usuario
Es un modo de visualización o sólo lectura y es el primer modo que se encuentra al entrar a la CLI de un dispositivo IOS. Por defecto, no requiere autenticación.

Este modo se distingue por el aspecto del [prompt](https://es.wikipedia.org/wiki/Prompt), que terminal con el símbolo `>`. Por ejemplo: 

```bash
Switch>
``` 

Para salir de este modo, ejecutamos: 

```bash
Switch> logout
``` 

### 2. EXEC Privilegiado
A través de este modo podemos ver y cambiar la configuración del dispositivo. Por tal motivo, es recomendable proteger este modo con una constraseña de administrador. 

Para acceder a este modo desde el modo **EXEC Usuario**, simplemente ejecutamos: 

```bash
Switch> enable
``` 

O bien, de forma abreviada:

```bash
Switch> en
``` 

Ingresaremos entonces al modo **EXEC Privilegiado**. Habrá cambiado el prompt desde `>` a `#`:

```bash
Switch#
``` 

Para volver al modo anterior (**EXEC Usuario**), simplemente ejecutamos: 

```bash
Switch# disable
``` 
O en su forma corta: 

```bash
Switch# disa
``` 

### 3. Configuración Global

Podremos acceder a este modo sólo desde el modo de **EXEC Privilegiado**, escribiendo: 

```bash
Switch# configure terminal
``` 

O bien, en su forma abreviada: 

```bash
Switch# config t
```

Al ingresar a este modo, veremos un cambio en el prompt: 

```bash
Switch(config)# 
```

A partir de este momento podremos realizar diferentes modificaciones en la configuración del dispositivo. 

Para volver al modo **EXEC Privilegiado** ejecutamos: 

```bash
Switch(config)# end
```

O bien, la combinación de las teclas ++ctrl+z++

#### Sub-modos
Dentro del modo **Configuración Global**, existen diferentes sub-modos de configuración. Cada uno de estos modos permite la configuración de una parte o función específica del dispositivo IOS. 

Algunos de estos modos:

* **Modo de interfaz (interface)**: permite configurar una de las interfaces de red Fa (Fast Ethernet), S (Serial), Gi (Gigabit Ethernet), etc. 

* **Modo de línea (line)**: para configurar una de las líneas físicas o virtuales (consola, auxiliar, ssh, telnet, etc).

* **Modo Virtual Lan (vlan)**: para configurar las redes virtuales o VLANs. 

* etc

Para salir de cualquiera de estos sub-modos y volver al modo de **Configuración Global**, ejecutamos: 

```bash
Switch(config-if)# exit
```

Si en cambio deseamos pasar directamente al modo **EXEC Privilegiado**, ejecutamos: 

```bash
Switch(config)# end
```

O bien, la combinación de las teclas ++ctrl+z++


## Ayuda de la CLI
En cualquier modo de usuario podemos invocar el manual de ayuda introduciendo el caracter de signo de pregunta (cierre) `?`. Éste nos desplegará una lista de comandos y opciones. 

Por ejemplo, para visualizar los comandos disponibles en el modo usuario: 

```bash
Switch> ?
``` 
Para ver las opciones del comando `ping`: 

```bash
Switch> ping ?
```

En cualquier caso nos desplazaremos por los elementos listados, presionando la tecla ++enter++ o la tecla ++space++

Finalmente, si comenzamos a escribir un comando y presionamos la tecla ++tab++ el sistema lo autocompletará. 