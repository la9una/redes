# Gestión de Routers Cisco
En esta sección abordaremos procedimientos básicos para la gestión de Routers basados en Cisco IOS, de la marca Cisco. 

## Acceso remoto (SSH)
El acceso remoto mediante SSH (Secure Shell) en routers Cisco es fundamental para la administración segura de redes. SSH permite la comunicación encriptada, protegiendo información sensible y previniendo accesos no autorizados. Configurar SSH en routers Cisco facilita a los administradores conectarse y gestionar la red de forma remota y segura. Este proceso incluye la generación de claves criptográficas, la creación de usuarios y la implementación de políticas de seguridad. A continuación, se describen los pasos esenciales para habilitar y configurar SSH en routers Cisco, destacando su importancia en la protección y eficiencia de la red.

```bash
# Ingresamos al modo "EXEC privilegiado"
Router> enable

# Ingresamos al modo "CONFIGURACIÓN GLOBAL"
Router# configure terminal

# Establecemos un nombre de dominio
Router(config)# ip domain-name <nombre_de_dominio>

# Cambiamos el nombre de host del router
Router(config)# hostname <nombre_del_router>

# Usamos la versión 2 del protocolo SSH
Router(config)# ip ssh version 2

# Generamos claves RSA de 1024 bits
Router(config)# crypto key generate rsa modulus 1024

# Creamos un usuario (y contraseña) con el privilegio más alto (15)
Router(config)# username <user> privilege 15 secret <password>

# Habilitamos todas las VTY (líneas virtuales de tipo terminal) del 0 al 4 para usar SSH.
Router(config)# line vty 0 4

# Indicamos que las VTY aceptarán conexiones SSH solamente
Router(config)# transport input ssh

# Aclaramos que la autenticación se realizará usando las cuentas locales configuradas en el router 
Router(config)# login local

# Salimos del modo de configuración global
Router(config)# exit

# Verificamos que el acceso SSH esté configurado
Router# show ip ssh
```

## Esquema de red (ejemplo)

Para una mejor comprensión, los ejemplos de la presente sección están basados en el siguiente esquema de red:

<div style="margin:0 auto; text-align:center;" >
<img src="../imgCisco/net_example_cisco.png" alt="Esquema de red de ejemplo">
  <figcaption>Esquema de red ejemplo</figcaption>
</div>

## Direccionamiento estático 
Establecemos IP estática en la interfaz `Ethernet 0/2` del _Router 1_. Procedemos:  

```bash
# Ingresamos al modo "EXEC privilegiado"
Router> enable

# Listamos las interfaces presentes en el dispositivo
Router# show ip interface brief

# Ingresamos al modo "CONFIGURACIÓN GLOBAL"
Router# configure terminal

# Seleccionamos la interfaz del router - Submodo "inteface"
Router(config)# interface e0/2

# Seteamos direccionamiento IP de la interfaz
# Sintaxis: ip address <dirección-ip> <máscara-red-decimal>
Router(config-if)# ip address 10.0.1.1 255.255.255.0

# "Levantamos" la interfaz
Router(config-if)# no shutdown

# Salimos del submodo "interface"
Router(config-if)# end

# Verificamos el direccionamiento de la interfaz
Router# show ip interface brief

# Guardamos los cambios
Router# copy running-config startup-config

# Salimos del modo "EXEC privilegiado"
Router# disable
```

!!!tip ""
    Para borrar el direccionamiento IP de una interfaz, sea éste estático o dinámico, ejecutamos:  
    
    ```bash
    Router(config-if)# no ip address
    ```

## Direccionamiento dinámico
Las asignación de direcciones IP pueden obtenerse de manera dinámica como cliente DHCP o ofrecerse a través de un servidor DHCP. 

### Cliente DHCP
En la interfaz `Ethernet 0/0` del _Router 1_, obtenemos IP a partir del servidor DHCP presente en la nube (Internet): 

```bash
# Ingresamos al modo "EXEC privilegiado"
Router> enable

# Listamos las interfaces presentes en el dispositivo
Router# show ip interface brief

# Ingresamos al modo "CONFIGURACIÓN GLOBAL"
Router# configure terminal

# Seleccionamos la interfaz del router - Submodo "interface"
Router(config)# interface e0/0

# Seteamos direccionamiento IP dinámico
Router(config-if)# ip address dhcp

# "Levantamos" la interfaz
Router(config-if)# no shutdown

# Salimos del submodo "interface"
Router(config-if)# end

# Verificamos el direccionamiento de la interfaz
Router# show ip interface brief

# Guardamos los cambios
Router# copy running-config startup-config

# Salimos del modo "EXEC privilegiado"
Router# disable
```

### Servidor DHCP
Para configurar un servidor DHCP en la interfaz `Ethernet 0/2` del _Router 2_ tendremos que:

* Establecer el [direccionamiento de la interfaz de manera estática](#direccionamiento-estatico) y 
* Crear un pool DHCP (rango de direcciones IP utilizab.es), que tenga como gateway, la dirección IP estática de la interfaz.  


Sintaxis general: 

```bash
# Ingresamos al modo "EXEC privilegiado"
Router> enable

# Ingresamos al modo "CONFIGURACIÓN GLOBAL"
Router# configure terminal

# Seleccionamos la interfaz del router 
Router(config)# interface <interfaz-fisica>

# Seteamos direccionamiento IP de la interfaz
Router(config-if)# ip address <dirección-ip> <máscara-red-decimal>

# "Levantamos" la interfaz
Router(config-if)# no shutdown

# Creamos el pool de direcciones IP 
Router(config)# ip dhcp pool <NOMBRE-POOL>

# Indicamos cuál será el gateway  
Router(dhcp-config)# default-router <dirección-ip-gateway>

# Indicamos el ID de red y máscara  
Router(dhcp-config)# network <id-red> <mascara-red>

# Excluimos direcciones IP del pool (opcional) 
Router(dhcp-config)# ip dhcp excluded-address <ip-a-exluir-1> <ip-a-exluir-2> <...>

# Definimos el servidor DNS 
Router(dhcp-config)# dns-server <ip-dns-1> <ip-dns-2>

# Salimos del submodo "ip dhcp"
Router(dhcp-config)# end

# Guardamos los cambios
Router# copy running-config startup-config

# Salimos del modo "EXEC privilegiado"
Router# disable
```

Por ejemplo:

```bash
# Ingresamos al modo "EXEC privilegiado"
Router> enable

# Ingresamos al modo "CONFIGURACIÓN GLOBAL"
Router# configure terminal

# Seleccionamos la interfaz del router 
Router(config)# interface Ethernet 0/0

# Seteamos direccionamiento IP de la interfaz
Router(config-if)# ip address 192.168.1.1 255.255.255.0

# "Levantamos" la interfaz
Router(config-if)# no shutdown

# Creamos el pool de direcciones IP 
Router(config)# ip dhcp pool MI-POOL-DHCP

# Indicamos cuál será el gateway  
Router(dhcp-config)# default-router 192.168.1.1

# Indicamos el ID de red y máscara  
Router(dhcp-config)# network 192.168.1.0 255.255.255.0

# Excluimos direcciones IP del pool (opcional) 
Router(dhcp-config)# ip dhcp excluded-address 192.168.1.100 192.168.1.200

# Definimos el servidor DNS 
Router(dhcp-config)# dns-server 8.8.8.8 8.8.4.4

# Salimos del submodo "ip dhcp"
Router(dhcp-config)# end

# Guardamos los cambios
Router# copy running-config startup-config

# Salimos del modo "EXEC privilegiado"
Router# disable
```

## NAT
Para enmascarar las direcciones privadas, en esta guía emplearemos **PAT**, ya que es el método de nateo más empleado por lejos, por encima de NAT estático y NAT dinámico.

Para realizar este tipo de nateo, tenemos que realizar estos pasos: 

* Definir las interfaces interna (LAN) y externa (hacia la WAN) 
* Crear una ACL que contenga la red LAN a la cual se desea permitir el acceso a internet  
* Autorizar el cursado de tráfico desde la interfaz interna hacia la interfaz externa 

En este ejemplo, definimos en el _Router 1_ su interfaz externa `Ethernet 0/0` e interfaz interna `Ethernet 0/2`:

```bash
# Ingresamos al modo "EXEC privilegiado"
Router> enable

# Ingresamos al modo "CONFIGURACIÓN GLOBAL"
Router# configure terminal

# Seleccionamos la interfaz LAN
Router(config)# interface e0/2

# Marcamos la interfaz como interna
Router(config-if)# ip nat inside

# Salimos del submodo "interface"
Router(config-if)# end

# Seleccionamos la interfaz con salida hacia la WAN
Router(config)# interface e0/0

# Marcamos la interfaz como externa
Router(config-if)# ip nat outside

# Salimos del submodo "interface"
Router(config-if)# end

```
Por último, creamos una ACL que incluya a la _RED A_ y autorizamos la salida de paquetes desde ésta hacia la interfaz externa.

```bash
# Ingresamos al modo "CONFIGURACIÓN GLOBAL"
Router# configure terminal

# Creamos una ACL para permitir salida a internet
# Sintaxis
# ACL numerada: access-list <#> <permit | deny> <network-id> <wildcaard-mask>
Router(config)# access-list 1 permit 10.0.1.0 0.0.0.255

# Nateamos (PAT) ACL hacia WAN
# Sintaxis: 
# ip nat inside source list <#-acl> interface <nombre-interface #/#> overload
Router(config)# ip nat inside source list 1 interface e0/0 overload

# Salimos del submodo "interface"
Router(config)# end
```

Opcionalmente, podemos verificar las IP que son nateadas, ejecutando el siguiente comando: 

```bash
Router# ip nat translations
```

## Enrutamiento

La sintaxis del comando es: 

```bash
ip route <red-destino> <máscara-red-destino> <ip-gateway>
```

En el presente ejemplo, establecemos una ruta estática para permitir la comunicación entre las  _PC 1_ y la _PC 2_ (bidireccional). Entonces, para establecer la ruta estática desde la _RED A_ hacia la _RED C_:

```bash
Router(config)# ip route 10.0.3.0 255.255.255.0 10.0.2.2
```

Del mismo modo, creamos una ruta estática desde la _RED C_ hacia la _RED A_:

```bash
Router(config)# ip route 10.0.1.0 255.255.255.0 10.0.2.1
```