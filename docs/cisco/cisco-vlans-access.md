# [VLAN de acceso (access)](../../networking/network-vlans/#vlan-de-acceso-access-vlan-untagged-ports)
Este tipo de VLAN se usa para conectar dispositivos finales a la red y segmentar el tráfico de la red en grupos lógicos. Dentro de este tipo de VLAN podemos encontrar: 

- [VLAN de Datos](../../networking/network-vlans/#vlan-de-datos-data-vlan)
- [VLAN de Voz](../../networking/network-vlans/#vlan-de-voz-voice-vlan)
- [VLAN Administrativa](../../networking/network-vlans/#vlan-administrativa-management-vlan)


## Creación de una VLAN de datos
Para crear VLANs deberemos ejecutar secuencialmente los siguientes comandos: 


=== "Sintaxis general"

    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan <id-de-la-vlan>
    Switch(config-vlan)# name <nombre-de-la-vlan> # Opcional
    Switch(config-vlan)# exit
    ```

=== "Ejemplo"

    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan 200
    Switch(config-vlan)# name Marketing
    Switch(config-vlan)# exit
    ```

## Asignación de una _VLAN de datos_ a una interfaz física del switch
Una vez creadas las VLANs tendremos que asociarlas a algún puerto del switch en particular. Es imporante destacar que esta permitido asignar sólo una vlan de datos (vlan de acceso por defecto) por cada puerto físico. 

=== "Sintaxis general"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface <puerto-fisico-del-switch>
    Switch(config-if)# switchport mode access 
    Switch(config-if)# switchport access vlan <vlan-id> 
    Switch(config-if)# exit 
    ```

=== "Ejemplo"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/1
    Switch(config-if)# switchport mode access 
    Switch(config-if)# switchport access vlan 100 
    Switch(config-if)# exit 
    ```

!!!warning "Cantidad de VLANs por puerto del switch"
    Por lo general, se asigna **una única VLAN de acceso a cada puerto físico del switch**. Sin embargo, hay una **excepción**: podemos configurar una [VLAN de datos](../../networking/network-vlans/#vlan-de-datos-data-vlan) y una [VLAN de voz](../../networking/network-vlans/#vlan-de-voz-voice-vlan) simultáneamente en un mismo puerto físico del switch.

## Creación de una VLAN de voz
El procedimiento para crear una VLAN de voz es el mismo que el empleado para [crear una VLAN de datos](#creacion-de-una-vlan-de-datos). 


## Asignación de una _VLAN de voz_ a una interfaz física del switch
Una vez creadas las VLANs de voz, tendremos que asociarlas a algún puerto del switch en particular. 


=== "Sintaxis general"

    ```apache hl_lines="5"
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface <puerto-fisico-del-switch>
    Switch(config-if)# switchport mode access
    Switch(config-if)# mls qos trust cos # Opcional
    Switch(config-if)# switchport voice vlan <vlan-id> 
    Switch(config-if)# exit 
    ```

=== "Ejemplo" 

    ```apache hl_lines="5"
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/1
    Switch(config-if)# switchport mode access 
    Switch(config-if)# mls qos trust cos # Opcional
    Switch(config-if)# switchport voice vlan 200 
    Switch(config-if)# exit 
    ```

???tip "Calidad de servicio (QoS)"
    En VLAN de voz es posible setear calidad de servicio (QoS - Quality of Service), mediante la instrucción:
    
    ```
    Switch(config-if)# mls qos trust cos
    ```

    Sin embargo, este característica no está disponible en todos los switches.



## Creación de una VLAN Administrativa 
El procedimiento para crear una VLAN Administrativa tiene dos etapas: 

- **Creación de la VLAN** en sí (siguiendo el mismo procedimiento que el empleado para [crear una VLAN de Datos](#creacion-de-una-vlan-de-datos)). 
- **Asignación de direccionamiento a la intefaz VLAN**, o en otras palabras, dotar de dirección IP a la interfaz VLAN (la cual funciona como una interfaz virtual que simula una interfaz física), junto a la máscara de subred y la puerta de enlace predeterminada o _gateway_.

### Direccionamiento estático
A continuación, configuraremos la dirección IP de la interfaz de manera estática, ateniéndonos a la siguiente sintaxis: 

=== "Sintaxis general"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface vlan <vlan-id>
    Switch(config-if)# ip address <direccion-ip> <mascara-decimal> 
    Switch(config-if)# no shutdown
    Switch(config-if)# exit
    Switch(config)# ip default-gateway <dirección-ip-gateway> # Opcional
    Switch(config)# end
    ```

=== "Ejemplo"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface vlan 100
    Switch(config-if)# ip address 192.168.100.10 255.255.255.0 
    Switch(config-if)# no shutdown
    Switch(config-if)# exit
    Switch(config)# ip default-gateway 192.168.100.1 # Opcional
    Switch(config)# end
    ```

### Direccionamiento dinámico
Para configurar la dirección IP de la interfaz de manera dinámica, tendremos que asegurarnos que existe un servidor DHCP en la red. La interfaz VLAN se comportará como cliente de dicho servidor, obteniendo de este modo, dirección IP de manera automática: 

=== "Sintaxis general"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface vlan <vlan-id>
    Switch(config-if)# ip address dhcp
    Switch(config-if)# no shutdown
    Switch(config-if)# end
    ```

=== "Ejemplo"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface vlan 100
    Switch(config-if)# ip address dhcp
    Switch(config-if)# no shutdown
    Switch(config-if)# end
    ```

???question "¿Direcciones IP en Capa 2?"
    **No es del todo preciso decirlo de esa manera. En realidad, lo que sucede es que se asigna direccionamiento IP (Capa 3) a una VLAN (Capa 2). Es decir, se trata a cada VLAN como si fuera una interfaz virtual, similar a una interfaz física en el switch.**. 

    Asignar una **dirección IP a la VLAN administrativa ofrece varias ventajas**:
    
    - Permite la administración remota del switch a través de la red IP, utilizando herramientas como Telnet, SSH o SNMP.
    - Simplifica la administración del switch al proporcionar una interfaz de red estándar para acceder a la configuración
    - Permite el monitoreo remoto del switch y la red mediante herramientas de administración de red.

    Sin embargo, el **direccionamiento de la interfaz VLAN presenta algunas limitaciones**:

    - La interfaz VLAN solo puede ser utilizada para tareas de administración del switch, no para el tráfico de datos regular.
    - Se requiere un router en la red para proporcionar conectividad IP y servicios de enrutamiento a la VLAN administrativa.

## Asignación de una _VLAN Administrativa_ a una interfaz física del switch

Si se utiliza un método de administración remoto que no requiere una conexión física al switch, como [SSH](https://es.wikipedia.org/wiki/Secure_Shell), [SNMP](https://es.wikipedia.org/wiki/Protocolo_simple_de_administraci%C3%B3n_de_red) o [Telnet](https://es.wikipedia.org/wiki/Telnet), entonces **no es necesario asociar una VLAN administrativa a un puerto físico**. 

!!!question "¿Se recomienda asociar una VLAN administrativa a un puerto físico del switch?"

    **No es estrictamente necesario asociar una VLAN administrativa a un puerto físico del switch, pero se recomienda hacerlo por varias razones**:

    - Mayor seguridad, al aislar el tráfico administración, mediante la creación de una red aislada para la gestión del switch.
    - Mejor control de acceso, permitiendo el acceso a la VLAN Administrativa desde puertos físicos específicos, lo que limita quién puede acceder a las opciones de administración y configuraciones del switch.
    - Simplificación de la configuración, al tener un puerto físico dedicado, la configuración de la VLAN administrativa se vuelve más sencilla y organizada. Y, por lo mismo, mayor facilidad de resolución de problemas. 

Para asignar una interfaz física del swtich a una VLAN Administrativa, se seguirá [el mismo procedimiento que el empleado para una VLAN de datos](#asignacion-de-una-vlan-de-datos-a-una-interfaz-fisica-del-switch). 

