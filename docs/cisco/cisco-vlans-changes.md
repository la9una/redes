## Guardar la configuración
Para guardar la configuración activa, de manera que se cargue en el próximo inicio del sistema:

```apache
Switch# copy running-config startup-config
```
Ante la pregunta que nos devuelve el comando anterior, presionamos la tecla ++enter++ para aceptar.


## Eliminar configuración
Es posible revertir los cambios efectuados en el switch fácilmente.


### Eliminar direccionamiento IP (estático)
Podemos quitar una dirección IP seteada de manera estática

=== "Sintaxis general"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface <puerto-fisico-del-switch>
    Switch(config-if)# no ip address
    Switch(config-if)# exit
    ```
=== "Ejemplo"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/2
    Switch(config-if)# no ip address
    Switch(config-if)# exit
    ```

### Eliminar direccionamiento IP (dinámico)
Igualmente, podemos quitar una dirección IP seteada de manera dinámica.

=== "Sintaxis general"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface <puerto-fisico-del-switch>
    Switch(config-if)# no ip address dhcp
    Switch(config-if)# exit
    ```
=== "Ejemplo"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/2
    Switch(config-if)# no ip address dhcp
    Switch(config-if)# exit
    ```

### Elminar tipo de VLAN
Podemos setear la interfaz física de un switch, en modo _VLAN Access_ o en modo _VLAN Trunk_, como vimos anteriormente. Y también, con un sólo comando, deshacer la configuración de dicha interfaz. 

=== "Sintaxis general"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface <puerto-fisico-del-switch>
    Switch(config-if)# no switchport mode
    ```
=== "Ejemplo"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/1
    Switch(config-if)# no switchport mode
    ```

### Eliminar VLAN ID
Podemos eliminar una VLAN siempre y cuando conozcamos el ID de la misma. 

=== "Sintaxis general"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface <puerto-fisico-del-switch>
    Switch(config-if)# no vlan <vlan-id>
    ```
=== "Ejemplo"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/1
    Switch(config-if)# no vlan 100
    ```

!!!warning "Eliminar VLAN ID"
    Antes de eliminar una VLAN, es necesario reasignar todos los puertos hacia otra VLAN. De otro modo, los puertos que no se trasladen a una VLAN activa no se podrán comunicar con otros hosts hasta que se asignen a una VLAN activa.

### Eliminar VLAN Nativa
Es posible eliminar una VLAN nativa fácilmente. 

=== "Sintaxis general"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface <puerto-fisico-del-switch>
    Switch(config-if)# no switchport trunk native vlan
    ```
=== "Ejemplo"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/1
    Switch(config-if)# no switchport trunk native vlan
    ```