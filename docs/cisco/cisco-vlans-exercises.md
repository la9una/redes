Ejercicios varios para practicar configuración de un switch Cisco. 


## Ejercicio sobre VLANs
Teniendo en cuenta el esquema de red presente en la imagen, se procederá a crear las VLANs correspondientes. 

<div style="margin:0 auto; text-align:center; font-size: 0.7rem" >
<img src="../imgCisco/vlan_exercise.png" alt="Ejercicio integrador VLAN" style="width: 100%; height: auto;">
  <figcaption>Ejercicio de integración de conocimientos</figcaption>
</div>

### Switch 1

| VLAN  ID | VLAN Name               | VLAN Port | VLAN Type      | VLAN Description        |
| -------- | ----------------------- | --------- | -------------- | ----------------------- |
| 100      | RRHH                    | e0/2      | Data (Access)  | Recursos humanos (PC 1) |
| 200      | VoIP                    | e0/1      | Voice (Access) | Telefonía IP (Phone 1)  |
| ---      | ---                     | e0/0      | Trunk          | VLAN Troncal            |
| 300      | Nativa                  | e0/0      | Native         | VLAN Nativa             |

### Switch 2

| VLAN  ID | VLAN Name               | VLAN Port | VLAN Type      | VLAN Description        |
| -------- | ----------------------- | --------- | -------------- | ----------------------- |
| 100      | RRHH                    | e0/2      | Data (Access)  | Recursos humanos (PC 2) |
| 200      | VoIP                    | e0/1      | Voice (Access) | Telefonía IP (Phone 2)  |
| ---      | ---                     | e0/0      | Trunk          | VLAN Troncal            |
| 300      | Nativa                  | e0/0      | Native         | VLAN Nativa             |

### Comandos del Switch
Como el esquema de red fue cuidadosamente diseñado, los puertos y las VLANs coinciden en ambos switches. Por lo tanto, se listan todos los comandos necesarios para configurar dichos dispositivos intermedios, según requerimientos del esquema de red de la imagen (los comandos dados a continuación deberán _correrse_ en cada switch):

```apache
Switch> enable
Switch# configure terminal
Switch(config)# vlan 100
Switch(config-vlan)# name RRHH
Switch(config-vlan)# exit
Switch(config)# vlan 200
Switch(config-vlan)# name VoIP
Switch(config-vlan)# exit
Switch(config)# vlan 300
Switch(config-vlan)# name Nativa
Switch(config-vlan)# exit
Switch(config)# interface Ethernet0/2
Switch(config-if)# switchport mode access 
Switch(config-if)# switchport access vlan 100 
Switch(config-if)# exit
Switch(config)# interface Ethernet0/1
Switch(config-if)# switchport mode access 
Switch(config-if)# switchport voice vlan 200 
Switch(config-if)# mls qos trust cos
Switch(config-if)# exit
Switch(config)# interface Ethernet0/0
Switch(config-if)# switchport mode trunk 
Switch(config-if)# switchport trunk native vlan 300
Switch(config-if)# end
Switch# show vlan brief
Switch# copy running-config startup-config
```

¡Listo! Restará configurar el direccionamiento IP de los dispositivos finales, desplegando una subred para cada VLAN de accesso. 



