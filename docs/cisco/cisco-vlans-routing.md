Las VLANs son redes independientes y, por defecto, no pueden comunicarse entre sí. El inter-VLAN routing permite que estas redes separadas puedan intercambiar información. Esto se hace mediante un **router** o un **switch capa 3**, que actúa como un puente para enviar datos de una VLAN a otra. Hay tres modos principales de hacer esto:

1. [Enrutamiento Inter-VLAN con Router con múltiples interfaces físicas (Deprecado)](#1-enrutamiento-inter-vlan-heredado-o-legacy-deprecado)
2. [Enrutamiento Inter-VLAN con Router Router-on-a-stick](#2-enrutamiento-inter-vlan-router-on-a-stick)
3. [Enrutamiento Inter-VLAN con Switch de capa 3](#3-enrutamiento-inter-vlan-en-switch-capa-3)

Veamos a continuación el detalle de cada implementación. 

## 1. Enrutamiento Inter-VLAN Heredado o Legacy (Deprecado)
Este método se usaba cuando recién aparecieron las VLANs, antes de que existiera el concepto de trunking. Consiste en un router con una interfaz física dedicada a cada VLAN. Cada interfaz del router se conecta a un puerto del switch que pertenece a la VLAN correspondiente. Actualmente, este método está obsoleto y ya no se usa por cuestiones de escalabilidad.

=== "Inter-VLAN Heredado - Router (Cisco)"
    <div style="margin:0 auto; text-align:center; font-size: 0.7rem" >
    <img src="../imgCisco/vlan_routing_1_cis.png" alt="Inter-VLAN Routing - Legacy" style="width: 80%; height: auto;">
    <figcaption>Inter-VLAN Routing Heredado (Deprecado) - Router Cisco</figcaption>
    </div>
=== "Inter-VLAN Heredado - Router (Mikrotik)"
    <div style="margin:0 auto; text-align:center; font-size: 0.7rem" >
    <img src="../imgCisco/vlan_routing_1_mik.png" alt="Inter-VLAN Routing - Legacy" style="width: 80%; height: auto;">
    <figcaption>Inter-VLAN Routing Heredado (Deprecado) - Router Mikrotik</figcaption>
    </div>

### Configuración de los dispositivos del ejemplo

=== "Router (Cisco) - Ejemplo"
    ```apache
    # Establecer direccionamiento de las interfaces del router 
    Router> enable
    Router# configure terminal
    Router(config)# interface e0/0
    Router(config-if)# ip address 192.168.110.1 255.255.255.0
    Router(config-if)# duplex full # Seteando Full Dúplex
    Router(config-if)# no shutdown
    Router(config-if)# interface e0/1
    Router(config-if)# ip address 192.168.120.1 255.255.255.0
    Router(config-if)# duplex full # Seteando Full Dúplex
    Router(config-if)# no shutdown
    Router(config-if)# end
    Router# show ip route
    ```
=== "Switch L2 (Cisco) - Ejemplo"
    ```apache
    # Creación de las VLANs
    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan 110 
    Switch(config-vlan)# name VLAN_110
    Switch(config-vlan)# exit
    Switch(config)# vlan 120 
    Switch(config-vlan)# name VLAN_120
    Switch(config-vlan)# exit

    # Asignar las VLANs a las interfaces físicas
    Switch(config)# interface range e0/0-1
    Switch(config-if-range)# switchport mode access
    Switch(config-if-range)# switchport access vlan 110
    Switch(config-if-range)# no shutdown
    Switch(config-if-range)# exit
    Switch(config)# interface range e0/2-3
    Switch(config-if-range)# switchport mode access
    Switch(config-if-range)# switchport access vlan 120
    Switch(config-if-range)# no shutdown
    Switch(config-if-range)# end
    ```
=== "VPCS - Ejemplo"
    ```apache
    # PC 1
    PC_1> ip 192.168.110.2/24 192.168.110.1

    # Server
    Server> ip 192.168.120.2/24 192.168.120.1
    ```

!!!failure "Inter-VLAN Routing Heredado"
    Este método de routing entre VLANs ya no se implementa en redes y se incluye únicamente con fines explicativos.


## 2. Enrutamiento Inter-VLAN Router-on-a-Stick
El router tiene una única interfaz física pero varias subinterfaces, una para cada VLAN. El switch envía tráfico etiquetado con el ID de VLAN al router, y este se encarga de redirigirlo a la VLAN correspondiente.

=== "Router-on-a-stick (Router Cisco)"
    <div style="margin:0 auto; text-align:center; font-size: 0.7rem" >
    <img src="../imgCisco/vlan_routing_2_cis.png" alt="Inter-VLAN Routing - Router-on-a-Stick" style="width: 80%; height: auto;">
    <figcaption>Inter-VLAN Routing - "Router-on-a-Stick" (Router Cisco)</figcaption>
    </div>
=== "Router-on-a-stick (Router Mikrotik)"
    <div style="margin:0 auto; text-align:center; font-size: 0.7rem" >
    <img src="../imgCisco/vlan_routing_2_mik.png" alt="Inter-VLAN Routing - Router-on-a-Stick" style="width: 80%; height: auto;">
    <figcaption>Inter-VLAN Routing - "Router-on-a-Stick" (Router Mikrotik)</figcaption>
    </div>

### Configuración de los dispositivos del ejemplo

=== "Router (Cisco) - Ejemplo"
    ```apache
    # Router (Cisco) - Establecer dirección IP administrativa para las VLANs
    Router(config)# interface e0/0.110
    Router(config-subif)# encapsulation dot1q 110
    Router(config-subif)# ip address 192.168.110.1 255.255.255.0
    Router(config-subif)# exit
    Router(config)# interface e0/0.120
    Router(config-subif)# encapsulation dot1q 120
    Router(config-subif)# ip address 192.168.120.1 255.255.255.0
    Router(config-subif)# exit
    Router(config)# interface e0/0
    Router(config-if)# duplex full # Seteando Full Dúplex
    Router(config-if)# no shutdown
    Router(config-if)# exit

    # [Opcional] Router (Cisco) - Servidor DHCP en la VLAN 110
    Router(config)# ip dhcp pool POOL_VLAN_110
    Router(dhcp-config)# network 192.168.110.0 255.255.255.0
    Router(dhcp-config)# default-router 192.168.110.1
    Router(dhcp-config)# dns-server 8.8.8.8
    Router(config-if)# end

    # Router (Cisco) - Verficar configuración
    Router# show vlan brief
    Router# show ip interface brief
    ```
=== "Router (Mikrotik) - Ejemplo"
    ```apache
    # Router (Mikrotik) - Creación de las VLANs y asignación a las interfaces físicas
    /interface vlan add interface=ether1 name=VLAN_110 vlan-id=100
    /interface vlan add interface=ether1 name=VLAN_120 vlan-id=200
    /interface vlan add interface=ether1 name=VLAN_NATIVA vlan-id=300

    # Router (Mikrotik) - Establecer dirección IP administrativa para las VLANs
    /ip address add address=192.168.110.1/24 interface=VLAN_110 network=192.168.110.0
    /ip address add address=192.168.120.1/24 interface=VLAN_120 network=192.168.120.0

    # [Opcional] Router (Mikrotik) - Establecer un servidor DHCP para vlan 110
    /ip pool add name=POOL_100 ranges=192.168.110.2-192.168.110.254
    /ip dhcp-server add address-pool=POOL_100 interface=VLAN_110 name=DHCP_100
    /ip dhcp-server network add address=192.168.110.0/24 gateway=192.168.110.1

    # Router (Mikrotik) - Verficar configuración
    /interface print
    /ip address print
    /ip dhcp-server print
    ```
=== "Switch L2 (Cisco) - Ejemplo"
    ```apache
    # Switch L2 - Creación de las VLANs
    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan 110 
    Switch(config-vlan)# name VLAN_110
    Switch(config-vlan)# exit
    Switch(config)# vlan 120 
    Switch(config-vlan)# name VLAN_120
    Switch(config-vlan)# exit  
    Switch(config)# vlan 130
    Switch(config-vlan)# name VLAN_NATIVA 
    Switch(config-vlan)# exit

    # Switch L2 - Asignar las VLANs a las interfaces físicas
    Switch(config)# interface e0/0
    Switch(config-if)# switchport trunk encapsulation dot1q
    Switch(config-if)# switchport mode trunk 
    Switch(config-if)# switchport trunk native vlan 130
    Switch(config-if)# duplex full # Seteando Full Dúplex
    Switch(config-if)# no shutdown
    Switch(config-if)# exit
    Switch(config)# interface e0/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 110
    Switch(config-if)# no shutdown
    Switch(config-if)# exit
    Switch(config)# interface e0/2
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 120
    Switch(config-if)# no shutdown
    Switch(config-if)# end
    ```
=== "VPCS - Ejemplo"
    ```apache
    # PC 1
    PC_1> ip 192.168.110.2/24 192.168.110.1

    # Server
    Server> ip 192.168.120.2/24 192.168.120.1
    ```

## 3. Enrutamiento Inter-VLAN en Switch Capa 3
Un switch L3 que puede realizar funciones de routing. Estos switches pueden manejar el tráfico entre VLANs internamente, sin necesidad de un router separado. Cada VLAN se configura con una interfaz virtual (SVI) en el switch.

<div style="margin:0 auto; text-align:center; font-size: 0.7rem" >
<img src="../imgCisco/vlan_routing_3_cis.png" alt="Inter-VLAN Routing - Switch L3" style="width: 80%; height: auto;">
  <figcaption>Inter-VLAN Routing - Switch L3</figcaption>
</div>

### Configuración de los dispositivos del ejemplo
=== "Switch L3 (Cisco) - Ejemplo"
    ```apache
    # Switch L3 - Creación de las VLANs
    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan 110 
    Switch(config-vlan)# name VLAN_110
    Switch(config-vlan)# exit
    Switch(config)# vlan 120 
    Switch(config-vlan)# name VLAN_120
    Switch(config-vlan)# exit  
    Switch(config)# vlan 130
    Switch(config-vlan)# name VLAN_NATIVA 
    Switch(config-vlan)# exit

    # Switch L3 - Configurar el enlace troncal
    Switch(config)# interface e0/0
    Switch(config-if)# switchport trunk encapsulation dot1q
    Switch(config-if)# switchport mode trunk 
    Switch(config-if)# switchport trunk native vlan 130
    Switch(config-if)# no shutdown
    Switch(config-if)# exit

    # Switch L3 - Configurar direccionamiento de las interfaces VLAN ó SVIs
    Switch(config)# interface vlan 110
    Switch(config-if)# ip address 192.168.110.1 255.255.255.0
    Switch(config-if)# no shutdown
    Switch(config-if)# exit
    Switch(config)# interface vlan 120
    Switch(config-if)#ip address 192.168.120.1 255.255.255.0
    Switch(config-if)# no shutdown
    Switch(config-if)# exit

    # [Opcional] Switch L3 - Configurar un servidor DHCP para la VLAN 110
    Switch(config)# ip dhcp pool POOL_VLAN_110
    Switch(dhcp-config)# network 192.168.110.0 255.255.255.0
    Switch(dhcp-config)# default-router 192.168.10.1
    Switch(dhcp-config)# dns-server 8.8.8.8
    Switch(dhcp-config)# exit

    # Switch L3 - Habilitar el enrutamiento IP
    Switch(config)# ip routing
    Switch(config)# end

    # Switch L3 - Verificar la configuración
    Switch# show vlan brief
    Switch# show ip interface brief
    ```
=== "Switch L2 (Cisco) - Ejemplo"
    ```apache
    # Switch L2 - Creación de las VLANs
    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan 110 
    Switch(config-vlan)# name VLAN_110
    Switch(config-vlan)# exit
    Switch(config)# vlan 120 
    Switch(config-vlan)# name VLAN_120
    Switch(config-vlan)# exit  
    Switch(config)# vlan 130
    Switch(config-vlan)# name VLAN_NATIVA 
    Switch(config-vlan)# exit

    # Switch L2 - Asignar las VLANs a las interfaces físicas
    Switch(config)# interface e0/0
    Switch(config-if)# switchport trunk encapsulation dot1q
    Switch(config-if)# switchport mode trunk 
    Switch(config-if)# switchport trunk native vlan 130
    Switch(config-if)# no shutdown
    Switch(config-if)# exit
    Switch(config)# interface e0/1
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 110
    Switch(config-if)# no shutdown
    Switch(config-if)# exit
    Switch(config)# interface e0/2
    Switch(config-if)# switchport mode access
    Switch(config-if)# switchport access vlan 120
    Switch(config-if)# no shutdown
    Switch(config-if)# end
    ```
=== "VPCS - Ejemplo"
    ```apache
    # PC 1
    PC_1> ip 192.168.110.2/24 192.168.110.1

    # Server
    Server> ip 192.168.120.2/24 192.168.120.1
    ```

!!!success "Direccionamiento: SVI vs IP de Gestión en Switches Cisco"
    En la administración de redes Cisco, es crucial distinguir entre una Interfaz Virtual de Switch (SVI) y una IP de gestión. Aunque ambos se utilizan en la configuración de switches, su propósito y funcionalidad son diferentes. Esta distinción es vital para garantizar una correcta configuración y operación de la red, ya que cada uno desempeña un papel único en el manejo del tráfico y la administración del dispositivo.

    | Característica            | SVI en Switches L3                                           | IP de Gestión en Switches L2                                 |
    | ------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
    | **Propósito**             | Enrutamiento de tráfico entre diferentes VLANs               | Administración remota del switch                             |
    | **Funcionalidad**         | Capacidad de enrutamiento de capa 3                          | Solo capacidad de capa 2, sin enrutamiento                   |
    | **Uso Típico**            | En switches que necesitan inter-VLAN routing                 | En switches para acceso remoto de configuración y gestión    |
    | **Configuración de VLAN** | Asociado a una VLAN específica                               | Generalmente asociado a la VLAN de gestión (por defecto VLAN 1) |
    | **Enrutamiento**          | Sí, enruta tráfico entre VLANs                               | No, no enruta tráfico                                        |
    | **Interacción con VLANs** | Proporciona una puerta de enlace para dispositivos en la VLAN | Proporciona acceso administrativo al switch                  |