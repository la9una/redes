# Visualizando la configuración
Existen diversos modos de visualización que podremos emplear. Para ello, deberemos encontrarnos en el modo [EXEC Privilegiado](../cisco/cisco-management.md#2-exec-privilegiado). 


## Información completa del dispositivo

=== "Ejemplo"
    ```apache
    Switch# show running-config
    ```
=== "Ejemplo abreviado"
    ```apache
    Switch# sh ru
    ```

## Interfaces
Mostrar todas las interfaces: 
=== "Ejemplo"
    ```apache
    Switch# show interfaces
    ```
=== "Ejemplo abreviado"
    ```apache
    Switch# sh int
    ```


Mostrar el estado de todas las interfaces:

```apache
Switch# show interfaces status
```

Mostrar el estado operativo de los puertos a nivel de Capa 2 (todos los puertos):

=== "Ejemplo"
    ```apache
    Switch# show interfaces switchport
    ```
=== "Ejemplo abreviado"
    ```apache
    Switch# sh int sw
    ```

Mostrar el estado operativo de los puertos a nivel de Capa 2 (puerto determinado):

=== "Sintaxis general"
    ```apache
    Switch# show interface <puerto-fisico-del-switch> switchport
    ```
=== "Ejemplo"
    ```apache
    Switch# show interface Ethernet0/1 switchport
    ```
=== "Ejemplo abreviado"
    ```apache
    Switch# sh int e0/1 sw
    ```
Mostrar información sobre los enlaces troncales: 

=== "Ejemplo"
    ```apache
    Switch# show interfaces trunk
    ```
=== "Ejemplo abreviado"
    ```apache
    Switch# sh int tr
    ```


## Direccionamiento

=== "Ejemplo"
    ```apache
    Switch# show ip interface
    ```
=== "Ejemplo abreviado"
    ```apache
    Switch# sh ip in
    ```


O bien, para mostrar información acotada de las interfaces: 

=== "Ejemplo"
    ```apache
    Switch# show ip interface brief
    ```
=== "Ejemplo abreviado"
    ```apache
    Switch# sh ip in br
    ```


## VLANs

```apache
Switch# show vlan
```

=== "Ejemplo"
    ```apache
    Switch# show vlan
    ```
=== "Ejemplo abreviado"
    ```apache
    Switch# sh vl
    ```

O bien, mostrando información acotada:  

=== "Ejemplo"
    ```apache
    Switch# show vlan brief
    ```
=== "Ejemplo abreviado"
    ```apache
    Switch# sh vl br
    ```

