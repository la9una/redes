# [VLAN Troncal (trunk)](../../networking/network-vlans/#vlan-troncal-trunk-vlan-tagged-ports)
Las VLANs troncales son un tipo especial de VLANs que permiten el paso de diferentes VLANs de acceso. 

## Creación de una VLAN troncal
Las VLAN troncales no se crean de la misma manera que las VLAN "normales". En su lugar, se designan puertos físicos del switch los cuáles formarán un enlace troncal. 

Cada **enlace troncal** debe estar asociado a una [VLAN nativa](../../networking/network-vlans/#vlan-nativa), la cual se encarga de **gestionar el tráfico no etiquetado que circula a través del enlace troncal**. Por defecto, la VLAN nativa es la VLAN 1. No obstante, por cuestiones de seguridad, **crearemos una VLAN diferente y la asignaremos al enlace _trunk_**.

=== "Sintaxis general"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan <id-de-la-vlan-nativa>
    Switch(config-vlan)# name <nombre-de-la-vlan-native> # Opcional
    Switch(config-vlan)# exit
    ```
=== "Ejemplo"
    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# vlan 300
    Switch(config-vlan)# name Nativa # Opcional
    Switch(config-vlan)# exit
    ```

!!!failure "Error: Native VLAN mismatch"
        
    El mensaje de error **"Native VLAN mismatch"** se produce cuando dos dispositivos de red configurados para formar un enlace troncal tienen diferentes configuraciones de VLAN nativa.

    **Por ejemplo**: En un enlace troncal, intervienen dos switches:

    - Switch 1: VLAN nativa 1
    - Switch 2: VLAN nativa 300

    Para resolver este error, es necesario definir el ID de la VLAN nativa que se empleará y configurarlo de manera idéntica en ambos extremos del enlace troncal.

## Asignación de una _VLAN troncal_ a una interfaz física del switch
En este paso, asignaremos la VLAN troncal a un puerto específico del switch. Adicionalmente, vincularemos la VLAN Trunk con un [VLAN Nativa](../../networking/network-vlans#vlan-nativa) con el fin de gestionar el tráfico no etiquetado. 

=== "Sintaxis general"
    ```apache hl_lines="7"
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface <puerto-fisico-del-switch>
    Switch(config-if)# switchport trunk encapsulation dot1q
    Switch(config-if)# switchport mode trunk 
    Switch(config-if)# switchport trunk native vlan <vlan-id>
    Switch(config-if)# switchport trunk allowed vlan <vlan-id-a>,<vlan-id-n> # Opcional
    Switch(config-if)# exit
    ```

=== "Ejemplo"
    ```apache hl_lines="7"
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/1
    Switch(config-if)# switchport trunk encapsulation dot1q
    Switch(config-if)# switchport mode trunk 
    Switch(config-if)# switchport trunk native vlan 300
    Switch(config-if)# switchport trunk allowed vlan 100,200,300 # Opcional
    Switch(config-if)# exit
    ```

!!!info "Método de encapsulamiento VLAN"
    Cisco utiliza principalmente el estándar IEEE 802.1Q para encapsular el tráfico VLAN en enlaces troncales, por lo tanto, no es necesario especificarlo. Sin embargo, hay otros métodos de encapsulación que Cisco ha utilizado en el pasado o que son compatibles con ciertos dispositivos. 

    - **802.1Q**
        - Descripción: Estándar por defecto en la actualidad
        - Comando: `Switch(config-if)# switchport trunk encapsulation dot1q`

    - **DTP**
        - Descripción: Negociación automática (Dynamic Trunking Protocol)
        - Comando: `Switch(config-if)# switchport mode dynamic auto`

    - **ISL [Deprecado]**
        - Descripción: ISL (Inter-Switch Link). Propietario de Cisco.
        - Comando: `Switch(config-if)# switchport trunk encapsulation isl`

    - **Desactivado (None)**
        - Descripción: Desactiva completamente la encapsulación VLAN en la interfaz especificada
        - Comando: `Switch(config-if)# switchport trunk encapsulation none`


!!!success "Filtrado de VLANs (Opcional)"

    El comportamiento por defecto de un enlace troncal es permitir el paso de todas las VLANs. No obstante, podemos modificar esto restringiendo el paso de determinadas VLANS.  

    <div style="margin:0 auto; text-align:center; font-size: 0.6rem">
    <img src="../imgCisco/vlan_trunk_allowed.png" alt="VLAN Nativa" style="width: 90%; height: auto;">
    <figcaption>Ejemplo de `switchport trunk allowed`: sólo se permite el paso de las VLANs 10 y 20.<br>(imagen: www.networkacademy.io)</figcaption>
    </div>

    Para el ejemplo de la imagen, el comando a ejecutar en cada uno de los switches sería: 

    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/1
    Switch(config-if)# switchport trunk allowed vlan 100,200
    Switch(config-if)# exit
    ```
    
    Si el comando anterior no se ejecuta, o se elimina, **el puerto troncal permitirá el paso de todas las VLANs**. 

    Para permitir el paso de todas las VLANs: 

    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/1
    Switch(config-if)# switchport trunk allowed vlan all
    Switch(config-if)# exit
    ```

    O bien: 

    ```apache
    Switch> enable
    Switch# configure terminal
    Switch(config)# interface Ethernet0/1
    Switch(config-if)# no switchport trunk allowed vlan
    Switch(config-if)# exit
    ```