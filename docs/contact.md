# Contactame
Haceme llegar tus inquietudes. Responderé a la brevedad. Muchas gracias

<form class="contact" action="https://fabform.io/f/RTJ7ub2" method="POST">
  <label for="name">Nombre</label>
  <input type="text" id="name" name="name" required>
  <label for="email">Correo electrónico</label>
  <input type="email" id="email" name="email" required>
  <label for="message">Mensaje</label>
  <textarea id="message" name="message" required></textarea>
  <input type="submit" value="Enviar">
</form>