# Ejemplo práctico de Docker y CI/CD

En la presente guía se pretende ejemplifica el uso de Docker en conjunto con Gitlab CI/CD (Integración continua/Despliegue continuo) para desplegar un sitio de documentación empleando [MkDocs](https://www.mkdocs.org/)

## ¿Qué es MkDocs?

MkDocs es una herramienta de código abierto escrita en Python que  permite crear documentación para proyectos de forma rápida y sencilla. Está diseñada para generar sitios web estáticos, lo que significa que convierte archivos de texto en formato Markdown en un sitio web completamente navegable y con estilo.

### Preliminares
Para poder instalar MkDocs es condición necesaria tener [instalado Python en Python pip en el sistema](https://www.mkdocs.org/user-guide/installation/). 

A continuación, ejecutá el siguiente comando para instalar Python en Linux Debian/Ubuntu: 

```bash
sudo apt install python3 python3-pip -y
```

### Instalación de MkDocs
Una vez verificada la instalación de Python, procedemos a instalar MkDocs empleando una terminal: 

```bash
pip3 install mkdocs
```

Para verificar la correcta instsalación de MkDocs, ejecutamos: 

```bash
mkdocs --version
```

O en Windows:

```bash
python -m pip install mkdocs
```

```bash
python -m mkdocs
```

### Trabajando en local
Para desplegar MkDocs en tu equipo personal, seguí estos pasos. 

#### 1. Creá un nuevo proyecto

```bash
mkdocs new nombre_del_proyecto
cd nombre_del_proyecto
```
#### 2. Creá una nueva página

Dentro de la carpeta del proyecto (`nombre_del_proyecto`), abrí la carpeta `docs`.

Crea un archivo Markdown (por ejemplo, `index.md` o `introduccion.md`) y agregale contenido: 

```markdown
# Bienvenido a Mi Proyecto
Esta es la primera página de mi documentación.
```

#### 3. Agregá la página a la navegación

Abrí el archivo `mkdocs.yml` y agregá la nueva página en la sección `nav` para que aparezca en el menú de navegación:

```yaml
nav:
  - Inicio: index.md
  - Introducción: introduccion.md
```

#### 4. Iniciá el servidor local 

```bash
mkdocs serve
```
Esto mostrará el sitio de documentación de manera local.

#### 5. Generá el sitio estático 
Para que MkDocs convierta los archivos markdown en el contenido de un sitio web estático, ejecutá: 

```bash
mkdocs build
```
Esto generará una carpeta llamada `site`, que contendrá el sitio web estático generado. 


### Trabajando en remoto
Para desplegar un sitio de MkDocs en GitLab Pages, seguí estos pasos:

#### 1. Creá el archivo de configuración para GitLab CI/CD:

En la raíz de tu proyecto, creá un archivo llamado `.gitlab-ci.yml` con el siguiente contenido:

```yaml
image: python:3.9

stages:
  - deploy

pages:
  stage: deploy
  script:
    - pip install mkdocs
    - mkdocs build -d public
  artifacts:
    paths:
      - public
```
Este archivo le indica a GitLab CI que use una imagen **Docker** de Python, instale MkDocs, construya el sitio y coloque los archivos generados en la carpeta public, que es la que GitLab Pages usa para servir el sitio.

#### 2. Subí el proyecto a GitLab

Si todavía no lo hiciste, inicializá un repositorio en GitLab y subí tu proyecto:

```bash
git init
git add .
git commit -m "Initial commit"
git remote add origin <URL-de-tu-repositorio-en-GitLab>
git push -u origin main
```

#### 4. Verificá el despliegue

Una vez subido el proyecto, GitLab va a iniciar el `pipeline` automáticamente y de esta manera generará y publicará el sitio web estático de documentación. 

Podés verificar el estado de todas las `pipelines` en `Build > Pipelines`

#### 5. Accedé a la página de documentación

Cuando finalice el proceso, dirigite a `Deploy > Pages` (`Implementar > Páginas` en español) para encontrar el enlace a tu sitio web de documentación. 

Opcionalmente, podés configurar un dominio propio (del tipo `.com`, `.ar`, etc).

### Personalización
MkDocs ofrece amplias capacidades de personalización para adaptar la documentación a las necesidades específicas de cada proyecto. Permite elegir entre distintos temas visuales, ajustando colores, tipografías y estilo general. Además, se pueden agregar plugins y extensiones de Markdown para ampliar las funcionalidades, como tablas de contenido automáticas, búsqueda en tiempo real, y soporte para snippets de código avanzados.

#### Temas

Podés encontrar temas para tu sitio de documentación en el siguiente enlace:

:globe_with_meridians: https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes

Para elegir un tema, configurá la opción theme en tu archivo de configuración  `mkdocs.yml`:

```yaml
theme:
  name: readthedocs
```

#### Navegación
La navegación en el archivo `mkdocs.yml` se configura bajo la opción `nav`, donde se define la estructura jerárquica del menú del sitio. 

A continuación, un ejemplo de navegación: 

```yaml
# Configuración de navegación en varios niveles
nav:
  - Inicio: index.md
  - Guía de Usuario:
      - Introducción: guia/introduccion.md
      - Instalación:
          - Linux: guia/instalacion/linux.md
          - Windows: guia/instalacion/windows.md
          - Mac: guia/instalacion/mac.md
      - Uso Básico: guia/uso_basico.md
  - Referencia:
      - API:
          - Autenticación: referencia/api/autenticacion.md
          - Endpoints Principales: referencia/api/endpoints.md
      - CLI:
          - Comandos Básicos: referencia/cli/comandos_basicos.md
          - Opciones Avanzadas: referencia/cli/opciones_avanzadas.md
  - Preguntas Frecuentes: faq.md
  - Acerca de: acerca.md
```

#### Ejemplo completo de `mkdocs.yml`
Este archivo `mkdocs.yml` configura un sitio de documentación usando MkDocs con opciones avanzadas de personalización. Define el nombre, la descripción, y una estructura de navegación jerárquica para organizar el contenido en secciones y subsecciones. Utiliza el tema [material](https://squidfunk.github.io/mkdocs-material/), al que se le personalizan colores, fuentes, y logotipos, e incluye enlaces a redes sociales como GitHub, Twitter, y LinkedIn para facilitar el acceso a los perfiles del autor. Además, emplea plugins y extensiones de Markdown que añaden funcionalidad adicional, como la búsqueda, el estilo de bloques de código, y las notas al pie, lo que mejora la usabilidad y presentación del sitio.

```yaml
# Información básica del sitio
site_name: "Documentación de Mi Proyecto"
site_description: "Guía y referencia de Mi Proyecto"
site_author: "Raulito"
site_url: "https://mi-sitio.com"

# Navegación del sitio
nav:
  - Inicio: index.md
  - Guía de Usuario:
      - Introducción: guia/introduccion.md
      - Instalación:
          - Linux: guia/instalacion/linux.md
          - Windows: guia/instalacion/windows.md
          - Mac: guia/instalacion/mac.md
      - Uso Básico: guia/uso_basico.md
  - Referencia:
      - API:
          - Autenticación: referencia/api/autenticacion.md
          - Endpoints Principales: referencia/api/endpoints.md
      - CLI:
          - Comandos Básicos: referencia/cli/comandos_basicos.md
          - Opciones Avanzadas: referencia/cli/opciones_avanzadas.md
  - Preguntas Frecuentes: faq.md
  - Acerca de: acerca.md

# Configuración del tema
theme:
  name: material
  logo: images/logo.png
  favicon: images/favicon.ico
  palette:
    primary: indigo
    accent: pink
  font:
    text: Roboto
    code: Roboto Mono
  # Redirección de íconos sociales
  social:
    - icon: fontawesome/brands/github
      link: https://github.com/mi_usuario
    - icon: fontawesome/brands/twitter
      link: https://twitter.com/mi_usuario
    - icon: fontawesome/brands/linkedin
      link: https://linkedin.com/in/mi_usuario

# Extensiones de Markdown
markdown_extensions:
  - admonition
  - toc:
      permalink: true
  - codehilite
  - footnotes
  - pymdownx.details
  - pymdownx.superfences

# Plugins adicionales
plugins:
  - search
  - awesome-pages
  - minify:
      minify_html: true

# Configuración de Google Analytics
extra:
  analytics:
    provider: google
    tracking_id: UA-XXXXXXXXX-X  # Reemplazá con tu ID de seguimiento de Google Analytics

# Opciones adicionales para personalización
extra_css:
  - styles/extra.css
extra_javascript:
  - scripts/extra.js
```

La estructura del proyecto organiza el contenido en la carpeta `docs/`, donde se encuentran las secciones de documentación como `guia` y `referencia`, junto con subcarpetas para recursos visuales (`images/`), estilos (`styles/`) y scripts (`scripts/`).

```cmd
.
├── mkdocs.yml
└── docs
    ├── index.md
    ├── guia
    │   ├── introduccion.md
    │   ├── instalacion
    │   │   ├── linux.md
    │   │   ├── windows.md
    │   │   └── mac.md
    │   └── uso_basico.md
    ├── referencia
    │   ├── api
    │   │   ├── autenticacion.md
    │   │   └── endpoints.md
    │   └── cli
    │       ├── comandos_basicos.md
    │       └── opciones_avanzadas.md
    ├── faq.md
    ├── acerca.md
    ├── images
    │   ├── logo.png
    │   └── favicon.ico
    ├── styles
    │   └── extra.css
    └── scripts
        └── extra.js
```

