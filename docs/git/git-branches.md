# Gestionando ramas
Comandos útiles de git para la gestión de ramas en modo local y remoto


## Creando ramas
Sintaxis general: 

	git branch <nombre-de-la-nueva-rama>

Ejemplo: 

	git branch rama1

Podemos crear y acceder a la rama creada con un mismo comando: 

	git checkout -b <nombre-de-la-rama>

Por ejemplo: 

	git checkout -b rama2

## Renombrando ramas
Sintaxis general: 

	git branch -M <nuevo-nombre-de-la-rama>

Ejemplo: 

	git branch -M rama1


### Creando ramas a partir de un commit

Otro modo de crear ramas es partiendo de un commit determinado: 

	git branch <nombre-de-la-rama> <commit-hash>

O bien usando una referencia simbólica: 

	git branch <nombre-de-la-rama> HEAD~3

Para acceder a la rama una vez creada: 

	git checkout -b <nombre-de-la-rama> <commit-hash or HEAD~3>

!!!done "Uso de switch"
	El comando switch es reciente. Su uso es similar a checkout: 

		git switch <nombre-de-la-rama>

	Por ejemplo: 

		git switch rama1

	También podemos crear una rama y desplazarnos hacia ella con un sólo comando: 

		git switch -c <nombre-de-la-nueva-rama>

	Por ejemplo: 

		git switch -c rama2

	Finalmente, podremos crear una rama a partir de un commit determinado y desplazarnos hacia ella con un sólo comando: 

		git switch -c <nombre-de-la-nueva-rama> <commit-hash>

	Por ejemplo: 

		git switch -c rama4 3e9c398

## Renombrando ramas
Sintaxis general: 

	git branch -M <nuevo-nombre-de-la-rama>

Ejemplo: 

	git branch -M rama2

## Desplazándose entre ramas
Podemos movernos entre las diferentes ramas exixtentes. 

Sintaxis general: 

	git checkout <nombre-de-la-rama>

Ejemplo: 

	git checkout rama1

Y obtendremos una salida similar a esta: 


```bash
  main
* rama1
  rama2
```

## Fusionando ramas
Procedimiento destinado a combinar el contenido de una rama en la rama actual.

Sintaxis general: 

	git merge <nombre-de-la-rama-a-fusionar-con-la-rama-actual>

Ejemplo: 

	git merge rama1

El comando anterior fusionará el contenido de la rama `rama1` con el contenido de la rama actual. 


## Cargando la rama local en remoto
Para tener una copia de la rama local en remoto (uptstream), la sintaxis general es: 

	git push --set-upstream origin <nombre-de-la-rama-local>

Por ejemplo:

	git push --set-upstream origin rama1

El comando anterior creará la `rama1` de manera remota. 


!!!done "Clonar ramas remotas"
	Cuando clonamos un repositorio, se clonan también las ramas existentes. No obstante, en local, es posible que al ejecutar el comando: 
	
		git branch
	
	No visualicemos todas las ramas existentes, sino sólo la principal (usualmente "master" o "main"). Para poder visualizar todas las ramas, ejecutamos el mismo comando, con la opción `-a`: 

		git branch -a

## Eliminando ramas
Al borrar una rama, borraremos también su contenido. Borrar una rama local no implica borrar su copia remota, y viceversa. 

### Eliminando una rama local
Sintaxis general: 

	git branch -d <nombre-de-la-rama-a-eliminar>

Ejemplo: 

	git branch -d rama1

El comando anterior eliminará la rama local `rama1`  y su contenido.

### Eliminando una rama remota
Sintaxis general: 

	git push origin --delete <nombre-de-la-rama-a-eliminar>


Ejemplo: 

	git push origin --delete rama1

El comando anterior eliminará la rama local `rama1` y su contenido.

NOTA: No es posible eliminar la rama actual. En otras palabras, para eliminar una rama determinada tendremos que estar ubicados en una rama distinta a la que deseamos eliminar. 



