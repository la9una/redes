# Configuración inicial
Una vez instalado git, deberás configurar la herramienta por única vez para poder utilizarla. Realizaremos los pasos que se indican a continuación, escribiendo los comandos en una terminal. 

## Datos personales
git es una herramienta de colaboración por lo cual, se hace necesario setear algunos datos como tu nombre de pila y correo electrónico que servirán para identificarte.  

### Configurando nombre
	
	git config --global user.name <nombre>

Por ejemplo: 

	git config --global user.name "Raúl Jesús López"


### Configurando email
	
	git config --global user.email <email>

Por ejemplo: 

	git config --global user.email rauljesusl@gmail.com

## Configurando color
Resulta especialmente útil habilitar los colores en la terminal, para facilitar la lectura: 

	git config --global color.ui allways

## Visualizando la configuración
Para ver todas las opciones de configuración, ejecutá: 

	git config --list

Para visualizar sólo una clave específica de la configuración:

	git config <clave>

Por ejemplo: 

	git config user.name

## Obteniendo ayuda
Para conocer las opciones de los diferentes comandos, debe ejecutarse: 

	git help <comando>

Por ejemplo: 

	git help config
