# Fundamentos
No se necesita aprender muchas cosas para comenzar a usar git de manera efectiva. Sin embargo, los conceptos que siguen a continuación son claves para comprender su funcionamiento y sacarle provecho rápidamente. 

## Comenzando a trabajar con un repositorio git

Existen dos maneras de comenzar a trabajar con un repositorio git: 

* Creando un repositorio git en una carpeta local existente
* Clonando un repositorio remoto existente

A continuación, se explica el procedimiento en cada caso. 

### Creando un repositorio git en una carpeta local existente
Supongamos que tenemos en nuestro ordenador una carpeta que contiene el código de una aplicación que estamos desarrollando y deseamos hacer un seguimiento de las modificaciones que hagamos sobre dicho código. El primer paso será dirigirnos a la carpeta de nuestro proyecto: 

```bash
cd carpetaConMiProyecto
```

Acto seguido, inicializaremos un repositorio git dentro de la carpeta en cuestión: 

    git init

El comando anterior creará una carpeta llamada .git (oculta) que contiene todos los archivos necesarios del repositorio. 

### Clonando un repositorio remoto existente
Si deseás obtener una copia de un repositorio Git existente (por ejemplo, un proyecto en el que te gustaría contribuir), el comando que necesitas es `git clone`:

```bash
git clone <url-repositorio-remoto>
```

Por ejemplo: 

```bash
git clone https://gitlab.com/la9una/redes.git
```

Con dichos pasos, habrás obtenido una copia local de un repositorio remoto. 

## Los tres estados
Git tiene tres estados principales en los que se pueden encontrar los archivos de tu repositorio git: 

* Modificado (modified): significa que has modificado el archivo pero todavía no lo has confirmado a tu base de datos. 
* Preparado (staged): significa que has marcado un archivo modificado en su versión actual para que vaya en tu próxima confirmación.
* Confirmado (committed): significa que los datos están almacenados de manera segura en tu base de datos local. 

Esto nos lleva a las tres secciones principales de un proyecto de Git: 

* El directorio de trabajo (working directory)
* El área de preparación o *index* (staging area)
* El directorio de Git (Git directory)

![git Data](imgGit/git-data-1.svg)

## Flujo de trabajo básico
Teniendo en cuenta los 3 estados mencionados con anterioridad, el flujo de trabajo básico en Git es algo así:

1. Modificás una serie de recursos (archivos, carpetas) en tu directorio de trabajo
2. Preparás los cambios realizados sobre los recursos, añadiéndolos al área de preparación
3. Confirmás los cambios, los cuales se tomarán desde el área de preparación y se almacenarán de manera permanente en tu directorio .git

### 1. Modificar recursos
Se trata de realizar cualquier cambio en los archivos o carpetas de tu repositorio git (agregar, modificar, eliminar). Cada uno de estos cambios serán registrados por git en los recursos que estén bajo seguimiento. 

### 2. Preparar modificaciones realizadas a los recursos
Para que git haga un seguimiento de los cambios realizados sobre tus recursos, es necesario indicarle cuáles serán dichos recursos. Esto se consigue con el siguiente comando: 

    git add <recurso>

Por ejemplo: 

    git add estilos.css

Si, en cambio, queremos agregar todos los archivos que se encuentran en nuestra ubicación, ejecutamos: 

    git add .

!!!info "Excluyendo recursos"
        Podemos hacer que git ignore ciertos recursos, listándolos en un archivo llamado [`.gitignore`](https://www.atlassian.com/es/git/tutorials/saving-changes/gitignore), ubicándolo en la raíz de nuestro repositorio git. Veamos un ejemplo: 

        ```bash
        # Ignora archivos del sistema Mac 
        .DS_store

        # Ignora todos los archivos de texto
        *.txt

        # Ignora los archivos dentro del directorio mydir
        mydir/*
        ```

### 3. Confirmar modificaciones realizadas a los recursos
Para confirmar los cambios que se encuentran en el área de preparación, ejecutamos: 

```bash
git commit -m "<mensaje>"
```

Donde `<mensaje>` deberá ser reemplazado por una cadena de texto descriptiva en relación a las acciones que estemos realizando. Por ejemplo: 

```bash
git commit -m "Actualizando el archivo estilos.css"
```

!!!done "Reescribiendo el texto del mensaje del último _commit_"
    Nos puede ocurrir cometimos un error al escribir el mensaje del commit o simplemente deseamos cambiarlo. Entonces, ejecutamos:

    ```bash
    git commit --amend
    ```

    Esto nos abrirá un editor de texto de terminal donde podremos cambiar el texto del mensaje del commit. 

    También podemos realizar esta acción en un solo paso: 

    ```bash
    git commit --amend -m "<Nuevo texto del mensaje para el commit>"
    ```

!!!info "Seguimiento de cambios"
        Git almacena cualquier cambio realizado en el repositorio y a cada uno le asigna un [hash (SHA-1)](https://es.wikipedia.org/wiki/Secure_Hash_Algorithm) determinado y único, formado por 40 caracteres hexadecimales. Un hash SHA-1 se ve de la siguiente forma: `24b9da6552252987aa493b52f8696cd6d3b00373`

Con estos simples pasos, tenemos un repositorio git con archivos bajo seguimiento y una confirmación inicial.
