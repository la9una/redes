[Git](https://git-scm.com/) es un [sistema de control de versiones](https://www.atlassian.com/es/git/tutorials/what-is-version-control) distribuido creado por [Linus Torvalds](https://es.wikipedia.org/wiki/Linus_Torvalds) pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente.

![Logo de Git](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/512px-Git-logo.svg.png)

Desde su nacimiento en el 2005, Git ha evolucionado y madurado para ser fácil de usar y conservar sus características iniciales. Es tremendamente rápido, muy eficiente con grandes proyectos, y tiene un increíble sistema de ramificación (branching) para desarrollo no lineal.

