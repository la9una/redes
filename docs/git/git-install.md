# Instalación
Listado de instaladores según sistemas operativos

* Windows: 
	* Descargar instalador: [gitforwindows.org](https://gitforwindows.org/)
	* Usando [Chocolatey](https://chocolatey.org/): `choco install git`
* Linux:
	* Distribuciones basadas en Debian: `sudo apt install git`
	* Distribuciones basadas en CentOS: `sudo yum install git`
* MacOS: 
	* Usando [homebrew](https://brew.sh/): `brew install git`
	* Usando [MacPorts](https://www.macports.org/): `sudo port install git`

