# Recursos
Algunos resúmenes útiles encontrados en internet sobre git: 

* [Pro Git, libro oficial de git](https://git-scm.com/book/es/v2)
* [git, la guía sencilla](https://rogerdudler.github.io/git-guide/index.es.html) 
* [Starting with Git & GitHub](https://www.slideshare.net/nicotourne/starting-with-git-git-hub-27464735)
* [Hoja de referencia para GitHub Git](https://training.github.com/downloads/es_ES/github-git-cheat-sheet.pdf)
* [git Cheatsheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)