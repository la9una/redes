# Deshaciendo cosas en Git
Una de las fortalezas de git es la capacidad de poder realizar "viajes en el tiempo", es decir, recorrer el historial de cambios para deshacerlos o crear nuevas versiones de nuestro trabajo a partir de un punto determinado.  

## git restore
El comando sacará el archivo del área de preparación para colocarlo en el directorio de trabajo. En otras palabras, cambiará el estado del archivo de _preparado_ a _modificado_. Su sintaxis: 

    git restore --staged <archivo>

Por ejemplo: 

    git restore --staged index.html

## git reset

Un reset es una operación que **toma un commit específico y restablece el historial para que coincida con el estado del repositorio en ese commit específico**.

Supongamos que, partiendo de la siguiente situación, ejecutamos el comando `git reset HEAD~2`

![git reset](imgGit/checkout-revert-reset-2.png)

Estamos deshaciendo los dos últimos commits de la rama Hotfix . Estos dos commits quedarán huérfanos y se eliminarán la próxima vez que Git haga limpieza.

![git reset](imgGit/checkout-revert-reset-3.png)

Por ejemplo: 

```bash
git reset HEAD~3
```

Eliminará los últimos 3 commits hacia atrás, pero manteniendo las modificaciones que contiene ese commit en el área de trabajo. 

### git reset a nivel de archivo
El comando git reset se puede usar también a nivel de un único archivo. En cuyo caso su comportamiento es diferente a cuando se usa a nivel de commit.

Por ejemplo: 

```bash
git reset HEAD~2 index.html
```

El archivo `index.html` ahora está modificado (además, se mantienen las modificaciones hechas) y, nuevamente, no preparado.

## git revert
Un revert es una operación que **toma un commit específico y crea un nuevo commit con el contenido del commit especificado**.

Supongamos que partimos de la siguiente situación y queremos volver al commit marcado con el asterisco.

![git revert](imgGit/checkout-revert-reset-4.png)

Tras ejecutar el comando `git revert HEAD~2` estaremos deshaciendo los cambios de los dos últimos commits de la rama _Hotfix_ **añadiendo un nuevo commit** que ejecuta los cambios.

![git revert](imgGit/checkout-revert-reset-5.png)


!!!warning "`git revert` sobre archivos"
    Este comando solo se puede ejecutar a nivel de commit y no a nivel de archivo.

## git checkout
Un checkout es una operación que mueve el puntero de referencia `HEAD` a un _commit_ específico.

Supongamos que el `HEAD` de la rama master se encuentra en el _commit d_. Ahora ejecutamos el comando `git checkout b`.

![git revert](imgGit/checkout-revert-reset-1.png)

El resultado es un cambio en el historial de commit.

Veámoslo con un ejemplo: 

```bash
git checkout 56a4e5c08
```

Permite volver hacia atrás a cualquier _commit_ (56a4e5c08 en el ejemplo) que forme parte de la historia de nuestro proyecto, deshaciendo los cambios posteriores al commit elegido.

!!!warning "Detached HEAD"
    La acción anterior dejaría al proyecto sin referencia a `HEAD`(DETACHED HEAD). Eso significa que podemos hacer cambios y modificaciones sobre el código del momento en el que nos hemos situado y hacer los _commits_ que necesitemos, pero si queremos que esos cambios se mantengan en git y no sean eliminados por el proceso `git garbage collection process` tendremos que [crear una nueva rama del último commit que hemos realizado](git-branches.md#creando-ramas-a-partir-de-un-commit).

### git checkout a nivel de archivo
El comando git checkout se puede usar también a nivel de un único archivo. En cuyo caso su comportamiento es diferente a cuando se usa a nivel de commit.

En este caso no mueve el HEAD del repositorio, lo que hace es llevar al directorio de trabajo el fichero al que hemos hecho checkout con el contenido que tenía en el commit especificado.

Por ejemplo: 

    git checkout -- index.html

De esta manera, el archivo `index.html` volverá al estado que tenía antes del último _commit_


## Base de conocimiento
* https://www.aunitz.net/diferencias-checkout-revert-reset/