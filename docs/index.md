# Bienvenid@s

El presente espacio recopila teoría y ejercicios prácticos sobre temas relacionados con la implementación de redes y servicios utilizando, siempre que sea posible, software de código abierto. 

<!-- <div align="center"><img src="../img/network.svg" alt="Redes de datos"  width="50%" height="30%"></div> -->

<div align="center" style="width:100%;">
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<lottie-player src="https://assets6.lottiefiles.com/packages/lf20_zw5fzhuo.json"  background="transparent"  speed="1"  style="width: auto; height: auto;" loop  autoplay></lottie-player>
</div>

Los contenidos abordan los objetivos propuestos en el diseño curricular de la materia **Instalación, mantenimiento y reparación de redes informáticas** correspondiente al séptimo año de estudios de la escuela secundaria técnica, con orientación **Técnico en Informática Profesional y Personal**, en Buenos Aires, Argentina. 





