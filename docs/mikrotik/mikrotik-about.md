# Mikrotik

[Mikrotik](https://mikrotik.com/) es un fabricante letón de equipos de red. La compañía desarrolla y vende enrutadores de red cableados e inalámbricos, conmutadores de red, puntos de acceso, así como sistemas operativos y software auxiliar.

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik-logo.png" alt="Logo Mikrotik" style="width: 60%;
  height: auto;">
  <figcaption>Logo de Mikrotik</figcaption>
</div>

La compañía fue fundada en 1996 con el objetivo de vender equipos en mercados emergentes. Rápidamente sus productos alcanzaron popularidad por la gran relación costo/prestacion que éstos ofrecen.