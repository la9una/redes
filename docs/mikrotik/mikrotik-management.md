# Conectándonos a Mikrotik
Para poder gestionar nuestro Router Mikrotik, se suguiere conectarlo directamente a la PC mediante cable, según la siguiente figura: 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik_first_run.png"  width="60%" height="30%" alt="Mikrotik First Run">
  <figcaption>Mikrotik - Conexión inicial</figcaption>
</div>

Cuando sacamos un Router Mikrotik de la caja, éste viene con una configuración por defecto:  

* IP: `192.168.88.1/24`
* Puerto: `ether1`
* Usuario: admin
* Contraseña: [En blanco]

Para ganar acceso, tendremos que configurar la interfaz de red de nuestra PC con el mismo direccionamiento con el que el Router viene provisto de fábrica. 


!!!tip "Hard Reset"

    Una práctica recomendada es hacer un _hard reset_ al router para configurarlo según nuestras necesidades, empleando el siguiente comando:  

    ```c#
    /system reset-configuration no-defaults=yes skip-backup=yes
    ```

    Esta orden tendrá el mismo efecto que el reset que se hace físicamente, introduciendo un pin dentro del orificio de _reset_. 

    Acto seguido tendremos que [confgurar el direccionamiento](../mikrotik/mikrotik-router.md#direccionamiento-estatico) de alguna de sus interfaces.  

## Modos de acceso
Es posible acceder al Router mediante diferentes vías, las cuáles se detallan a continuación. 

### Winbox
Este método de acceso, tal vez el más empleado por los administradores de estos equipos, se realiza mediante una aplicación desarrollada por el fabricante, sólo disponible para sistemas Microsoft Windows. 

* WinBox (64 Bits): https://mt.lv/winbox64 
* WinBox (32 Bits): https://mt.lv/winbox 

Una vez descargada la aplicación, haremos doble clic sobre la misma para abrirla. Vemores una pantalla similar a la siguiente:

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik_winbox_1.png" width="80%" height="auto%" alt="Mikrotik - Acceso Winbox">
  <figcaption>Mikrotik - Acceso Winbox</figcaption>
</div>

Si conocemos la IP del router, y las credenciales de acceso, podremos introducirlos en los campos correspondientes e iniciar sesión:

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik_winbox_2.png" width="80%" height="auto%" alt="Mikrotik - Acceso Winbox">
  <figcaption>Winbox - IP Login</figcaption>
</div>

Si no conocemos el direccionamiento del Router, nos dirigimos a la pestaña de _Neighbors_ y desde allí podremos visualizar los equipos de la marca conectados a nuestra red. Haciendo clic sobre la dirección MAC del equipo y conociendo las credenciales de inicio de sesión, ganaremos acceso: 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik_winbox_3.png" width="80%" height="auto%" alt="Mikrotik - Acceso Winbox">
  <figcaption>Winbox - MAC Login</figcaption>
</div>

Si el inicio de sesión fue exitoso, veremos una ventana similar a la siguiente: 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik_winbox_4.png" width="80%" height="auto%" alt="Mikrotik - Acceso Winbox">
  <figcaption>Winbox - GUI</figcaption>
</div>


### SSH
Para acceder al Router empleando el protocolo SSH, tendremos que emplear algún cliente SSH como [Putty](https://www.putty.org/) o [MobaXterm](https://mobaxterm.mobatek.net/). Allí, introduciremos los siguientes datos: 

* IP del router
* Usuario: admin
* Contraseña: [En blanco]

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik_cli_1.png" width="80%" height="auto%" alt="Mikrotik - SSH CLI">
  <figcaption>Mikrotik - Acceso SSH</figcaption>
</div>

Al primer inicio de sesión, se nos pedirá cambiar la contraseña por defecto por una de nuestra elección. 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik_cli_2.png" width="80%" height="auto%" alt="Mikrotik - SSH CLI">
  <figcaption>Mikrotik - Acceso SSH</figcaption>
</div>

Otra opción, si tenemos el servicio de SSH instalado en nuestra PC, es ingresar mediante la terminal de comandos del sistema. Suponiendo que la dirección IP del Router es `10.0.6.43`, ejecutamos: 

```bash
ssh admin@10.0.6.43
```
Luego, el procedimiento a seguir será el mismo que cuando iniciamos sesión con algun otro cliente SSH.

### Webfig
El último modo de acceso al Router se vale del uso de un navegador. En éste último tendremos que ingregar la IP del Router, usuario y contraseña:  

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik_webfig_1.png" width="80%" height="auto%" alt="Webfig Login">
  <figcaption>Webfig Login</figcaption>
</div>

Una vez que hayamos ganado acceso, veremos una interfaz web, muy similar a la que presenta la aplicación de escritorio Winbox: 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik_webfig_2.png" width="80%" height="auto%" alt="Webfig GUI">
  <figcaption>Webfig GUI</figcaption>
</div>


