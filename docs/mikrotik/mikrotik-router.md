# Gestión de Routers Mikrotik
En la presente documentación, se exponen comandos básicos de configuración de un router Mikrotik (Router OS).

## Acceso remoto (SSH)
El acceso remoto mediante SSH (Secure Shell) en routers MikroTik es crucial para garantizar una administración segura de la red. SSH proporciona una conexión encriptada que protege la información sensible y evita accesos no autorizados. Configurar SSH en estos routers permite a los administradores gestionar la red desde cualquier ubicación de forma segura. Este proceso incluye la creación de claves criptográficas, la configuración de usuarios y la aplicación de medidas de seguridad. En esta guía, se detallarán los pasos necesarios para habilitar y configurar SSH en routers MikroTik, resaltando su papel en la seguridad y eficiencia de la red.

### 1. Conexión al Router Mikrotik
Para conectarte al router Mikrotik, necesitás conocer su dirección IP. Esta dirección IP debe ser accesible desde la red.

```bash
/ip address print
```

### 2. Habilitación del Servicio SSH
Por defecto, Mikrotik tiene SSH habilitado en el puerto 22, pero es posible que necesites configurarlo o verificar la configuración existente si has modificado los puertos o las credenciales de acceso.

```bash
/ip service enable ssh
/ip service set ssh port=22
```

### 3. Credenciales de Acceso
Deberás disponer de las credenciales correctas para autenticarte en el router Mikrotik. Una buena práctica consiste en crear un usuario específico para esta función. 

```bash
/user add name=<nombre-de-usuario> password=<contraseña> group=full
```

Por ejemplo: 

```bash
# Crea un nuevo usuario administrador
/user add name=admin2 password=password123 group=full

# Verifica que el usuario se haya creado correctamente
/user print
```

## Esquema de red (ejemplo)

Para una mejor comprensión, los ejemplos de la presente sección están basados en el siguiente esquema de red:

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/net_example_mikrotik.png" alt="Esquema de red de ejemplo">
  <figcaption>Esquema de red ejemplo</figcaption>
</div>

## Direccionamiento estático
Cuando deseamos que una dirección IP no cambie a lo largo del tiempo, empleamos este modo de direccionamiento. Es útil para ubicar fácilmente servicios que puedan estar corriendo en la red.

Sintaxis general:

```c#
/ip address add address=<ip>/<mascara-cidr> interface=<interfaz>
```

En este ejemplo, configuramos el direccionamiento de la interfaz `ether3` del _Router 1_:

```c#
/ip address add address=10.0.1.1/24 interface=ether3
```

### Bridge de Interfaces
Es posible agrupar diferentes interfaces en un bridge. Éste último, se comportará como una única interfaz a la que podemos asociar con una misma subred. 

Para una mejor comprensión, crearemos un bridge de interfaces basándonos en el siguiente esquema de red:

<div style="margin:0 auto; text-align:center;" >
<img src="../imgMikrotik/mikrotik-bridge.png" width="40%" height="auto" alt="Esquema de red de ejemplo">
  <figcaption>Esquema de red ejemplo</figcaption>
</div>

La sintaxis general para la creación de un bridge de interfaces:  

```c#
/interface bridge add name=<nombre-bridge>
```
Y para asignar que interfaces al bridge: 

```c#
/interface bridge port add bridge=<nombre-bridge> interface=<interfaz>
```

Aplicando lo aprendido a nuestro esquema de red ejemplo, llamaremos `bridge_lan` a nuestro bridge de interfaces: 

```c#
/interface bridge add name=bridge_lan
```

Y luego, le asociaremos a `bridge_lan`, las interfaces `ether2` y `ether3` del router:  

```c#
/interface bridge port add bridge=bridge_lan interface=ether2
/interface bridge port add bridge=bridge_lan interface=ether3
```

A partir de este momento, podremos asignar al bridge [direccionamiento estático](mikrotik-router.md#direccionamiento-estatico) como si se tratara de una única interfaz física. 

## Direccionamiento dinámico
La asignación de direcciones IP puede darse de manera dinámica, sea siendo el router cliente o servidor DHCP. 

### Cliente DHCP

Sintaxis general:

```c#
/ip dhcp-client add interface=<interfaz>
```

En este ejemplo, solicitaremos dirección IP al servidor DHCP alojado en la nube. La interfaz a configurar es la `ether1` del _Router 1_:

```c#
/ip dhcp-client add interface=ether1
```

### Servidor DHCP
Para constituir un servidor DHCP es necesario dar algunos pasos que se describen secuencialmente a continuación. 

#### Direccionamiento 
El primer paso es establecer una [dirección IP (estática)](#direccionamiento-estatico) en la interfaz sobre la que se montará el servidor DHCP. 

Para nuestro caso de ejemplo, asignamos una dirección IP estática para la interfaz `ether3` del _Router 2_: 

```c#
/ip address add address=10.0.3.1/24 interface=ether3
```

#### Pool DHCP
Un pool no es más que un rango de direcciones IP que serán ofrecidas por el servidor DHCP a los clientes que se conecten a la red. 

Sintaxis general: 

```c#
/ip pool add name=<nombre-pool> ranges=<rango-ip-usables>
```

Siguiendo con nuestra red de ejemplo, configuraremos un servidor DHCP en la interfaz `ether3` del _Router 2_: 

```c#
/ip pool add name=pool_lan_2 ranges=10.0.3.2-10.0.3.254
```

#### Asociar interfaz/pool

Sintaxis general:

```c#
/ip dhcp-server add address-pool=<nombre-pool> interface=<interfaz> name=<nombre-dhcp>
```

En el punto anterior, creamos un pool de direcciones IP. Es momento de asociarlo con la interfaz `ether3` del _Router 2_, donde queremos configurar nuestro servidor DHCP: 

```c#
/ip dhcp-server add address-pool=pool_lan_2 interface=ether3 name=dhcp_lan_2
```

#### Indicar red y gateway

Sintaxis general:

```c#
/ip dhcp-server network add address=<id-red>/<mascara-cidr> gateway=<ip-gateway>
```

Finalmente, para finalizar la configuración del servidor DHCP sobre la interfaz `ether3` del _Router 2_, indicamos la dirección de red, máscara y gateway del mismo:

```c#
/ip dhcp-server network add address=10.0.3.0/24 gateway=10.0.3.1
```

!!!tip "DHCP Server Wizard"
    Es posible crear el servidor DHCP siendo guiados en todo momento por un asistente. Para ello,  abrimos una terminal y ejecutamos:  
    
    ```c#
    /ip dhcp-server setup
    ``` 
    
## NAT

Sintaxis general:

```c#
/ip firewall nat add chain=srcnat out-interface=<interfaz> action=masquerade
```

Siguiendo con nuestro esquema de red de ejemplo, para permitir el acceso a internet de cualquier red LAN configurada en ambos Routers  (_Router 1_ y _Router 2_), ejecutamos:

```c#
/ip firewall nat add chain=srcnat out-interface=ether1 action=masquerade
```

## Enrutamiento

Sintaxis general:

```c#
/ip route add dst-address=<id-red>/<mascara-cidr> gateway=<ip-gateway> 
```
Con el siguiente comando, en el _Router 1_ estableceremos una ruta estática para permitir tráfico desde la _RED A_ hacia la _RED C_: 

```c#
/ip route add dst-address=10.0.3.0/24 gateway=10.0.2.2 
```
Para finalizar el enrutamiento estático, en el _Router 2_ es necesario establecer una ruta estática desde la _RED C_ hacia la _RED A_:

```c#
/ip route add dst-address=10.0.1.0/24 gateway=10.0.2.1 
```

## Opciones de impresión
A continuación se listan comandos útiles para visualizar la configuración de diferentes secciones del router. 

**Interfaces**

Visualizar las interfaces del router: 

```c#
/interface print
```

Listamos los bridges configurados en el router...

```c#
/interface bridge print
```

...así como los puertos involucrados en los mismos:

```c#
/interface bridge port print
```


**Direccionamiento**

Para ver el direccionamiento asignado para cada interfaz del router: 

```c#
/ip address print
```

**Enrutamiento**

Para visualizar la tabla de enrutamiento del router:

```c#
/ip route print
```

**DHCP**

Podemos listar tanto los clientes DHCP en el equipo... 

```c#
/ip dhcp-client print
```

...como los servidores DHCP presentes: 

```c#
/ip dhcp-server print
```








