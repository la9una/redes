
Para poder **monitorear equipos que tengan instalado el sistema operativo Microsoft Windows**, es preciso instalar en ellos un agente que se encargará de comunicarse con el servidor Nagios para enviar información sobre los mismos. Este agente es NSClient++.

## 1. Descarga e instalación de NSClient++
Podemos descargar el agente de Nagios para Windows desde [github](https://github.com/mickem/nscp/releases/download/0.5.3.4/NSCP-0.5.3.4-x64.msi)

Una vez que hayamos descargado el programa, al hacer clic sobre el ejecutable veremos una pantalla similar a la se muestra a continuación. Hacemos clic en _Next_ para continuar:

![NSClient](imgNagios/nsclient_01.png)

Luego elegimos la herramienta de monitoreo _Generic_:

![NSClient](imgNagios/nsclient_02.png)

A continuación, seleccionamos el tipo de instalación _Complete_:

![NSClient](imgNagios/nsclient_03.png)

Llegados a este punto, ingresamos la dirección IP del servidor Nagios (sin contraseña). Luego marcamos:

* todas las casillas de verificación 
* el botón de radio seteado en la opción "Insecure legacy mode". Luego presionamos el botón _Next_:

![NSClient](imgNagios/nsclient_04.png)

En este paso, presionamos _Install_:

![NSClient](imgNagios/nsclient_05.png)

Y comenzará la Instalación...

![NSClient](imgNagios/nsclient_06.png)

Segundos más tarde, la instalación habrá concluido. Clic en _Finish_ para cerrar el instalador.

![NSClient](imgNagios/nsclient_07.png)

## 2. Habilitando los módulos de NSClient++ manualmente

Para habilitar todos los módulos del agente NSClient++ editamos el archivo de configuración del programa, llamado `nsclient.ini`. Abriremos el archivo en cuestión con un editor de texto en **modo administrador** y reemplazaremos su contenido del mismo por el que sigue:  

```bash title="nsclient.ini" hl_lines="15"
; IMPORTANTE: modificar la dirección ip en "allowed hosts" por la dirección IP del servidor Nagios

[/modules]
CheckDisk = enabled
CheckEventLog = enabled
CheckExternalScripts = enabled
CheckHelpers = enabled
CheckNSCP = enabled
CheckSystem = enabled
CheckWMI = 1
NRPEServer = enabled
NSClientServer = enabled

[/settings/default]
allowed hosts = 10.0.7.131

[/settings/external scripts]
allow arguments = true

[/settings/external scripts/wrapped scripts]
check_replica=check_replica.ps1

[/settings/external scripts/alias]
alias_cpu = checkCPU warn=80 crit=90 time=5m time=1m time=30s
alias_cpu_ex = checkCPU warn=$ARG1$ crit=$ARG2$ time=5m time=1m time=30s
alias_disk = CheckDriveSize MinWarn=10% MinCrit=5% CheckAll FilterType=FIXED
alias_disk_loose = CheckDriveSize MinWarn=10% MinCrit=5% CheckAll FilterType=FIXED ignore-unreadable
alias_event_log = CheckEventLog file=application file=system MaxWarn=1 MaxCrit=1 "filter=generated gt -2d AND severity NOT IN ('success', 'informational') AND source != 'SideBySide'" truncate=800 unique descriptions "syntax=%severity%: %source%: %message% (%count%)"
alias_file_age = checkFile2 filter=out "file=$ARG1$" filter-written=>1d MaxWarn=1 MaxCrit=1 "syntax=%filename% %write%"
alias_file_size = CheckFiles "filter=size > $ARG2$" "path=$ARG1$" MaxWarn=1 MaxCrit=1 "syntax=%filename% %size%" max-dir-depth=10
alias_mem = checkMem MaxWarn=80% MaxCrit=90% ShowAll=long type=physical type=virtual type=paged type=page
alias_process = checkProcState "$ARG1$=started"
alias_process_count = checkProcState MaxWarnCount=$ARG2$ MaxCritCount=$ARG3$ "$ARG1$=started"
alias_process_hung = checkProcState MaxWarnCount=1 MaxCritCount=1 "$ARG1$=hung"
alias_process_stopped = checkProcState "$ARG1$=stopped"
alias_sched_all = CheckTaskSched "filter=exit_code ne 0" "syntax=%title%: %exit_code%" warn=>0
alias_sched_long = CheckTaskSched "filter=status = 'running' AND most_recent_run_time < -$ARG1$" "syntax=%title% (%most_recent_run_time%)" warn=>0
alias_sched_task = CheckTaskSched "filter=title eq '$ARG1$' AND exit_code ne 0" "syntax=%title% (%most_recent_run_time%)" warn=>0
alias_service = checkServiceState CheckAll
alias_service_ex = checkServiceState CheckAll "exclude=Net Driver HPZ12" "exclude=Pml Driver HPZ12" exclude=stisvc
alias_up = checkUpTime MinWarn=1d MinWarn=1h
alias_updates = check_updates -warning 0 -critical 0
alias_volumes = CheckDriveSize MinWarn=10% MinCrit=5% CheckAll=volumes FilterType=FIXED
alias_volumes_loose = CheckDriveSize MinWarn=10% MinCrit=5% CheckAll=volumes FilterType=FIXED ignore-unreadable

[/settings/NRPE/server]
verify mode = none
insecure = true
```


=== "Abrir el 'Bloc de Notas' desde la terminal (cmd)"

    a. Abrimos el "Simbolo del sistema" (cmd) en **modo administrador** y desde allí nos desplazamos a la carpeta de instalación del programa:

    ```bash
    cd "C:\Program Files\NSClient++"
    ```

    b. Continuando en la terminal, abrimos con el "Bloc de notas" el archivo `nsclient.ini`:
    ```bash
    start notepad nsclient.ini
    ```
    Reemplazamos el contenido del archivo `nsclient.ini` por el indicado más arriba. 

=== "Abrir el 'Bloc de Notas' en modo gráfico"
	Podemos realizar el mismo procedimiento abriendo el archivo con el "Bloc de Notas" en **modo administrador** desde la carpeta de instalación del programa NSClient++:
    
    ![NSClient](imgNagios/nsclient_08.png)

    Una vez abierto el archivo `nsclient.ini`, reemplazar su contenido por el indicado más arriba. 


### 3. Permitimos que el servicio interactúe con el sistema operativo

Podremos realizar la acción de dos maneras: gráfica o empleando la terminal de comandos PowerShell. 

=== "Modo gráfico"
    En Windows abrimos el diálogo _Ejecutar_ pulsando las teclas <kbd>Windows</kbd> + <kbd>R</kbd> y ejecutamos `services.msc`

    ![Servicios Windows](imgNagios/services_01.png)

    Hacemos clic derecho sobre el servicio _NSClient++ Monitoring Agent_ y seleccionamos la opción _Propiedades_ del menú contextual

    ![Servicios Windows](imgNagios/services_02.png)

    Verificamos que el servicio _NSClient++_ este iniciado y seteado como automático.

    ![Servicios Windows](imgNagios/services_03.png)

    Elegimos la pestaña _Iniciar sesión_. Allí tildaremos la opción _Permitir que el servicio interactúe con el escritorio_. 

    ![Servicios Windows](imgNagios/services_04.png)

    Finalmente, hacemos clic derecho sobre el servicio y del menú contextual, seleccionamos la opción "Restart" para reiniciar el servicio NSClient++ y aplicar los cambios realizados.  

=== "PowerShell"
    Abrimos PowerShell en **modo administrador** y ejecutamos:

    ```powershell
    Set-Service -Name nscp -StartupType Automatic
    sc.exe config nscp obj=LocalSystem
    sc.exe config nscp type=interact type=own
    Restart-Service -Name nscp
    ```


### 4. Permitimos ICMP en el Firewall de Windows 

Para permitir la comunicación fluida entre el host Windows y el servidor Nagios, permitimos el tráfico ICMP. Podemos hacerlo de manera gráfica o empleando la terminal de comando de PowerShell. 

=== "Agregamos regla del Firewall (vía PowerShell)"
    Abrimos PowerShell en **modo administrador** y ejecutamos: 

    ```powershell
    netsh advfirewall firewall add rule name="Permitir Ping" dir=in action=allow protocol=icmpv4
    ```
=== "Agregamos regla del Firewall (vía Gráfica)"
    Para crear una regla de Firewall de manera gráfica, seguí el tutorial en el enlace: https://redes.rauljesus.xyz/networking/network-firewall-windows



Por último, tendremos que [verificar la configuración y reiniciar el servidor Nagios](nagios-configuration.md#verificando-la-configuracion-y-reiniciando-nagios) para guardar los cambios que hayamos introducido.



