Una vez que hayamos instalado Nagios, disponemos de la carpeta `/usr/local/nagios/etc/` que contiene los archivos de configuración del programa.

Si ejecutamos el comando tree `tree /usr/local/nagios/etc/ -L 2 -d` podremos ver la estructura de directorios del programa:

```bash
/usr/local/nagios/etc/
├── bin
├── etc
│   └── objects
├── libexec
├── sbin
├── share
│   ├── angularjs
│   ├── bootstrap-3.3.7
│   ├── contexthelp
│   ├── d3
│   ├── docs
│   ├── images
│   ├── includes
│   ├── js
│   ├── locale
│   ├── media
│   ├── spin
│   ├── ssi
│   └── stylesheets
└── var
    ├── archives
    ├── rw
    └── spool

23 directories
```

## Creando nuestros archivos de configuración

A fin de mantener el orden, **crearemos una carpeta en la que alojaremos nuestros archivos de configuración personalizados**. Tanto el nombre de la carpeta que crearemos, como su ubicaiión son arbitrarios, pudiéndose emplear otros en su lugar: 

```bash
sudo mkdir /usr/local/nagios/etc/dispositivos
```

Dentro de la carpeta que creamos del paso anterior, **colocaremos nuestros archivos de configuración** los cuáles tendremos que crear, asignándoles un nombre arbitrario, pero con la extensión **.cfg**. Los archivos que crearemos serán: 

* `linux-host.cfg`
* `linux-services.cfg`
* `windows-host.cfg`
* `windows-services.cfg`
* `groups.cfg`
* `icons.cfg`


```bash
cd /usr/local/nagios/etc/dispositivos/ 
sudo touch linux-host.cfg linux-services.cfg windows-host.cfg windows-services.cfg groups.cfg icons.cfg
```

## Agregando nuestra configuración a Nagios

Tendremos que "notificar" a Nagios de los cambios que hemos introducido. Para ello, abriremos el archivo de configuración principal de Nagios: 

```bash
sudo vim /usr/local/nagios/etc/nagios.cfg
```

Una vez abierto el archivo, en algún lugar del mismo, agregaremos notificaremos a Nagios sobre la creación de nuestros archivos de configuración personalizados. Esta acción, puede realizarse indicando archivo por archivo, o bien, indicando el directorio en dónde se alojarán estos. Debe elegirse **sólo una de estas opciones**:  

=== "Opción A - Archivos de configuración"
    Se trata de indicar la ubicación de cada uno de los archivos de configuración:

    ```apache
    # Equipos Linux
    cfg_file=/usr/local/nagios/etc/dispositivos/linux-host.cfg

    # Servicios Linux
    cfg_file=/usr/local/nagios/etc/dispositivos/linux-services.cfg
    
    # Equipos Windows
    cfg_file=/usr/local/nagios/etc/dispositivos/windows-host.cfg

    # Servicios Windows
    cfg_file=/usr/local/nagios/etc/dispositivos/windows-services.cfg

    # Grupos de hosts
    cfg_file=/usr/local/nagios/etc/dispositivos/groups.cfg

    # Iconos
    cfg_file=/usr/local/nagios/etc/dispositivos/icons.cfg
    ```

=== "Opción B - Directorio de configuración"
    Se indica solamente la ubicación del directorio donde se alojan los archivos de configuración:

    ```apache
    # Directorio donde se alojan los archivos de configuración
    cfg_dir=/usr/local/nagios/etc/dispositivos
    ```

Para finalizar, guardaremos los cambios y cerraremos el editor.

## Verificando la configuración y reiniciando Nagios
Cada vez que realicemos cambios en los archivos de configuración del servidor tendremos que verificar que dichar configuración sea la correcta. Para ellos ejecutaremos el siguiente comando:

```bash
 sudo /usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
```

!!!danger "Advertencias y errores" 
	El comando emite dos tipos de mensajes: 
    
    - **Warnings**: Constituyen adevertencias que no interrumpirán el funcionamiento del servidor. 
    - **Errors**: Harán que el servidor deje de funcionar por lo que tendremos que solucionarlos para volver a activar el servidor.


Si la configuración está libre de errores entonces podremos reiniciar el servidor: 

```bash
sudo systemctl restart nagios
```

Si todo salió bien, podremos ingresar al panel web de administración desde un navegador y ver plasmados los cambios realizados. 



!!!success "Creación de alias para los comandos"

    Para facilitar la ejecución de los comandos anteriores podemos crear un alias para los mismos. Es decir, una especie de "acceso directo" que con una comando más simple, nos permita ejecutar comandos más completos. Para ello, habiéndonos logueado como usuarios root, ejecutaremos los siguientes comandos: 

    ```bash
    cd 
    echo "alias ngcheck='/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg'" >> .bashrc
    echo "alias ngreload='systemctl restart nagios'" >> .bashrc
    source .bashrc
    ```

    A partir de ahora y gracias al alias creado, para verificar la configuración de Nagios ejecutamos: 

    ```bash
    ngcheck
    ```

    Y para reiniciar el servicio de Nagios, ejecutamos: 

    ```bash
    ngreset
    ```


## Panel de administración
A continuación se listan algunas opciones del panel de administración web de Nagios que se configurarán en el presente curso: 

![Panel de Nagios](imgNagios/nagiospanel.png)

Donde:

|#|Nombre|Descripción|
|----|----|----|
|1|Tactical Overview|Resumen del estado de la red|
|2|Map|Mapa de conexiones y dispositivos de la red|
|3|Hosts|Detalle de los equipos de la red|
|4|Services|Detalle de los servicios monitoreados (por equipo)|
|5|Host Groups|Grupos de equipos|

