
Configurar Nagios a fin de monitorear los siguientes equipos de red:

| Nombre    | IP            | Grupo    | SO      | Variables a monitorear                                       |
| --------- | ------------- | -------- | ------- | ------------------------------------------------------------ |
| router    | A definir     | router   | Linux   | ping                                                         |
| ﬁleserver | A definir     | torvalds | Linux   | ping, FTP                                                    |
| webserver | A definir     | torvalds | Linux   | ping, HTTP, SSH                                              |
| windows   | A definir     | gates    | Windows | ping, Espacio en disco C, Estado<br/>del Firewall, Uso de RAM |

Creá los archivos de conﬁguración necesarios en la siguiente ubicación `/usr/local/nagios/etc/ejercicio` (deberás crear la carpeta y los archivos)