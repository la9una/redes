En Nagios Core, el monitoreo de la infraestructura de TI se organiza de manera eficiente a través del uso de grupos de hosts y grupos de servicios. Estos conceptos permiten agrupar recursos similares, facilitando así la gestión y supervisión de grandes cantidades de dispositivos y servicios.

## Grupos de Hosts
Un grupo de hosts es una colección de varios hosts que comparten características comunes o que forman parte de una misma categoría lógica dentro de la infraestructura. Por ejemplo, puedes crear un grupo de hosts para todos los servidores web, otro para las bases de datos, o para los dispositivos ubicados en una misma red. Esto permite realizar tareas de monitoreo de manera conjunta y recibir notificaciones centralizadas para todos los hosts dentro de un grupo.


Una vez que **hayamos definido los equipos** que forman parte de nuestra red, **los organizaremos en grupos** para su mejor administración. 

Para ello abrimos el arhivos llamado `groups.cfg` creado con anterioridad: 

```bash
sudo vim /usr/local/nagios/etc/dispositivos/groups.cfg
```

Y agregamos dentro del archivo, la información sobre los **grupos de hosts** que deseamos crear:

```apache
# Definición de grupos (para dispositivos de red, intermedios o finales)

define hostgroup {
hostgroup_name	linux-hosts
alias			Equipos con SO Linux
}

define hostgroup {
hostgroup_name	windows-hosts
alias			Equipos con SO Windows
}

```

!!!info "Definiendo miembros de grupos"
	Al momento de definir los grupos, opcionalmente, es posible definir los integrantes de cada uno de ellos, de la siguiente manera: 

    ```apache hl_lines="4"
    define hostgroup {
    hostgroup_name	windows-hosts
    alias			Equipos con SO Windows
    members		    pc-windows-01,pc-windows-02,pc-windows-03	
    }
    ```

Donde: 

|Directiva|Explicación|
|----|----|
|`define hostgroup { }`|Directiva empleada por Nagios para definir **un grupo** que querramos crear en la red.|
|`hostgroup_name`|Nombre único del grupo. Sin espacios ni caracteres reservados.|
|`alias`|Descripción larga del grupo (puede incluir espacios y caracteres reservados).|
|`members`|Nombres de host (equipos) que formarán parte del grupo, separados por comas (,).|


Por último, tendremos que [verificar la configuración y reiniciar el servidor Nagios](nagios-configuration.md#verificando-la-configuracion-y-reiniciando-nagios) para guardar los cambios que hayamos introducido.

## Grupos de Servicios
Un grupo de servicios agrupa varios servicios que están asociados a uno o más hosts. Esto es útil cuando se necesita monitorear varios aspectos de un servicio crítico, como la disponibilidad del servidor web, la carga del CPU, y el espacio en disco, y deseas visualizar o recibir alertas de estos servicios de manera unificada. Los grupos de servicios permiten gestionar estos conjuntos de servicios de forma más estructurada, facilitando la detección y resolución de problemas.

Por ejemplo, podemos reunir en un solo grupo el uso de RAM de todos los hosts que querramos, a fin de tener una vista panorámica de ítem. 


Abrimos el arhivos llamado `groups.cfg` creado con anterioridad: 

```bash
sudo vim /usr/local/nagios/etc/dispositivos/groups.cfg
```

Dentro del archivo incluimos la información sobre los **grupos de servicios** a crear. En este ejemplo, crearemos un grupo de servicios para monitorear ping en todos los equipos: 

```apache hl_lines="1"
define servicegroup {
servicegroup_name	all-cpu
alias			    Estado del CPU de todos los equipos
}
```

Luego, en la definición de los servicios que queremos incorporar al grupo creado tendremos que agregar la claúsula `servicegroups`: 


```apache hl_lines="6 15"
# Ejemplo de servicio para un equipo Windows
define service {
    use                     generic-service
    hostgroup_name          windows-hosts
    service_description     Carga del CPU
    servicegroups           all-cpu
    check_command           check_nt!CPULOAD!-l 5,80,90
}

# Ejemplo de servicio para un equipo Linux
define service {
    use                     generic-service                   
    hostgroup_name          linux-hosts                          
    service_description     Carga del CPU  
    servicegroups           all-cpu         
    check_command           check_local_load!5.0,4.0,3.0!10.0,6.0,4.0 
}
```