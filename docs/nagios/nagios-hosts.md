A continuación, agregaremos dos hosts a monitorear: un equipo con sistema operativo Linux y otro con sistema operativo Windows. 


=== "Equipo Linux"

    Abrimos el archivo que nombramos como `linux-host.cfg`:

    ```bash
    sudo nano /usr/local/nagios/etc/dispositivos/linux-host.cfg
    ```

    A continuación, incluimos en dicho archivo la configuración del equipo y servicios a monitorear:

    ```apache
    ###############################################################################
    #
    # DEFINICIÓN DE HOSTS - LINUX
    #
    ###############################################################################

    # Definiendo equipo a monitorear

    define host {
        use                     linux-server	                ; Plantilla utilizada
        host_name               my-linux-host		            ; Nombre del host 
        alias                   My Linux Host Device	        ; Descripción del host
        address                 10.0.0.49		                ; IP del host
        hostgroups              linux-hosts                     ; Grupo de pertenencia
        check_interval          1					            ; Chequear cada 1 minuto 
    }
    ```

=== "Equipo Windows"

    Abrimos el archivo `windows-host.cfg`:

    ```bash
    sudo nano /usr/local/nagios/etc/dispositivos/windos-host.cfg
    ```

    E incluimos en dicho archivo la configuración del equipo y servicios a monitorear:

    ```apache
    ###############################################################################
    #
    # DEFINICIÓN DE HOSTS - WINDOWS
    #
    ###############################################################################

    # Definir un host para la máquina Windows que se monitorizará
    # Cambiar el nombre del host (host_name), alias y dirección para ajustarlo a tu situación

    define host {

        use                     windows-server                  ; Plantilla utilizada
        host_name               my-windows-host                 ; Nombre del host
        alias                   My Windows Host Device          ; Descripción del host
        address                 10.0.0.15                       ; IP del host
        hostgroups              windows-hosts                   ; Grupo de pertenencia
        check_interval          1                               ; Intervalo de chequeo
    }
    ```
 

Por último, tendremos que [verificar la configuración y reiniciar el servidor Nagios](nagios-configuration.md#verificando-la-configuracion-y-reiniciando-nagios) para guardar los cambios que hayamos introducido.


!!!success "Topología de red"
    Nagios permite configurar los _hosts_ distinguiéndolos entre _padres_ e _hijos_ usando la directiva `parents`. Veámoslo con un ejemplo [extraído de la documentación de Nagios](https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/networkreachability.html), donde la siguiente topología de red:

    ![Parents](imgNagios/parents.png)

    Se corresponde con la siguiente configuración de _hosts_ (la configuración de cada _host_ aparece simplificada con fines didácticos )

    ```apache
    # El servidor Nagios (localhost) no posee "padre"
    define host{
        host_name		Nagios   
    }

    # Luego se definen el resto de los hosts

    define host{
        host_name		Switch1
        parents			Nagios
    }

    define host{
        host_name		Web
        parents			Switch1
    }

    define host{
        host_name		FTP
        parents			Switch1
    }

    define host{
        host_name		Router1
        parents			Switch1
    }

    define host{
        host_name		Switch2
        parents			Router1
    }

    define host{
        host_name		Wkstn1
        parents			Switch2
    }

    define host{
        host_name		HPLJ2605
        parents			Switch2
    }

    define host{
        host_name		Router2
        parents			Router1
    }

    define host{
        host_name		somewebsite.com
        parents			Router2
    }

    ```

