
Es posible **modificar las imágenes con que se mostrarán los equipos en el mapa** presentado por Nagios. 

## Definiendo vista de mapa
Para setear la vista de mapa por defecto tendremos que ejecutar el siguiente comando: 

```bash
echo "default_statusmap_layout=3" >> /usr/local/nagios/etc/cgi.cfg
```
Donde las opciones de mapa son (en el ejemplo se empleó la opción 3): 

- 0 = User-defined coordinates
- 1 = Depth Layers (Horizontal)
- 2 = Collapsed tree (Horizontal)
- 3 = Balanced tree (Horizontal)
- 4 = DON'T USE
- 5 = Circular Markup
- 6 = Circular Balloon
- 7 = Balanced tree (Vertical)
- 8 = Collapsed tree (Vertical)
- 9 = Depth Layers (Vertical)
- 10 = Force Map

Luego, [verificamos la configuración y reiniciamos el servidor Nagios](nagios-configuration.md#verificando-la-configuracion-y-reiniciando-nagios) para aplicar los cambios. 

## Definiendo íconos personalizados

Nagios ofrece una vista de mapa, a la cual se accede desde la opción **Map** del menú lateral izquierdo, que permite una vista global del estado de los hosts de manera gráfica. Los íconos que representan cada _host_ pueden ser modificados convenientemente. 

Podemos visualizar el nombre de los íconos que se mostraran en el mapa en: 

```bash
ls -l /usr/local/nagios/share/images  | less
```

Para salir de la vista, presionar la letra **q** (quit).

Para personalizar los íconos con los que se mostrarán los _hosts_ abrimos el archivo **iconos.cfg** creado con anterioridad:

```bash
sudo vim /usr/local/nagios/etc/dispositivos/icons.cfg
```

Dentro del archivo incluimos código similar al que sigue. Obviamente, deberemos adaptarlo a nuestras necesidades:

```apache
# Definiendo íconos para un equpo (individualmente)
define hostextinfo {
host_name       router1
icon_image      routerNuevo.png
icon_image_alt  Router de la sala principal
statusmap_image routerNuevo.png
vrml_image      routerNuevo.png
}

# Definiendo íconos para todos los miembros de un grupo
define hostextinfo {
hostgroup_name	sala1
icon_image		computer.png
icon_image_alt	Equipos de la Sala 1
statusmap_image	computer.png
vrml_image		computer.png
}
```

Por ejemplo, tomando un ícono entre los ofrecidos por Nagios para un _host_ llamado `my-windows-host`: 

```apache
# Definiendo íconos para un equipo (individualmente)
define hostextinfo {
host_name       my-windows-host
icon_image      logos/windows_server.png
icon_image_alt  Windows Server
statusmap_image logos/windows_server.png
vrml_image      logos/windows_server.png
}

```

Si empleamos un ícono (que aplique a un grupo de hosts) que se encuentra en `/usr/local/nagios/share/images/vendors`:

```apache
define hostextinfo {
hostgroup_name  linux-hosts
icon_image      ../vendors/linux.png
icon_image_alt  Equipos con Linux
statusmap_image ../vendors/linux.png
vrml_image      ../vendors/linux.png
}
```



!!!success "Sobre los íconos"
	Los **íconos** se pueden configurar para:

	* un equipo en particular, mediante la directiva **host_name** o
	* para un grupo de equipos empleando la directiva **hostgroup_name**

!!!warning "Imágenes con extensión .gd2"
	En versiones legacy de Nagios, para las imágenes de mapa, se solía emplear la extensión `.gd2`:
	
	```apache hl_lines="5"
	define hostextinfo {
	hostgroup_name  linux-hosts
	icon_image      ../vendors/linux.png
	icon_image_alt  Equipos con Linux
	statusmap_image ../vendors/linux.gd2
	vrml_image      ../vendors/linux.png
	}
	```

	En la actualidad, en versiones de Nagios 4 en adelante, se emplean imagenes `.png` por defecto: 

	```apache hl_lines="5"
	define hostextinfo {
	hostgroup_name  linux-hosts
	icon_image      ../vendors/linux.png
	icon_image_alt  Equipos con Linux
	statusmap_image ../vendors/linux.png
	vrml_image      ../vendors/linux.png
	}
	```

Por último, tendremos que [verificar la configuración y reiniciar el servidor Nagios](nagios-configuration.md#verificando-la-configuracion-y-reiniciando-nagios) para guardar los cambios que hayamos introducido.

## Transfiriendo íconos en Nagios

Otra opción interesante para **visualizar gráficamente los íconos** precargados de Nagios consiste en descargar la carpeta que los contiene desde el servidor. 

Para ello, instalaremos un **cliente FTP** como, por ejemplo, Filezilla. 

Una vez instalado el programa, hacemos clic sobre el ícono _Gestor de sitios_:

![Filezilla](imgNagios/filezilla_01.png)

Se desplegará un cuadro de diálogo en el cual hacemos clic en _Nuevo sitio_:

![Filezilla](imgNagios/filezilla_02.png)

Y allí completamos el formulario con los datos de nuestro servidor Nagios, según se muestra en la imagen y damos clic en _Aceptar_ para guardar los cambios. 

![Filezilla](imgNagios/filezilla_03.png)

Si volvemos a hacer clic en el botón _Gestor de sitios_ veremos la conexión a nuestro servidor realizada con anterioridad. Hacemos clic en _Conectar_:

![Filezilla](imgNagios/filezilla_04.png)

Como se trata de una conexión segura (SFTP) el programa nos preguntará si deseamos intercambiar claves de cifrado con el servidor. Hacemos clic en _Aceptar_:

![Filezilla](imgNagios/filezilla_05.png)

De esta manera, en la ventana izquierda del programa veremos nuestros archivos de Windows y en la ventana derecha, la carpeta _home_ de nuestro usuario en el servidor Nagios:

![Filezilla](imgNagios/filezilla_06.png)

A continuación nos desplazamos hacia la carpeta `/usr/local/nagios/share/images` que contiene los íconos preinstalados de Nagios que se mostrarán en la vista de mapa:

![Filezilla](imgNagios/filezilla_07.png)

Haciendo clic derecho sobre la carpeta en cuestión podemos descargar los archivos de imagen de ícono para visualizaros con mayor comodidad:

![Filezilla](imgNagios/filezilla_08.png)

![Filezilla](imgNagios/filezilla_09.png)

Dentro de la carpeta de íconos, encontramos distintos tipos, donde la carpeta _base_ contiene los íconos por defecto:

![Filezilla](imgNagios/filezilla_10.png)

Por ejemplo, el contenido de la carpeta _logos_:

![Filezilla](imgNagios/filezilla_11.png)


## Descargando íconos en nuestro servidor Nagios 
Podemos encontrar paquetes de íconos adicionales para agregar a nuestro servidor Nagios. Por ejemplo, el paquete [F*Nagios Icon Pack](https://exchange.icinga.com/exchange/F*Nagios+icon+pack+(Status+Map+and+Host+View+icons)). A continuación veremos como descargalos y emplearlos: 

Primeramente, nos dirigimos a la carpeta de imágenes de nuestro servidor Nagios: 

```apache
cd /usr/local/nagios/share/images
```
Luego, copiamos la URL de descarga de los íconos [F*Nagios Icon Pack](https://exchange.icinga.com/exchange/F*Nagios+icon+pack+(Status+Map+and+Host+View+icons)) y descargamos el recurso usando el comando wget: 


```apache
wget https://exchange.icinga.com/exchange/F%2ANagios%20icon%20pack%20%28Status%20Map%20and%20Host%20View%20icons%29/files/20/FNagios.zip
```

Habremos descargado un paquete con extensión `.zip`. Nos aseguramos que disponemos de la herramienta para descomprimirlo ejecutando: 

```apache
sudo apt install -y unzip
```

Luego, procedemos a descomprimir el archivo `.zip`:

```apache
sudo unzip FNagios.zip
```

¡Listo! Tendremos el paquete de íconos [F*Nagios Icon Pack](https://exchange.icinga.com/exchange/F%2ANagios%20icon%20pack%20%28Status%20Map%20and%20Host%20View%20icons%29/files/22/preview.jpg) listo para usar. 


## Creando nuestro propios íconos
Nagios aloja las imágenes (iconos) que se mostrarán en el mapa en `/usr/local/nagios/share/images`. 

Dentro de esta carpeta debemos copiar nuestros iconos personalizados. 

Al crear nuestra imagen deberemos tener en cuenta el formato de la misma:

* Medida: 40x40 píxeles
* Formato: png (transparente)

Luego tendremos que convertir la imagen al resto de los formatos, todos necesarios para una correcta visualización del mapa de Nagios, a saber: `.gif` y `.gd2`. Por ejemplo, suponiendo que nuestra imagen se llama _iconoPersonalizado_, deberíamos tener al final tres versiones de la misma:  

* iconoPersonalizado.png
* iconoPersonalizado.gd2
* iconoPersonalizado.gif

### Instalando las herramientas 
Para poder realizar la conversión de formato de imágenes necesitaremos instalar las siguientes herramientas: `imagemagick`, `libgd-tools` y `netpbm`.

```bash
sudo apt -y install libgd-tools netpbm imagemagick
```

### Convirtiendo .png a .gd2

Para convertir una imagen `.png` a un icono `.gd2` ejecutamos el siguiente comando:

```bash
pngtogd2 iconoPersonalizado.png iconoPersonalizado.gd2 1 1
```

Donde _iconoPersonalizado.png_ es la imagen  `.png` que deseamos convertir e _iconoPersonalizado.gd2_ es el nombre de la imagen convertida en formato `.gd2`. El parámetro `1 1` indica que la conversión debe crearse en formato raw (crudo), y que el archivo debe crearse sin compresión.

### Convirtiendo .png a .gif
Para realizar esta conversión, simplemente ejecutamos: 

```bash
convert iconoPersonalizado.png iconoPersonalizado.gif
```

### Redimensionando nuestro icono
Es probable que nuestro icono en formato `.png` no posea la medida requerida por Nagios (40x40 pixeles) ni sea transparente. En ese caso podemos salvar la cuestión ejecutando algunos comandos en la terminal.

Convertimos la imagen al formato _netpbm_ (pnm - portable anymap format):

```bash
pngtopnm iconoPersonalizado.png > iconoPersonalizado.pnm 
```

Finalmente, redimensionamos la imagen y tornamos el fondo transparente obteniendo un nuevo archivo `.png`: 

```bash
pnmtopng -transparent =rgb:00/00/00 -phys 40 40 1 iconoPersonalizado.pnm > iconoPersonalizado.png
```

