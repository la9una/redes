Existen dos maneras de instalar el software en nuestro servidor: empleando los paquetes ofrecidos por la distribución en sus repositorios oficiales o descargando el código fuente y compilándolo manualmente. Preferiremos la segunda opción, aunque se incluyen ambas explicaciones. 

## Instalación vía código fuente
Este procedimineto implicará la descargar del código fuente de cada uno de los componentes del software (core, plugins, etc) y luego compilarlo en nuestro server. 

Para este paso, se tomó como referencia la documentación Oficial:

* [Instalación de Nagios Core (Debian)](https://support.nagios.com/kb/article/nagios-core-installing-nagios-core-from-source-96.html#Debian)
* [Instalación de Nagios Plugins (Debian)](https://support.nagios.com/kb/article/nagios-plugins-installing-nagios-plugins-from-source-569.html#Debian)
* [Instalación de Nagios NRPE (Debian)](https://support.nagios.com/kb/article.php?id=515#Debian)


Instalaremos Nagios Core + Plugins + NRPE, empleando un script de instalación escrito en Bash.

### Máquina virtual

```bash
wget https://gitlab.com/-/snippets/3613579/raw/main/install_nagios_core_server_debian_12_qemu.sh
chmod +x install_nagios_core_server_debian_12_qemu.sh
sudo ./install_nagios_core_server_debian_12_qemu.sh
```

### Contenedor

```bash
wget https://gitlab.com/-/snippets/3611138/raw/main/install_nagios_core_server_debian_11_proxmox_lxc.sh
chmod +x install_nagios_core_server_debian_11_proxmox_lxc.sh
./install_nagios_core_server_debian_11_proxmox_lxc.sh
```

## Acceso a la WebGUI

Para configurar la contraseña de acceso del usuario administrador por defecto de Nagios (llamado "nagiosadmin"), tendremos que ejecutar el siguiente comando: 

```bash
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin
```

Es posible crear o utilizar cualquier otro nombre de usuario. Para ello, ejecute el comando anterior omitiendo la opción `-c`

Nagios posee una interfaz web que facilita la administración del servidor. La misma puede ser accedida desde un navegador introduciendo la dirección: `http://<ip-de-tu-servidor>/nagios`

Los datos de acceso serán:

  * **usuario**: nagiosadmin
  * **contraseña**: (la contraseña que definiste durante la instalación)

