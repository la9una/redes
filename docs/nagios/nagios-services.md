En este paso definiremos qué parámetros vamos a monitorear. Para mantener un orden, mantendremos los servicios en diferentes archivos según sistema operativo. 

=== "Equipos Linux"

    Abrimos el archivo que nombramos como `linux-services.cfg`:

    ```bash
    sudo nano /usr/local/nagios/etc/dispositivos/linux-services.cfg
    ```

    A continuación, incluimos en dicho archivo la configuración del equipo y servicios a monitorear:

    ```apache
    ###############################################################################
    #
    # DEFINICIÓN DE SERVICIOS - LINUX
    #
    ###############################################################################

    # Define el servicio ping

    define service {
        use                     generic-service           		; Plantilla a utilizar
        hostgroup_name          linux-hosts				        ; Nombre del grupo de hosts
        service_description     Ping				            ; Descripción del servicio
        check_command           check_ping!100.0,20%!500.0,60%	; Definición del comando
    }

    # Define el servicio de chequeo de disco

    define service {
        use                     generic-service           		
        hostgroup_name          linux-hosts				            
        service_description     Estado del disco			        
        check_command           check_local_disk!20%!10%!/		
    }

    # Define el servicio de monitoreo de cantidad de usuarios logueados
    # Se disparan las siguientes alertas, según cantidad de usuarios: 
    # 	- Warning si hay más de 20 usuarios
    # 	- Critical si hay más de 50 usuarios

    define service {
        use                     generic-service           		
        hostgroup_name          linux-hosts				            
        service_description     Usuarios logueados		        
        check_command           check_local_users!20!50		   
    }

    # Define el servicio que verifica el número de procesos actualmente activos. 
    # Se disparan las siguientes alertas: 
    #   - Warning si hay más de 250 procesos
    #   - Critical si hay más de 400 procesos

    define service {
        use                     generic-service                   
        hostgroup_name          linux-hosts                          
        service_description     Procesos              
        check_command           check_local_procs!250!400!RSZDT 
    }

    # Define el servicio de chequeo de carga del procesador

    define service {
        use                     generic-service                   
        hostgroup_name          linux-hosts                          
        service_description     Carga del CPU           
        check_command           check_local_load!5.0,4.0,3.0!10.0,6.0,4.0 
    }

    # Define el servicio de chequeo de memoria swap.
    # Se disparan las alertas: 
    #    - Warning, si menos del 20% de memoria swap está libre
    #    - Critical si menos del 10% de memoria swap está libre

    define service {
        use                     generic-service           
        hostgroup_name          linux-hosts
        service_description     Uso de Swap
        check_command           check_local_swap!20%!10%
    }

    # Define el servicio de chequeo de estado de SSH 
    # Notificaciones deshabilitadas por defecto. 

    define service {
        use                     generic-service
        hostgroup_name          linux-hosts
        service_description     Estado de SSH
        check_command           check_ssh
        notifications_enabled   0
    }
    ```

=== "Equipos Windows"

    Abrimos el archivo `windows-services.cfg`:

    ```bash
    sudo nano /usr/local/nagios/etc/dispositivos/windows-services.cfg
    ```

    E incluimos en dicho archivo la configuración del equipo y servicios a monitorear:

    ```apache
    ###############################################################################
    #
    # DEFINICIÓN DE SERVICIOS - WINDOWS
    #
    ###############################################################################

    # Crear un servicio para monitorear la versión de NSClient++ instalada

    define service {
        use                     generic-service                     ; Plantilla a utilizar
        hostgroup_name          windows-hosts                       ; Nombre del grupo de hosts 
        service_description     Versión de NSClient++               ; Descripción del servicio
        check_command           check_nt!CLIENTVERSION              ; Definición del comando
    }

    # Crear un servicio para monitorear el uptime del server

    define service {
        use                     generic-service
        hostgroup_name          windows-hosts
        service_description     Uptime
        check_command           check_nt!UPTIME
    }

    # Crear un servicio para monitorear la carga del CPU

    define service {
        use                     generic-service
        hostgroup_name          windows-hosts
        service_description     Carga del CPU
        check_command           check_nt!CPULOAD!-l 5,80,90
    }

    # Crear un servicio para monitorear el uso de RAM

    define service {
        use                     generic-service
        hostgroup_name          windows-hosts
        service_description     Uso de RAM
        check_command           check_nt!MEMUSE!-w 80 -c 90
    }

    # Crear un servicio para monitorear el uso del disco C:\

    define service {
        use                     generic-service
        hostgroup_name          windows-hosts
        service_description     Estado del disco C
        check_command           check_nt!USEDDISKSPACE!-l c -w 80 -c 90
    }

    # Crear un servicio para monitorear el servicio Workstation

    define service {
        use                     generic-service
        hostgroup_name          windows-hosts
        service_description     Servicio Workstation
        check_command           check_nt!SERVICESTATE!-d SHOWALL -l Workstation
    }

    # Crear un servicio para monitorear el proceso Explorer.exe

    define service {
        use                     generic-service
        hostgroup_name          windows-hosts
        service_description     Proceso Explorer
        check_command           check_nt!PROCSTATE!-d SHOWALL -l Explorer.exe
    }
    ```
 

Por último, tendremos que [verificar la configuración y reiniciar el servidor Nagios](nagios-configuration.md#verificando-la-configuracion-y-reiniciando-nagios) para guardar los cambios que hayamos introducido.

!!!warning "Servicios para hosts o para grupos de hosts"
	Los servicios que vamos a monitorear se pueden configurar para:

	**Un equipo en particular, mediante la directiva `host_name`**:
    
    ```apache hl_lines="3"
        define service {
        use                     generic-service
        host_name               my-windows-host
        service_description     Estado del disco C
        check_command           check_nt!USEDDISKSPACE!-l c -w 80 -c 90
    }
    ```

	**Un grupo de equipos empleando la directiva `hostgroup_name`**:

    ```apache hl_lines="3"
        define service {
        use                     generic-service
        hostgroup_name          windows-hosts
        service_description     Estado del disco C
        check_command           check_nt!USEDDISKSPACE!-l c -w 80 -c 90
    }
    ```


## Agregando servicios y procesos a monitorear en MS Windows
Es posible agregar servicios y procesos que deseemos monitorear en una máquina con SO Microsoft Windows


=== "Agregar servicios"
    Para agregar otros servicios de Windows a monitorear (para determinar, por ejemplo, si el servicio se está ejecutando o no) la Consola de Servicios de Windows, abriendo el diálogo _Ejecutar_, al cual se accede pulsando las teclas <kbd>WIN</kbd> + <kbd>R</kbd>. En dicho cuadro de diálogo, escribimos `services.msc`

    ![Servicios Windows](imgNagios/services_01.png)

    Luego, seleccionamos el servicio a monitorear. Por ejemplo, el **Cliente DHCP**. Hacemos clic derecho sobre el mismo y seleccionamos _Propiedades_ del menú contextual

    ![Servicios Windows](imgNagios/services_05.png)

    Tendremos que copiar el nombre del servicio **tal cual**, es decir, respetando las mayúsculas y minúsculas. En este caso, `Dhcp`.

    ![Servicios Windows](imgNagios/services_06.png)

    Por último, tendremos que definir el monitoreo del mismo en el archivo `servicios.cfg`, donde sólo tendremos que escribir el nombre del servicio tal cual, modificando la última palabra de la última línea. Por ejemplo, para el servicio `Dhcp`: 

    ```apache
    # Verificando funcionamiento del Cliente DHCP
    define service{
    use						generic-service
    hostgroup_name			windows-hosts
    service_description		Servicio: Cliente DHCP
    check_command			check_nt!SERVICESTATE!-d SHOWALL -l Dhcp
    } 
    ```

    !!! tip "Listando servicios empleando CLI"

        Podemos listar los servicios del sistema operativo, desde CLI, sea a través de _cmd_ o _PowerShell_. En cualquier caso, abrimos el diálogo "Ejecutar" presionando las teclas <kbd>WIN</kbd> + <kbd>R</kbd> y colocamos en dicho espacio `cmd` ó `powershell`, según corresponda.  
        
        Una vez abierta la terminal, ejecutamos el siguiente comando para obtener los servicios:
        
        * En _cmd_: `sc queryex type=service state=all`
        * En _PowerShell_: `get-service`  

=== "Agregar procesos"
    Podemos obtener el nombre de los procesos que se encuentran corriendo en Windows accediendo al "Administrador de Tareas de Windows": 

    * Presionando la combinación de teclas <kbd>CTRL</kbd> + <kbd>SHIFT</kbd> + <kbd>ESC</kbd>
    * Abriendo la ventana "Ejecutar" presionando las teclas <kbd>WIN</kbd> + <kbd>R</kbd> y escribiendo `taskmgr`. 

    Una vez identificado el nombre del proceso a monitorear, tendremos que explicitarlo en la última línea de un bloque `define service`, dentro del archivo `servicios.cfg`. Por ejemplo, para el proceso `powershell.exe`:

    ```apache
    # Verificando proceso PowerShell
    define service {
        use                     generic-service
        host_name               my-windows-host
        service_description     Windows PowerShell
        check_command           check_nt!PROCSTATE!-d SHOWALL -l powershell.exe
    } 
    ```

    !!! tip "Listando procesos empleando CLI"

        Podemos listar los procesos del sistema operativo, desde CLI, sea a través de _cmd_ o _PowerShell_. En cualquier caso, abrimos el diálogo "Ejecutar" presionando las teclas <kbd>WIN</kbd> + <kbd>R</kbd> y colocamos en dicho espacio `cmd` ó `powershell`, según corresponda.  
        
        Una vez abierta la terminal, ejecutamos el siguiente comando para obtener los procesos:
        
        * En _cmd_: `tasklist`
        * En _PowerShell_: `get-process`  