# Cálculo de redes
Antes de comenzar a administrar dispositivos finales de red, es preciso comprender cómo funciona el direccionamiento IPv4. 


!!!tip "Recursos"
    Material útil para entender el direccionamiento IP
    
    - [Presentación de Google](https://docs.google.com/presentation/d/1malAF-yTySELmGdjqKAGq-4-gI8ISnFOU_v3_5qHvQ0/edit?usp=sharing)
    - [Calculadora de redes](https://arcadio.gq/index.html)


## Tabla de cálculo - Direccionamiento IPv4
Frente a cualquier situación de cálculos de direccionamiento IPv4 nos será de gran utilidad tener a mano una tabla como la que sigue:


<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/subnetting_table_redes.rauljesus.xyz.png" alt="Tabla de Subneteo" style="width: 80%;
  height: auto;">
  <figcaption>Tabla de subneteo</figcaption>
</div>

## Procedimiento para calcular subred
1. Ubicar la máscara CIDR en la tabla
Identificar octeto crítico, salto y máscara decimal.

2. Calcular la Network ID
Poner en 0 el octeto crítico y los siguientes en la IP original (hacia la derecha).

3. Identificar la subred correcta
Listar redes sumando el salto hasta encontrar la que contiene la IP.

4. Calcular datos finales:
    * Siguiente subred = Network ID + salto
    * Broadcast = IP anterior a la siguiente subred
    * Pool utilizable = Desde Network ID +1 hasta Broadcast -1

5. Ejemplo (192.168.1.25/27)
    * Máscara decimal: 255.255.255.224
    * Network ID: 192.168.1.0
    * Siguiente subred: 192.168.1.32
    * Broadcast: 192.168.1.31
    * Rango utilizable: 192.168.1.1 - 192.168.1.30


## FLSM (Fixed-Length Subnet Masking) 
FLSM (Máscara de Subred de Longitud Fija) significa que todas las subredes de una red tienen el mismo tamaño y la misma máscara.

### 1. Determinar subred
Calcular la [subred a la que pertenece la dirección IP dada](#procedimiento-para-calcular-subred)

### 2. Dividir la red en bloques de tamaño fijo

Las subredes se generan **sumando el salto** en el octeto correspondiente.

### 3. Ejemplo 
Partiendo de la IP 192.168.1.0/26: 

* Máscara decimal: 255.255.255.192
* Salto: 64
* Subredes FLSM:
    - Red 0: 192.168.1.0
    - Red 1: 192.168.1.64
    - Red 2: 192.168.1.128
    - Red 3: 192.168.1.192

Cada subred tiene 64 direcciones, con 62 utilizables (descontando network ID y broadcast).
FLSM se usa cuando todas las subredes deben tener el mismo tamaño, como en redes estructuradas de empresas.

## VLSM (Variable-Length Subnet Masking)
VLSM (Máscara de Subred de Longitud Variable) permite usar diferentes tamaños de subred dentro de la misma red, optimizando direcciones IP.

### 1. Ordenar subredes según cantidad de hosts
Clasificar las necesidades de cada subred, ordenándolas de manera descendente tomando en cuenta la cantidad de hosts. 

### 2. Determinar subred
Calcular la [subred a la que pertenece la dirección IP dada](#procedimiento-para-calcular-subred)

### 3. Calcular las máscara de subred de cada subred
Elegir una máscara CIDR según la cantidad de subredes necesarias.

#### Ejemplo
* Para una red de 60 hosts se necesitan 6 bits, porque $2^6-2=62$
* Para obtener la máscara CIDR, restamos la cantidad de bits a 32: $32-6=26$
* Por tanto, la máscara de subred será /32

### 4. Realizar cálculos pertinentes para las subredes
Para cada subred, se deberán realizar los cálculos correspondientes. 

#### a. Ubicar la máscara en la tabla
Obtener salto y máscara decimal.

#### b. Calcular la Network ID
Poner en 0 el octeto crítico y los siguientes en la IP original (hacia la derecha).

#### c. Identificar la subred correcta
Listar redes sumando el salto hasta encontrar la que contiene la IP.

### 5. Ejemplo 
Partiendo de la IP 192.168.1.0/24 y con las siguientes necesidades de hosts:

* Red 0: 50 hosts → /26 (64 IPs, salto 64) → 192.168.1.0/26
* Red 1: 20 hosts → /27 (32 IPs, salto 32) → 192.168.1.64/27
* Red 2: 10 hosts → /28 (16 IPs, salto 16) → 192.168.1.96/28
* Red 3: 2 hosts → /30 (4 IPs, salto 4) → 192.168.1.112/30

VLSM permite aprovechar mejor el espacio IP, evitando desperdiciar direcciones como en FLSM.