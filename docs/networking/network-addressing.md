# Cálculo de redes
Antes de comenzar a administrar dispositivos finales de red, es preciso comprender cómo funciona el direccionamiento IPv4. 



## Tabla de cálculo - Direccionamiento IPv4
Frente a cualquier situación de cálculos de direccionamiento IPv4 nos será de gran utilidad tener a mano una tabla como la que sigue:

| $2^x$            | $2^7$   | $2^6$   | $2^5$   | $2^4$   | $2^3$   | $2^2$   | $2^1$   | $2^0$   |
| ---------------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- |
| **Saltos**       | 128     | 64      | 32      | 16      | 8       | 4       | 2       | 1       |
| **Octeto crítico** | 128     | 192     | 224     | 240     | 248     | 252     | 254     | 255     |
| **1º Octeto**    | 1       | 2       | 3       | 4       | 5       | 6       | 7       | 8       |
| **2º Octeto**    | 9       | 10      | 11      | 12      | 13      | 14      | 15      | 16      |
| **3º Octeto**    | 17      | 18      | 19      | 20      | 21      | 22      | 23      | 24      |
| **4º Octeto**    | 25      | 26      | 27      | 28      | 29      | 30      | 31      | 32      |


## Problema de ejemplo
Dada la dirección IP `192.168.3.51/24`, calcular su dirección IP de red (network ID), dirección IP de broadcast, _pool_ de direcciones IP utilizables y valor de la máscara en formato decimal. 

## Solución del problema de ejemplo
Para resolver el problema, tendremos en cuenta lo siguiente: 

* Identificaremos el valor numérico de la máscara de subred de la IP dada, en formato CIDR 
* Ubicaremos dicho valor de máscara en la _tabla de cálculo de direccionamiento IPv4_ 

En nuestro ejemplo, tenemos que: 

    24          # valor de máscara de red en formato CIDR
    
Al ubicar dicho valor en la tabla, ésta nos devuelve los siguientes datos: 

    3º Octeto   # octeto crítico
    255         # valor del octeto crítico en la máscara de subred
    1           # saltos de red ó magic number

Con dicha información, comenzaremos la resolución del problema. 

#### 1. Cálculo de la dirección de red

**a. Escribimos la dirección IP original**

    192.168.3.51

**b. Calculamos la Subred "0"**

Tomamos la IP original y la "ponemos a cero" a partir del octeto crítico en adelante. Como en nuestro caso, el octeto crítico es el 3º, tenemos: 

    192.168.3.51    # IP original
    192.168.X.51    # El octeto crítico es el 3º
    192.168.0.0     # Podemos los octetos a 0 a partir del octeto crítico (inclusive). 
    
La dirección IP obtenida en este paso es la "Subred 0" para la dirección IP con la máscara de red planteada en el problema.  

**c. Creamos posibles subredes hasta encontrar la subred a la que pertenece la IP original**

Tomamos la "Subred 0" y sumamos el valor del salto obtenido al octeto crítico (en nuestro caso, el 3º):  

    Subred 0    192.168.0.0         
    Subred 1    192.168.1.0  # +1
    Subred 2    192.168.2.0  # +1
    Subred 3    192.168.3.0  # +1
    Subred 4    192.168.4.0  # +1
    ...

Podemos inferir que la IP original `192.168.3.51` se encuentra en la "Subred 3". En este caso, la _Subred 3_ es la _dirección de red (ó Network ID)_ de la dirección IP en cuestión. 

#### 2. Cálculo de la máscara de subred en formato decimal
Para calcular el valor de la máscara, partiremos del concepto de diseño de una dirección IPv4, conformada por 4 octetos separados por un punto. Es decir: 

    x.x.x.x

Habiendo identificado el "octeto crítico" en la _tabla de direccionamieto_: 

- En la posición del octeto crítico, pondremos el valor dado en la tabla para la máscara.
- Rellenamos con "0" a la derecha del octeto crítico.
- Rellenamos con "255" a la izquierda del octeto crítico. 

En nuestro caso sabemos que el octeto crítico se ubica en el 3º octeto, según la tabla. Por tanto: 

    x.x.x.x         # Formato IPv4
    x.x.255.x       # Reemplazamos el O.C. con el valor de máscara
    x.x.255.0       # Rellenamos con "0" a la derecha del O.C.
    255.255.255.0   # Rellenamos con "255" a la izquierda del O.C.

El valor de la máscara de red para nuestro caso (/24) es `255.255.255.0`.


#### 3. Cálculo de la configuración de la red
Habiendo obtenido la _dirección de red (ó Network ID)_ y la máscara de red, podremos estimar el resto de los valores relativos a dicha red:  

| Elemento                         | Descripción                                                  |
| -------------------------------- | ------------------------------------------------------------ |
| Dirección IP de red (network ID) | - Es la primer dirección IP de la red <br />- No puede asignarse a dispositivos |
| Dirección IP de broadcast        | - Es la última dirección IP de la red<br />- Se obtiene restando 1 IP a la siguiente dirección de red<br />- No puede asignarse a dispositivos |
| Primera dirección IP utilizable  | - Se obtiene sumando 1 IP a la dirección de red<br />- Esta IP y las siguientes del _pool_ pueden asignarse a dispositivos |
| Última dirección IP utilizable   | - Se obtiene restando 1 IP a la dirección de red<br />- Esta IP y las anteriores del _pool_ pueden asignarse a dispositivos |      


En nuestro caso, dichos valores son: 

    255.255.255.0   # Máscara (decimal)
    /24             # Máscara (CIDR)
    192.168.3.0     # Dirección de red (Network ID)
    192.168.3.1     # Primera IP utilizable
    192.168.3.254   # Última IP utilizable 
    192.168.3.255   # IP de Broadcast



## FLSM (Fixed Length Subnet Mask)
Dividir una red en subredes más pequeñas, con la particularidad que estas subredes tendrán la misma máscara entre sí. 

**¿Cuántas subredes puedo crear con 1 bit?** Puedo crear 2 subredes, porque: 

$$
2^1 = 2 \enspace Redes
$$

**¿Cuántos bits necesito para crear 8 subredes?** Necesito 3 bits, porque: 

$$
2^3 = 8 \enspace Redes
$$

### EJEMPLO
Se tiene la dirección IP `10.11.12.13/10` y se necesitan crear 3 subredes. Calculá cuáles serán dichas subredes. 

#### 1. Calculamos la dirección de red de la dirección IP original

a. Escribimos la IP original:

    10.11.12.13/10

b. Obtenemos datos de la tabla de _subnetting_

Para ello, ubicamos el valor de la máscara de red (CIDR) en la tabla de _subnetting_. En nuestro caso, se trata de `/10`. Al ubicar dicho valor en la tabla de subnetting, obtenemos: 

    2º Octeto   # octeto crítico
    192         # valor del octeto crítico en la máscara de subred
    64          # saltos de red ó magic number

c. Calculamos la Subred "0"

Habiendo identificado el octeto crítico, tomamos la IP original y la "ponemos a cero" a partir del octeto crítico en adelante. Como en nuestro caso, el octeto crítico es el 2º, tenemos: 

    10.11.12.13 # IP original
    10.X.12.13  # El octeto crítico es el 2º
    10.0.0.0    # Esta red siempre es la subred "0"

d. Creamos posibles subredes hasta encontrar la subred a la que pertenece la IP original

Tomamos la "subred 0" y sumamos el valor del salto obtenido al octeto crítico:  

    Subred 0   10.0.0.0         
    Subred 1   10.64.0.0   # +64
    Subred 2   10.128.0.0  # +64
    ...

Podemos inferir que la IP original `10.11.12.13` se ecuentra en la "Subred 0". En este caso, la _Subred 0_ es la _dirección de red (ó Network ID)_ de la dirección IP en cuestión. Pues entonces, a partir de esta subred, comenzaremos el _subnetting_ en el próximo paso. 


#### 2. Calculamos el valor de máscara de la subred 0

a. Nos preguntamos, ¿cuántos bits necesitamos para crear 3 subredes? Necesitamos 2 bits. Porque: 
    
$$
2^2 = 4 \enspace Redes
$$ 
    
b. Tomamos la dirección de red y la máscara de red de la dirección IP de punto de partida del ejercicio (es decir `10.0.0.0/10`) y realizamos la siguiente operación: 
   
    NETWORK ID/MÁSCARA ORIGINAL + BITS DE RED -> RED 0 / NUEVA MÁSCARA
        
Es decir, para nuestro ejemplo: 
    
    10.0.0.0/10 + 2     # Sumamos a la máscara en formato CIDR, los bits calculados 
    10.0.0.0/12         # Obtenemos la primera subred ó Red 0, con la nueva máscara

4. Buscamos el valor de la nueva máscara de subred (CIDR) en la tabla de _subnetting_
En nuestro caso es 12. Si ubicamos dicho número en la tabla de subnetting, obtenemos: 

* 2º Octeto   (octeto crítico)
* 240         (valor del octeto crítico en la máscara de subred)
* 16          (saltos de red ó magic number)

5. Comenzamos el subneteo

Tomamos el valor de la red 0 juntos con los datos obtenidos en el punto anterior y comenzamos a subnetear. 


    RED 0

    10.0.0.0/12     # Dirección de red (Network ID)
    10.0.0.1        # Primera IP utilizable
    10.15.255.254   # Última IP utilizable 
    10.15.255.255   # IP de Broadcast
    255.240.0.0     # Máscara (decimal)
    
    RED 1

    10.16.0.0/12    # Dirección de red (Network ID)
    10.16.0.1       # Primera IP utilizable
    10.31.255.254   # Última IP utilizable 
    10.31.255.255   # IP de Broadcast
    255.240.0.0     # Máscara (decimal)

    NET ID      10.16.0.0/12
    BROADCAST   10.31.255.255   
    FIRST IP    10.16.0.1
    LAST IP     10.31.255.254
    MASK DEC    255.240.0.0
    
    RED 2   
        NET ID      10.32.0.0/12
        BROADCAST   10.47.255.255
        FIRST IP    10.32.0.1
        LAST IP     10.47.255.254
        MASK DEC    255.240.0.0
    
    RED 3   
        NET ID      10.48.0.0/12            
        BROADCAST   10.63.255.255                        
        FIRST IP    10.48.0.1
        LAST IP     10.63.255.254
        MASK DEC    255.240.0.0
    
    RED 4
        NET ID      10.64.0.0/12
