# Configuración de firewall en Microsoft Windows
Podemos gestionar la configuración de red de Windows de manera gráfica (el modo más habitual) o empleando comandos de terminal.

## Añadir regla de entrada
En este ejemplo, agregaremos una regla de entrada en el Firewall para permitir a todos los programas el uso del protocolo ICMP (relacionado con el comando `ping`).

Para abrir el Firewal de Windows presionamos las teclas <kbd>Win</kbd> + <kbd>R</kbd> y se abrirá la ventana "Ejecutar". En ella escribimos `wf.msc`. 

Una vez abierto el Firewall, procedemos a crear la regla de entrada. 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/wf_icmp_00.png" alt="Windows Firewall" style="width: 100%;
  height: auto;">
  <figcaption>Seleccionamos en "Reglas de entrada" la opción "Nueva regla..."</figcaption>
</div>

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/wf_icmp_01.png" alt="Windows Firewall" style="width: 100%;
  height: auto;">
  <figcaption>El tipo de regla que vamos a crear será "Personalizada" (Custom)</figcaption>
</div>

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/wf_icmp_02.png" alt="Windows Firewall" style="width: 100%;
  height: auto;">
  <figcaption>Aplicamos la regla a todos los programas instalados en el sistema</figcaption>
</div>

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/wf_icmp_03.png" alt="Windows Firewall" style="width: 100%;
  height: auto;">
  <figcaption>En protocolo elegimos "ICMPv4"</figcaption>
</div>

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/wf_icmp_04.png" alt="Windows Firewall" style="width: 100%;
  height: auto;">
  <figcaption>Aplicamos la regla tanto en direcciones IP locales como remotas</figcaption>
</div>

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/wf_icmp_05.png" alt="Windows Firewall" style="width: 100%;
  height: auto;">
  <figcaption>Permitimos las conexiones como norma general</figcaption>
</div>

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/wf_icmp_06.png" alt="Windows Firewall" style="width: 100%;
  height: auto;">
  <figcaption>Indicamos que la misma se aplique en todos los ámbitos</figcaption>
</div>

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/wf_icmp_07.png" alt="Windows Firewall" style="width: 100%;
  height: auto;">
  <figcaption>Elegimos un nombre y descripción para identificar nuestra regla</figcaption>
</div>

