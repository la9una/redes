# Configuración de interfaces de red en GNU/Linux

En GNU/Linux disponemos de multitud de herramientas gráficas para configurar la red. Sin embargo, podemos realizar las mismas configuraciones desde la terminal, sobre todo, si estamos frente a un terminal que no posee entorno gráfico. 

!!! warning "Gestión de la red en GNU/Linux"

    En entornos de escritorio, en la mayoría de los casos, la gestión de la red se realiza mediante [Network Manager](https://es.wikipedia.org/wiki/NetworkManager). 

	Para configuración manual de la red mediante [CLI](https://es.wikipedia.org/wiki/Interfaz_de_l%C3%ADnea_de_comandos), se debe editar el archivo:
	
	*  `/etc/network/interfaces` o bien,
	*  `/etc/netplan/<archivo-configuracion>.yml` (en versiones recientes)

	En esta guía, abordaremos la configuración de la red de modo manual, empleando la terminal de comandos. 

## `/etc/network/interfaces`
Siempre hablando de distribuciones Linux basadas en Debian, el archivo histórico donde encontramos la configuración de red. 

Para comenzar con la configuración de red, editaremos el archivo en cuestión con permiso de superusuario:

```bash
sudo vim /etc/network/interfaces
```

Luego, dependiendo si deseamos configurar direccionamiento estático o dinámico, variará el contenido del mismo. 

!!! warning "Desactivar Network Manager"
	En el caso de que estemos trabajando en un sistema con entorno gráfico de escritorio, es posible que necesitemos deshabilitar el gestor **Network Manager**: 

	```bash
	systemctl stop NetworkManager.service && systemctl disable NetworkManager.service
	```
	Habremos deshabilitado la configuración de red activa y procederemos a configurarla manualmente.

### IP dinámica

En el archivo `/etc/network/interfaces` la sintaxis general será:

```bash linenums="1"
# Interfaz de red local
auto lo
iface lo inet loopback

# Interfaz de red
auto <Nombre-de-la-interfaz-de-red>
iface <Nombre-de-la-interfaz-de-red> inet dhcp
```
Por ejemplo, para setear la IP dinámica de la interfaz de red llamada `eth0`:

```bash linenums="1" hl_lines="6 7"
# Interfaz de red local
auto lo
iface lo inet loopback

# Interfaz de red
auto eth0
iface eth0 inet dhcp
```

### IP estática
En este caso, al abrir el archivo `/etc/network/interfaces` la sintaxis general será:


```bash linenums="1"
# Interfaz local
auto lo
iface lo inet loopback

# Interfaz de red
auto <Nombre-de-la-interfaz-de-red>
iface <Nombre-de-la-interfaz-de-red> inet static
	address <dirección-ip>
	network <dirección-de-red>
	netmask <máscara-de-red>
	gateway <dirección-ip-router>
	dns-nameservers <dirección-ip-servidor-dns>
```

Por ejemplo, para setear la IP estática de la interfaz de red llamada `eth0` y direccionamiento `192.168.0.0/24`: 

```apache linenums="1"  hl_lines="6 7 9 11 13 15 17"
# Interfaz local
auto lo
iface lo inet loopback

# Interfaz de red
auto eth0
iface eth0 inet static
	# Dirección del equipo
	address 192.168.0.222
	# Dirección de red
	network 192.168.0.0
	# Máscara de red
	netmask 255.255.255.0
	# Puerta de enlace predeterminada
	gateway 192.168.0.1
	# Servidor DNS
	dns-nameservers 8.8.8.8
```

**Reiniciar el demonio de red**

```bash
sudo /etc/init.d/networking restart
```

_O bien_

```bash
sudo service networking restart
```

## `/etc/netplan`
En versiones más recientes de distribuciones Linux que adopataron [systemd](https://es.wikipedia.org/wiki/Systemd) se emplea `netplan` en lugar del archivo  `/etc/network/interfaces`. 

El nombre del archivo de configuración es variable, según la distribución Linux de la que se trate. Sin embargo, en todos los casos, se trata de un archivo con extensión [.yml](https://es.wikipedia.org/wiki/YAML) ubicado en el siguiente directorio: 

```bash
/etc/netplan/archivo-de-configuracion-de-la-red.yml
```
Veamos a continuación, el modo de empleo y configuración de la red empleando este sistema. 

### IP dinámica
La sintaxis general del archivo de configuración de red, será: 

```apache
network:
  version: 2
  renderer: networkd
  ethernets:
    <Nombre-de-la-interfaz-de-red>:
      dhcp4: true
```
Por ejemplo, para setear la IP dinámica de la interfaz de red llamada `eth0`: 

```apache
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      dhcp4: true
```

### IP estática
La sintaxis general del archivo de configuración de red, será: 

```apache
network:
    ethernets:
        <Interfaz-de-red>:
            dhcp4: false
            addresses: [<Direccion-IP>/<mASCARA-DE-RED>]
            nameservers:
              addresses: [<IP-dns-1>,<IP-dns-2>]
            routes:
            - to: default
              via: 192.168.1.100
    version: 2
```
Por ejemplo, para setear la IP estática de la interfaz de red llamada `enp0s3` y direccionamiento `192.168.1.0/24`: 

```apache
network:
    ethernets:
        enp0s3:
            dhcp4: false
            addresses: [192.168.1.202/24]
            nameservers:
              addresses: [8.8.8.8,8.8.4.4]
            routes:
            - to: default
              via: 192.168.1.100
    version: 2
```

## Aplicar los cambios
Sin importar que tipo de configuración de IP hayamos establecido, para aplicar los cambios de forma permanente tendremos que realizar algunos pasos adicionales que se listan a continuación. 

#### /etc/network/interfaces

**Deshabilitar la interfaz de red**

```bash
sudo ip link set <Nombre-de-la-interfaz-de-red> down
```
Ejemplo para la interfaz de red llamada `eth0`

```bash
sudo ip link set eth0 down
```

**Habilitar la interfaz de red**

```bash
sudo ip link set <Nombre-de-la-interfaz-de-red> up
```

Ejemplo para la interfaz de red llamada `eth0`

```bash
sudo ip link set eth0 up
```
#### /etc/netplan

**Validar la configuración de red**

```bash
sudo netplan try
```

**Aplicar la configuración de red**

```bash
sudo netplan apply
```

**Reiniciar resolución de nombres**

```bash
sudo systemctl restart systemd-resolved
```

## Ver configuración de red
A continuación listamos comandos para visualizar la configuración de red del equipo

**Mostrar las interfaces de red del equipo**
```bash
sudo ip a
```

**Ver los vecinos en la caché ARP**

```bash
sudo ip ne
```

## Configurando Proxy de la red
Si nuestro equipo se encuentra detrás de un proxy, tendremos que realizar configuraciones adicionales. 

1. Crear un archivo vacío dentro de `/etc/apt` llamado `apt-conf`:

```bash
sudo vi /etc/apt/apt.conf
```

2. Ingresar el siguiente contenido: 

```bash linenums="1"
Acquire::http::Proxy "http://<direccion-IP-del-proxy>:<puerto>";
```

!!!warning "Proxy con autenticación / seguro"
		En caso que necesitamos autenticarnos en el servidor proxy, cambiaremos la línea 
		
		`http://<direccion-IP-del-proxy>:<puerto>` 
		
		por 
		
		`http://<usuario>:<contraseña>@<direccion-IP-del-proxy>:<puerto>` 
		
		Si se trata de un servidor seguro, simplemente cambiaremos `http` por `https`