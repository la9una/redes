# Configuración de red en Microsoft Windows
Podemos gestionar la configuración de red de Windows de manera gráfica (el modo más habitual) o empleando comandos de terminal.

En esta guía, se abordará la configuración de interfaces de red IPv4.

## Configurar red de modo gráfico
Para abrir las opciones de configuración de red, combinamos las teclas ++win+r++ y escribimos en el cuadro de diálogo "Ejecutar" la palabra `ncpa.cpl`. 

??? tip "Archivos .cpl"

    Cada herramienta del panel de control está representada por un archivo .cpl (Control Panel Launcher) en la carpeta Windows\System de Windows. Los archivos .cpl se cargan automáticamente cuando se inicia el panel de control.

    | **ARCHIVO**      | **ACCIÓN**                             |
    | ---------------- | -------------------------------------- |
    | **appwiz.cpl**   | Programas y características            |
    | **bthprops.cpl** | Panel de control Bluetooth             |
    | **desk.cpl**     | Ajustes de pantalla                    |
    | **firewall.cpl** | Firewall de Windows                    |
    | **hdwwiz.cpl**   | Administrador de dispositivos          |
    | **inetcpl.cpl**  | Propiedades de internet                |
    | **intl.cpl**     | Configuración regional y de idioma     |
    | **irprops.cpl**  | Propiedades de infrarrojos             |
    | **joy.cpl**      | Dispositivos de juego                  |
    | **main.cpl**     | Mouse                                  |
    | **mmsys.cpl**    | Sonido                                 |
    | **ncpa.cpl**     | Conexiones de red                      |
    | **powercfg.cpl** | Configuración de energía de Windows    |
    | **sapi.cpl**     | Opciones de reconocimiento de voz      |
    | **sysdm.cpl**    | Nombre de equipo (opciones de sistema) |
    | **tabletpc.cpl** | Lápiz y entrada táctil                 |
    | **telephon.cpl** | Conexión telefónica o vía modem        |
    | **timedate.cpl** | Fecha y hora de sistema                |
    | **wscui.cpl**    | Seguridad y Mantenimiento              |

Se abrirá una ventana con todas las conexiones de red (interfaces de red) disponibles en el equipo: 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/windows_network_connections.png" alt="Windows Network Connections" style="width: 100%;
  height: auto;">
  <figcaption>Interfaces de red</figcaption>
</div>

Haciendo doble clic sobre la interfaz deseada, se abrirá la ventana de estado de la interfaz: 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/windows_network_state.png" alt="Windows Network State" style="width: 50%;
  height: auto;">
  <figcaption>Estado de la red</figcaption>
</div>

Para acceder a las propiedades de la interfaz, en la ventana anterior, tendremos que clicar el botón **"Propiedades"**: 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/windows_network_properties.png" alt="Windows Network Propertiess" style="width: 50%;
  height: auto;">
  <figcaption>Propiedades de red</figcaption>
</div>

Para configurar las opciones de red IPv4, tendremos que hacer doble clic sobre la leyenda **"Habilitar el protocolo de Internet versión 4 (TCP/IPv4)"**. Se nos abrirá una ventana indicando la configuración IP de la interfaz, seteada en DHCP (por defecto): 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/windows_network_dhcp.png" alt="Windows Network DHCP" style="width: 50%;
  height: auto;">
  <figcaption>Configuración IP dinámica (DHCP)</figcaption>
</div>

No obstante, podremos configurar la dirección IP de manera estática, según el direccionamiento de nuestra LAN (en el ejemplo, 192.168.0.0/24): 

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/windows_network_static_ip.png" alt="Windows Network Static IP" style="width: 50%;
  height: auto;">
  <figcaption>Configuración IP estática</figcaption>
</div>


## Configurar red mediante CLI

[Netshell](https://es.wikipedia.org/wiki/Netsh) (netsh) es una utilidad de línea de comandos que ofrece varias opciones para la configuración de una red en sistemas Microsoft Windows. 

Algunos comandos útiles antes de comenzar: 

**Mostrar las interfaces de red del sistema**

```shell
netsh interface ipv4 show interface
```

**Resetear la configuración de red**

```shell
netsh interface ip reset
```

**Mostrar las direcciones IP de las interfaces de red**  

```shell
netsh interface ipv4 show address
```

**Mostrar los servidores DNS** 

```shell
netsh interface ipv4 show dns
```

### IP dinámica

**Setear IP dinámica**

Sintaxis general: 

```shell
netsh interface ipv4 set address <Nombre de la interfaz de red> dhcp
```
Por ejemplo, suponiendo que el nombre de la interfaz es "Ethernet":

```shell
netsh interface ipv4 set address "Ethernet" dhcp
```
**Setear servidores DNS**

Sintaxis general: 

```shell
netsh interface ipv4 set dnsservers <Nombre de la interfaz de red> dhcp
```
Por ejemplo, suponiendo que el nombre de la interfaz es "Wi-Fi":

```shell
netsh interface ipv4 set dnsservers "Wi-Fi" dhcp
```
### IP estática

**Setear IP estática** 

Sintaxis general:

```shell
netsh interface ipv4 set address <Nombre de la interfaz de red> static <StaticIP> <SubnetMask> <DefaultGateway>
```

Por ejemplo, suponiendo que el nombre de la interfaz es "Wi-Fi":

```shell
netsh interface ipv4 set address "Wi-Fi" static 192.168.0.40 255.255.255.0 192.168.1.1 
```

**Setear servidores DNS estáticos**

Sintaxis general: 

```shell
netsh interface ipv4 set dns <Nombre de la interfaz de red> 8.8.8.8 validate=no index=<Prioridad>
```

Por ejemplo, suponiendo que el nombre de la interfaz es "Wi-Fi", seteamos el servidor DNS primario:

```shell
netsh interface ipv4 set dns "Wi-Fi" 8.8.8.8 validate=no index=1
```
Y el servidor DNS secundario: 

```shell
netsh interface ipv4 set dns "Wi-Fi" 8.8.4.4 validate=no index=2 
```

### Más comandos netsh

**Mostrar ayuda de comandos**

```shell
netsh ?
```
**Mostrar ayuda de comandos según contexto**

```shell
netsh interface ?
```
**Exportar la configuración de red (ejemplo)**

```shell
netsh dump >> C:\Users\Raul\Desktop\configuracion-de-red.txt
```

**Restaurar configuración de red (ejemplo)**

```shell
netsh -f C:\Users\Raul\Desktop\configuracion-de-red.txt
```

**Mostrar la contraseña de un determinado perfil de Wi-Fi**

```
netsh wlan show profile name="Perfil" key=clear
```
**Más comandos para red Wi-Fi**

```
netsh wlan show profiles
```

```
netsh wlan show drivers
```

```
netsh wlan show wireless capabilities
```

```
netsh wlan show interfaces
```

## Ver configuración de red
Para visualizar el estado y configuración de la red del equipo, disponemos del entorno gráfico y sus herramientas. 

Sin embargo, existe un comando -propio de Windows- para consultar el estado de la red: **ipconfig**. 

### El comando ipconfig
A continuación, veamos el modo de empleo del comando **ipconfig** y algunas de sus opciones. 

**Mostrar  la configuración completa de todos los adaptadores de red del equipo**

```powershell
ipconfig /all
```

**Liberar la dirección IP asignada por DHCP**

```powershell
ipconfig /release
```

**Renovar una configuración de dirección IP asignada por DHCP**

```powershell
ipconfig /renew
```

**Vaciar la caché de la resolución DNS**
```powershell
ipconfig /flushdns
```

!!! help "Ayuda del comando `ipconfig`"

    Para conocer otras opciones del comando `ipconfig`, consultá la [guía oficial](https://learn.microsoft.com/es-es/windows-server/administration/windows-commands/ipconfig) en el sitio web de Microsoft.



