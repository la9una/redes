En la presente sección se desarrollan temas relacionados con fundamentos de redes de datos, así como la configuración básica de dispositivos de red de diferentes _vendors_

<div align="center"><img src="../imgNetworking/computer-network.svg" alt="Redes de datos"  width="60%" height="30%"></div>

Esperamos que esta información sea de utilidad para iniciarse en este fascinante mundo. 

