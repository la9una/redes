# NAT - Network Address Translation
Constituye una de las soluciones frente al [agotamiento de direcciones IPv4](https://es.wikipedia.org/wiki/Agotamiento_de_las_direcciones_IPv4). Por este motivo, en líneas generales, los [ISP](https://es.wikipedia.org/wiki/Proveedor_de_servicios_de_internet) suelen entregar sólo una (1) dirección pública por cliente. En entornos empresariales este número de direcciones puede ser mayor, bajo demanda, pero no suele superar la decena de direcciones IPv4. 

Por otro lado, para acceder a Internet se necesita una dirección IP pública, pero podemos usar una dirección IP privada en nuestra red privada. El mecanismo llamado NAT (Network Address Translation) ó Enmascaramiento de IP, permite que múltiples dispositivos accedan a Internet a través de **una sola dirección pública o un conjunto (pool) de ellas**. Para lograr esto, se requiere la traducción de una dirección IP privada a una dirección IP pública. NAT generalmente opera en un enrutador o firewall. 

## Tipos de NAT

<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/nat_types.jpg" alt="Tipos de NAT">
  <figcaption>Tipos de NAT</figcaption>
</div>

Existen varios tipos de NAT: 

* **Estática**: una dirección IP privada se traduce siempre en una misma dirección IP pública (1:1). Sólo tienen salida a internet aquellos hosts que tengan asociada una dirección IP pública (por ejemplo, si sólo hay 2 direcciones IP públicas, sólo dos hosts de la LAN podrán salir a internet)

* **Dinámica**: Se dispone de un pool de direcciones IP públicas. Una dirección IP privada se traduce en alguna dirección IP pública del pool, de manera aleatoria. (N:M). Cada vez que un host requiera una conexión a Internet, el router le asignará una dirección IP pública que no esté siendo utilizada. 

* **Sobrecargado**: La NAT con sobrecarga o **PAT** (Port Address Translation) es el más común de todos los tipos, ya que es el utilizado en los hogares. Se pueden mapear múltiples direcciones IP privadas a través de una dirección IP pública, con lo que evitamos contratar más de una dirección IP pública (N:1). Para poder hacer esto el router hace uso de los puertos. En los protocolos TCP y UDP se disponen de 65.536 puertos para establecer conexiones. 


<div style="margin:0 auto; text-align:center;" >
<img src="../imgNetworking/nat_pat.png" alt="PAT">
  <figcaption>Esquema de PAT (NAT con sobrecarga)</figcaption>
</div>