# Enrutamiento

Es un mecanismo presente en las redes de datos mediante el cual se produce una selección de rutas para la transmisión de información. El enrutamiento (_routing_ o encaminamiento) es clave ya que permite "organizar" el tráfico de red, minimizando los errores de congestión durante la trnasmisión de información entre diferentes nodos de una red. 

El enrutador es un dipositivo de red que conecta nodos de diferentes redes y determina, en cada conexión, cuál es la mejor ruta que deben seguir los datos desde un determinado origen hacia un determinado destino.

## Tipos
Existen dos maneras de enrutamiento, seǵun el modo de generar las tablas de enrutamiento (documento electrónico que almacena rutas hacia los nodos de una red)

* Estático
* Dinámico

El **enrutamiento estático** es definido manualmente por el administrador de red. Es útil en redes pequeñas, donde los cambios no suelen ser habituales y los valores de configuración de la red permanecenn constantes a lo largo del tiempo.

El **enrutamiento dinámico**, como su nombre lo indica, se ajusta en tiempo real según las cambiantes condiciones de la red. Esto es posible gracias a los protocolos de enrutamiento dinámico, que consisten en un conjunto de reglas que gestionan la tabla de enrutamiento dinámico. 

!!!info "Protocolos de enrutamiento dinámico"
    Los protocolos de enrutamiento dinámico se agrupan en dos categorías: 
    
    - **Protocolos de puerta de enlace interior** ([RIP](https://es.wikipedia.org/wiki/Routing_Information_Protocol) y [OSPF](https://es.wikipedia.org/wiki/Open_Shortest_Path_First)), que funcionan mejor en un sistema autónomo, es decir, en una red gestionada por una sola organización. Ejemplos: .
    - **Protocolos de puerta de enlace exterior** ([BGP](https://es.wikipedia.org/wiki/Border_Gateway_Protocol)), que gestionan mejor la transferencia de información entre dos sistemas autónomos.


## Tablas de enrutamiento

Los hosts TCP/IP utilizan una tabla de enrutamiento para mantener información sobre otras redes. En cada dispositivo de una red, existe una tabla de enrutamiento.

Para acceder a la tabla de enrutamiento de un equipo, se debe ejecutar cualquiera de estos comandos: `route print` o `netstat -r`

La tabla de enrutamiento contiene información sobre:

* los diferentes destinos posibles en la red (dirección ip destino)
* la máscara de red de dichos destinos
* la dirección ip del próximo salto, es decir, la dirección ip del siguiente dispositivo de red en el camino hacia el destino final 
y la interfaz de salida que se debe utilizar para enviar el paquete.

Estas tablas de enrutamiento pueden ser estáticas o dinámicas, dependiendo del modo en que son generadas, según se detalló en el punto anterior.

## Ejemplo de enrutamiento
Tomando en cuenta el siguiente esquema de red: 

<div style="margin:0 auto; text-align:center;" >
    <img src="../imgNetworking/routing-example.png" alt="Ejemplo de enrutamiento" style="width: 100%; height: auto;">
          <figcaption>Ejemplo de enrutamiento</figcaption>
</div>

Podemos apreciar que, por defecto:
 
* El **ROUTER 1** conoce cómo llegar a la **RED A** y a la **RED B**, pero no sabe cómo alcanzar  la **RED C**
* Por otro lado, el **ROUTER 2** conoce cómo llegar a la **RED B** y a la **RED C**, pero no sabe llegar a la **RED A**

Por lo tanto, si quisiéramos que el **EQUIPO 1** se comunique con el **EQUIPO 2** deberíamos ingresar manualmente una regla (ruta estática) en la tabla de enrutamiento del **ROUTER 1**, con la siguiente información: 

**ROUTER 1**

| Red destino | Máscara destino | Siguiente salto |
| ----------- | --------------- | --------------- |
| 10.0.2.0    | 255.255.255.0   | 192.168.100.2   |

 
Del mismo modo, si quisiéramos que el **EQUIPO 2** se comunique con el **EQUIPO 1** deberíamos ingresar manualmente una regla (ruta estática) en la tabla de enrutamiento del **ROUTER 2**, con la siguiente información: 

**ROUTER 2**

| Red destino | Máscara destino | Siguiente salto |
| ----------- | --------------- | --------------- |
| 10.0.1.0    | 255.255.255.0   | 192.168.100.1   |


## Ruta por defecto (Default route)
La ruta por defecto es una ruta estática que tiene como destino cualquier red posible. Si en la tabla de enrutamiento no hay ninguna ruta específica a la red de destino, entonces se utilizará esta ruta para enviar el paquete en cuestión (generalmente, hacia una red remota).


| Red destino | Máscara destino | Siguiente salto |
| ----------- | --------------- | --------------- |
| 0.0.0.0     | 0.0.0.0         | interfaz o ip de salida |
