# Listas de control de acceso
O por sus siglas en inglés ACL (access control list) consisten en una serie de reglas que tienen por finalidad organizar privilegios de acceso a recursos infromáticos. 

## Estructura

En toda ACL podemos encontrar instrucciones del tipo `permit` (permitir) o `deny` (denegar). Las ACLs son una herramienta potente para controlar el tráfico hacia y desde la red, aportando seguridad a una red. Se pueden configurar ACLs para todos los protocolos de red enrutada. 

Existen varios tipos de ACLs. Entre las más comunes, tenemos la **ACL estándar** y la **ACL extendida**.

  * Las **ACL estándar** se pueden utilizar para permitir o denegar el tráfico de direcciones IPv4 de origen únicamente. 
    
  * Las **ACL extendidas** filtran paquetes IPv4 según varios atributos: tipo de protocolo, dirección IPv4 de origen y de destino, Puertos TCP o UDP de origen y de destino, etc. 


!!!info "Máscara Wilcard"

    Las ACL de IPv4 incluyen el uso de máscaras wildcard. Una máscara wildcard es una cadena de 32 dígitos binarios que el router utiliza para determinar los bits de la dirección que debe examinar para encontrar una coincidencia.

    El cálculo de máscaras wildcard puede ser difícil. Un método abreviado es restar la máscara de subred a 255.255.255.255.

    Por ejemplo:

    <div style="margin:0 auto; text-align:center;" >
      <img src="../imgNetworking/calculo-wildcard.png" alt="PAT" style="width: 40%; height: auto;">
          <figcaption>Cálculo de máscara wildcard</figcaption>
    </div>