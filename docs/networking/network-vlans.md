#VLAN

Una **VLAN (Red de Área Local Virtual) es una forma de dividir una red física en múltiples redes lógicas o virtuales**. Cada VLAN actúa como su propia red independiente, aunque físicamente pueden compartir el mismo hardware de red.

## Preliminares
En una red **LAN**, los switches no separan dominios de broadcast. Por tanto, todos los dispositivos pertenecen a la misma subred: 

<div style="margin:0 auto; text-align:center; font-size: 0.6rem">
<img src="../imgNetworking/vlan_same_lan.gif" alt="Esquema LAN Tradicional" style="width: 95%; height: auto;">
  <figcaption>Esquema tradicional de una red LAN <br>(imagen: www.networkacademy.io)</figcaption>
</div>


Siguiendo con el esquema tradicional de una **LAN**, si quisieramos segmentar el tráfico de la red en redes diferentes tendríamos que aislar físicamente los switches:

<div style="margin:0 auto; text-align:center; font-size: 0.6rem">
<img src="../imgNetworking/vlan_separate_lans.gif" alt="Esquema LAN Tradicional" style="width: 95%; height: auto;">
  <figcaption>Esquema tradicional de una red LAN <br>(imagen: www.networkacademy.io)</figcaption>
</div>

La solución anterior es perfectamente válida, aunque poco escalable: en caso de tener que aumentar la cantidad de redes habría que adquirir más hardware. Esto supodría un problemas de costos a la vez que la dificultad en la gestión de la red iría en aumento. 


Aquí es donde las **VLANs** pueden ser de gran ayuda. Aprovechando la infraestructura de hardware existente, podemos segmentar el tráfico en múltiples redes (por ende, los dominios de broadcast), lo que nos proporciona numerosos beneficios adicionales:

<div style="margin:0 auto; text-align:center; font-size: 0.6rem">
<img src="../imgNetworking/vlan_simple.gif" alt="Esquema VLAN" style="width: 95%; height: auto;">
  <figcaption>Esquema simplificado de una VLAN<br>(imagen: www.networkacademy.io)</figcaption>
</div>

## Clasificación de las VLANs

Las VLAN se pueden clasificar de varias maneras según los criterios utilizados para asignar los dispositivos a las VLAN. Aquí están algunos de los tipos más comunes de VLAN:

- **VLAN por puerto (Port-based VLAN)**: En este tipo de VLAN, los puertos en un switch están asignados a una VLAN específica. Cada puerto pertenece a una única VLAN y los dispositivos conectados a ese puerto son miembros de esa VLAN.

- **VLAN por dirección MAC (MAC-based VLAN)**: En este tipo de VLAN, los dispositivos se asignan a una VLAN según su dirección MAC. Cuando un dispositivo se conecta al switch, el switch examina la dirección MAC del dispositivo y lo asigna automáticamente a una VLAN específica basada en una tabla de asignación preconfigurada.

- **VLAN por protocolo (Protocol-based VLAN)**: En este tipo de VLAN, los dispositivos se asignan a una VLAN según el protocolo de capa de aplicación utilizado en el tráfico de red. Por ejemplo, todo el tráfico de voz podría asignarse a una VLAN específica para la telefonía IP.

- **VLAN basada en políticas (Policy-based VLAN)**: En este tipo de VLAN, los dispositivos se asignan a una VLAN basada en políticas de red predefinidas. Estas políticas pueden incluir criterios como la dirección IP del dispositivo, el tipo de tráfico, el puerto de origen o destino, entre otros.


!!!done ""
    :fontawesome-solid-check: **El tipo de VLAN más empleado es el basado en puertos o _Port-based VLAN_**.

### Esquema visual de una VLAN basada en puertos
Como una imagen vale más que mil palabras, las siguientes ilustraciones ejemplifican el funcionamiento de una VLAN basada en puertos: 

<div style="margin:0 auto; text-align:center; font-size: 0.6rem">
<img src="../imgNetworking/vlan_ports_1.png" alt="VLAN basada en puertos" style="width: 95%; height: auto;">
  <figcaption>VLAN basada en puertos<br>(imagen: www.networkacademy.io)</figcaption>
</div>

<div style="margin:0 auto; text-align:center; font-size: 0.6rem">
<img src="../imgNetworking/vlan_ports_2.gif" alt="Tráfico aislado según VLAN" style="width: 70%; height: auto;">
  <figcaption>Tráfico aislado según VLAN<br>(imagen: www.networkacademy.io)</figcaption>
</div>

## Glosario
Listado de conceptos relacionados con la tecnología VLAN. 

???done "Videos explicativos sobre la tecnología VLAN"

    <div style="text-align:center; font-size:1.1rem;padding: 5px 0;">¿Qué es una VLAN?</div>
    <center><iframe width="560" height="315" src="https://www.youtube.com/embed/n8j9UcDsV2g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

    <div style="text-align:center; font-size:1.1rem;padding: 5px 0;">VLAN: Puertos troncales</div>
    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/rsaAiaAKCsc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </center>


### VLAN por defecto (o predeterminada)
Es la VLAN que viene configurada _por defecto_ en la mayoría de los switches del mercado, independientemente del _vendor_. Esta VLAN se usa automáticamente para el todo el tráfico gestionado por el switch si no se asigna a ninguna otra VLAN específica. En otras palabras, funciona como una especie de "reserva" para el tráfico que no tiene una VLAN asignada explícitamente.

!!!info "VLAN por defecto en la práctica"
    - La VLAN 1 suele ser la VLAN por defecto (todos los puertos de switch forman parte de la VLAN predeterminada después del arranque inicial de un switch sin configurar). 
    - Esta VLAN no se puede eliminar, pero es posible modificar su configuración como cambiar su nombre, desactivar puertos en dicha VLAN, etc. 
    - No es recomendable cursar tráfico de red sobre esta VLAN por cuestiones de seguridad. 

### Tráfico etiquetado (tagged)
El tráfico etiquetado es aquel en el que cada paquete de datos lleva una "etiqueta" que indica a qué VLAN pertenece. Estas etiquetas se utilizan en los puertos del switch en [modo trunk](#vlan-troncal-trunk-vlan-tagged-ports) para transportar datos entre diferentes VLANs. La "etiqueta" es de 4 bytes y se inserta en el encabezado de la trama de Ethernet original: 

<div style="margin:0 auto; text-align:center; font-size: 0.8rem">
<img src="../imgNetworking/802dotq.png" alt="Etiqueta 802.1Q" style="width: 95%; height: auto;">
  <figcaption>Inserción de etiqueta 802.1Q en una trama Ethernet</figcaption>
</div>

Donde: 

- **Type**: Campo de 16 bits que indica el tipo de trama y tiene un valor fijo de `0x8100` para las tramas Ethernet con etiqueta VLAN.
- **Priority**: Campo de 3 bits que se utiliza para asignar prioridades al tráfico VLAN. Se utiliza para implementar calidad de servicio (QoS) y priorizar el tráfico en la red. 
    - PCP 0 (Priority Code Point 0): Prioridad predeterminada asignada al tráfico que no requiere una prioridad específica.
    - PCP 1 (Priority Code Point 1): Se utiliza para indicar un nivel de prioridad de tráfico bajo. El tráfico marcado con PCP 1 puede ser considerado como menos importante o tener un menor nivel de prioridad en comparación con otros tipos de tráfico.
    - PCP 2 (Priority Code Point 2): Asignado a tráfico que requiere una prioridad moderada. Puede utilizarse para tráfico que no es crítico pero que puede beneficiarse de un nivel de prioridad superior al tráfico predeterminado.
    - PCP 3 (Priority Code Point 3): Se utiliza para indicar un nivel de prioridad medio-alto. El tráfico marcado con PCP 3 puede recibir una atención especial en términos de entrega y calidad de servicio en comparación con el tráfico marcado con prioridades más bajas.
    - PCP 4 (Priority Code Point 4): Asignado a tráfico que requiere una prioridad alta pero no la máxima. Puede utilizarse para aplicaciones o servicios que necesitan un nivel de prioridad elevado pero no crítico
    - PCP 5 (Priority Code Point 5): Se utiliza para indicar una prioridad alta, específicamente para tráfico de voz sobre IP (VoIP). Este nivel de prioridad se asigna típicamente al tráfico de voz para garantizar una calidad adecuada en las llamadas telefónicas.
    - PCP 6 (Priority Code Point 6): Asignado a tráfico que requiere la prioridad más alta después de la prioridad de voz. Puede utilizarse para aplicaciones o servicios críticos que necesitan un alto nivel de prioridad en la red.
    - PCP 7 (Priority Code Point 7): La prioridad más alta disponible, reservada para aplicaciones o servicios críticos que requieren un tratamiento prioritario absoluto. El tráfico marcado con PCP 7 tiene la máxima prioridad en la red.
- **Flag**:  Campo de 1 bit que se utiliza para indicar si la trama puede ser descartada (drop) en situaciones de congestión de la red.
- **VLAN ID**: Campo de 12 bits que especifica la VLAN a la que pertenece la trama. Cada VLAN tiene un identificador único y el campo VID se utiliza para identificar la VLAN de destino de la trama.


### Tráfico sin etiquetar (untagged)
Es el tráfico de red que carece de información de etiqueta VLAN. Esto significa que cuando una trama de datos atraviesa un puerto de red, no está marcada con una etiqueta que indique a qué VLAN pertenece. Los puertos del switch en [modo access](#vlan-de-acceso-access-vlan-untagged-ports) manejan este tipo de tráfico sin etiquetar.

### VLAN de acceso (Access VLAN / Untagged ports)
Es una VLAN que se asigna a un puerto en un switch y se usa para conectar dispositivos finales. Estos puertos que se conectan a dispositivos finales se denominan **puertos sin etiquetar** y solo se pueden configurar para una única VLAN.


#### VLAN de datos (Data VLAN)
Es una VLAN que se utiliza para transportar tráfico de datos, como el que enviamos cuando navegamos por internet, enviamos correos electrónicos o compartimos archivos. A veces a una VLAN de datos se la denomina "VLAN de usuario". 

!!!success "Separar el tráfico VLAN"
    Es una práctica común separar el tráfico de **voz** y de **administración** del tráfico de **datos**. 

#### VLAN de voz (Voice VLAN)
Es una VLAN específicamente configurada para el tráfico de voz en una red. Se utiliza para transportar llamadas de voz sobre IP (VoIP) y otros servicios de comunicación de voz.

#### VLAN administrativa (Management VLAN)
Una VLAN administrativa, también conocida como VLAN de gestión, tiene como principal cometido separar el tráfico de red relacionado con la administración de los dispositivos de red (como switches, routers, firewalls y puntos de acceso) del resto del tráfico de usuario. Esta separación permite gestionar la infraestructura de red de forma más segura y eficiente.

Se emplea para la gestión y administración de dispositivos de red, como switches o enrutadores, generalmente de manera remota empleando [SSH](https://es.wikipedia.org/wiki/Secure_Shell), [SNMP](https://es.wikipedia.org/wiki/Protocolo_simple_de_administraci%C3%B3n_de_red) o [Telnet](https://es.wikipedia.org/wiki/Telnet). 

### VLAN troncal (Trunk VLAN / Tagged ports)
Las VLANs Troncales no son estrictamente necesarias para el funcionamiento de una red basado en VLANs aunque, en la mayoría de los escenarios, es muy conveniente emplearlas.

Analicemos el siguiente caso donde se gestionan **VLANs sin enlace troncal**: 


<div style="margin:0 auto; text-align:center; font-size: 0.6rem">
<img src="../imgNetworking/vlan_without_trunk.gif" alt="VLANs sin enlace troncal" style="width: 95%; height: auto;">
  <figcaption>Gestión de VLANs sin enlace troncal<br>(imagen: www.networkacademy.io)</figcaption>
</div>

El esquema es perfectamente válido, pero tiene sus limitaciones. ¿Que ocurriría con la implementación si debemos gestionar 10 VLANs? Pues habría que emplear 10 cables diferentes para conectar ambos switches. Y la cuestión se complica más si el número de VLANs va en aumento. Esta claro que este tipo de implementación resulta poco práctica. 

Veamos ahora el caso donnde de gestionan **VLANs con enlace troncal**: 

<div style="margin:0 auto; text-align:center; font-size: 0.6rem">
<img src="../imgNetworking/vlan_with_trunk.gif" alt="VLANs con enlace troncal" style="width: 95%; height: auto;">
  <figcaption>Gestión de VLANs con enlace troncal<br>(imagen: www.networkacademy.io)</figcaption>
</div>

En el esquema anterior se conectan ambos switches mediante un único cable, empleando un **enlace troncal**. Este tipo de conexión entre switches permite el transporte de tráfico proviniente de múltiples VLANs. Los puertos que forman el enlace se conocen como **puertos etiquetados** porque el swtich aplica etiquetas a los paquetes enviados desde dichos puertos. 

!!!success "Protocolo dot1q"
    El protocolo [802.1Q, también conocido como dot1Q](https://es.wikipedia.org/wiki/IEEE_802.1Q), es un estándar [IEEE](https://es.wikipedia.org/wiki/Institute_of_Electrical_and_Electronics_Engineers) que define la forma en que se [etiqueta el tráfico](#trafico-etiquetado-tagged) de VLAN en redes Ethernet. Específicamente, el protocolo [802.1Q](https://es.wikipedia.org/wiki/IEEE_802.1Q) define la inserción de etiquetas VLAN en los encabezados de trama Ethernet, lo que permite la segmentación del tráfico en redes locales virtuales (VLANs). 


<div style="margin:0 auto; text-align:center; font-size: 0.6rem">
<img src="../imgNetworking/vlan_trunk.gif" alt="Reenvío de tramas a través de un enlace troncal VLAN" style="width: 95%; height: auto;">
  <figcaption>Reenvío de tramas a través de un enlace troncal VLAN<br>(imagen: www.networkacademy.io)</figcaption>
</div>

#### VLAN nativa
La VLAN nativa es una VLAN predeterminada en un **enlace troncal (trunk)** que se utiliza para gestionar el **tráfico sin etiquetar (untagged)**, es decir, el tráfico que no tiene una etiqueta VLAN.

<div style="margin:0 auto; text-align:center; font-size: 0.6rem">
<img src="../imgNetworking/vlan_native.gif" alt="VLAN Nativa" style="width: 95%; height: auto;">
  <figcaption>La VLAN nativa procesa el tráfico sin etiquetar <br>(imagen: www.networkacademy.io)</figcaption>
</div>

Cuando un switch recibe una trama **sin etiquetar** a través de un **enlace troncal** - es decir, con **puertos etiquetados**-, la envía a la **VLAN nativa** para procesarla y dirigirla adecuadamente en la red. 

!!!info "VLAN nativa en la práctica"
    - La VLAN nativa suele coincidir con la VLAN 1, en un switch sin configurar. 
    - Se recomienda emplear una VLAN diferente a la VLAN 1 como VLAN nativa, por cuestiones de seguridad. 
