# VPCS
[Virtual PC Simulator (VPCS)](https://github.com/GNS3/vpcs) es un programa escrito por Paul Meng, que le permite simular una PC liviana compatible con DHCP y ping. Consume solo 2 MB de RAM por instancia y no requiere una imagen adicional.

## Configuraciones básicas

### Nombre de host
```apache
set pcname <name>
```
Donde `<name>` es el nombre de host

### Servidor DNS
```apache
ip dns <dns-server>
```
Donde `<dns-server>` es la dirección IP del servidor DNS

### Guardar cambios
```apache
save
```

## Direccionamiento

### Mostrar IP 
```apache
show ip
```

### IP fija (ip estática)
```apache
ip <ip-address> <subnetmask> <default-gateway>
```
Donde: 

* `<ip-address>` Es la dirección IP del equipo
* `<subnetmask>` Es la máscara de red
* `<default-gateway>` Es la dirección IP de la puerta de enlace predeterminada

### IP por DHCP (ip dinámica)
```apache
ip dhcp
```

### Eliminar IP
```apache
clear ip
```

## Verificaciones de red

### Ping
```apache
ping <ip-address>
```

### TCP Ping
```apache
ping <ip-address> -P 6 -p <port>
```
Donde `<port>` es el puerto TCP (el número 6 identifica a TCP en el programa)

### UDP Ping
```apache
ping <ip-address> -P 17 -p <port>
```
Donde `<port>` es el puerto TCP (el número 17 identifica a UDP en el programa)

### Traceroute
```apache
trace <ip-address>
```

### Display the ARP cache
```apache
show arp
```



