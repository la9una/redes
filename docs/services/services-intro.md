# Servicios de red

En una red de datos formada por dispositivos diversos, existen aquellos -también aplicaciones o programas- que ofrecen servicios y, por lo mismo se los denomina "servidores", al resto de los nodos de la red que hacen las veces de clientes.

<div align="center"><img src="../imgServices/network_services.svg" alt="Servicios de red"  width="70%" height="30%"></div>


Existen diferentes tipos de servicios de red. Entre los más comunes se encuentran los servidores:

* de archivos
* de aplicaciones
* de almacenamiento de datos
* de base de datos
* web
* de resolución de nombres
* de impresión
* de correo
* de autenticación
* proxy
* de monitoreo
* etc 
