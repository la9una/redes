# Información general
site_name: "Instalación, mantenimiento y reparación de redes informáticas"
site_description: "Instalación, mantenimiento y reparación de redes informáticas"
site_author: "Raul Jesus Lopez"
site_url: https://redes.rauljesus.xyz

# Repositorio
repo_name: "Ver en GitLab"
repo_url: "https://gitlab.com/la9una/redes.git"

# Copyright
copyright: "Raul Jesus Lopez"

# Tema
theme:
  name: "material"
  custom_dir: overrides
  language: "es"
  icon:
    logo: fontawesome/solid/graduation-cap
    repo: fontawesome/brands/git-alt
  palette:
    - media: "(prefers-color-scheme: light)"
      scheme: default
      primary: "indigo"
      accent: "red"
      toggle:
        icon: material/lightbulb
        name: Cambiar a modo oscuro
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      primary: "indigo"
      accent: "indigo"
      toggle:
        icon: material/lightbulb-outline
        name: Cambiar a modo claro
  font:
    text: "Roboto"
    code: "Roboto Mono"
  features:
    - announce.dismiss
    - content.code.annotate
    - content.code.copy
    - content.code.select
    - content.tabs.link
    - navigation.instant
    - navigation.tracking
    # - navigation.sections
    # - navigation.expand
    - navigation.tabs
    # - navigation.indexes
    # - navigation.tabs.sticky
    - header.autohide
    - toc.follow
    # - toc.integrate
    - navigation.top
    - search.suggest
    - search.highlight
    - search.share


#hide:
#  - navigation
#  - toc

# Extra
extra:
  #  disqus: 'rauljesus-xyz'
  manifest: manifest.webmanifest
  social:
    - icon: fontawesome/brands/twitter
      link: "https://twitter.com/la9una"
    - icon: fontawesome/brands/facebook-f
      link: "https://www.facebook.com/rauljesuslopez"
    - icon: fontawesome/brands/instagram
      link: "https://www.instagram.com/la9una"
    - icon: fontawesome/brands/gitlab
      repo_url: "https://gitlab.com/la9una/redes.git"
  version:
    provider: mike

# Extensiones
markdown_extensions:
  - toc:
      permalink: true
  - attr_list
  - admonition
  - codehilite
  - tables
  - footnotes
  # MathJax
  - pymdownx.arithmatex:
      generic: true
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.snippets
  - pymdownx.superfences
  - pymdownx.superfences:
      custom_fences:
        - name: vegalite
          class: vegalite
          format: !!python/name:mkdocs_charts_plugin.fences.fence_vegalite
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.tilde
  - pymdownx.keys
  - pymdownx.highlight:
      anchor_linenums: true
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
plugins:
  - search
  - charts
# kroki https://github.com/AVATEAM-IT-SYSTEMHAUS/mkdocs-kroki-plugin
  - kroki
# terminal.js https://github.com/daxartio/termynal
  - termynal
extra_javascript:
  - javascripts/config.js
  - https://unpkg.com/tablesort@5.3.0/dist/tablesort.min.js
  - javascripts/tablesort.js
  # Charts https://github.com/timvink/mkdocs-charts-plugin
  - https://cdn.jsdelivr.net/npm/vega@5
  - https://cdn.jsdelivr.net/npm/vega-lite@5
  - https://cdn.jsdelivr.net/npm/vega-embed@6
  # KaTeX
  #  - javascripts/katex.js
  #  - https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.16.7/katex.min.js
  #  - https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.16.7/contrib/auto-render.min.js
  # MathJax
  - javascripts/mathjax.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
extra_css:
  - stylesheets/extra.css
# Katex
#  - https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.16.7/katex.min.css

####################################################################
######################## MENU DE NAVEGACIÓN ########################
####################################################################

# Páginas
nav:
  - Inicio:
      - Introducción: "index.md"
      - Sobre esta guía: "about.md"
  - Redes:
      - Introducción: "networking/network-intro.md"
      - Fundamentos:
#          - ¿Qué es una red?: "networking/network-about.md"
#          - Tipos y topologías: "networking/network-topologies.md"
#          - Arquitectura:
#              - Modelos: "networking/network-architecture.md"
#              - Estándares y Protocolos: "networking/network-protocols.md"
#              - Capa Física: "networking/network-layer-physical.md"
            - Capa de Enlace de datos:
#                  - Fundamentos: "networking/network-layer-link.md"
                   - VLANs: "networking/network-vlans.md"
            - Capa de Red:
#                  - Fundamentos: "networking/network-layer-network.md"
                   - Direccionamiento: "networking/network-addressing.md"
                   - Enrutamiento: "networking/network-routing.md"
                   - NAT: "networking/network-nat.md"
                   - Seguridad: "networking/network-security.md"
#              - Capa Transporte: "networking/network-layer-transport.md"
#              - Capa de Aplicación: "networking/network-layer-aplication.md"
      - Dispositivos intermedios:
          - Cisco:
              - Acerca de: "cisco/cisco-about.md"
              - Gestión: "cisco/cisco-management.md"
              - Router: "cisco/cisco-router.md"
              - Switch:
                  - VLAN:
                    - Access VLAN: "cisco/cisco-vlans-access.md"
                    - Trunk VLAN: "cisco/cisco-vlans-trunk.md"
                    - Inter-VLAN Routing: "cisco/cisco-vlans-routing.md"
                    - Comando show: "cisco/cisco-vlans-show.md"
                    - Gestionando cambios: "cisco/cisco-vlans-changes.md"
                    - Ejercicios: "cisco/cisco-vlans-exercises.md"
          - Miktotik:
              - Acerca de: "mikrotik/mikrotik-about.md"
              - Gestión: "mikrotik/mikrotik-management.md"
              - Router: "mikrotik/mikrotik-router.md"
      - Dispositivos finales:
          - Linux: "networking/network-interfaces-linux.md"
          - Windows: 
              - Interfaces: "networking/network-interfaces-windows.md"
              - Firewall: "networking/network-firewall-windows.md"
          - VPCS: "networking/network-vpcs.md"
  - Servicios de red:
      - Introducción: "services/services-intro.md"
      - GNU/Linux:
          - Introducción al SO: 
              - Historia: "gnuLinux/basics-history.md"
              - GUI vs CLI: "gnuLinux/basics-desktop-terminal.md"
              - Comandos básicos:
                  - Archivos y carpetas: "gnuLinux/basics-filesystem.md"
                  - Compresión y descompresión: "gnuLinux/basics-compression.md"
                  - El editor nano: "gnuLinux/basics-nano.md"
                  - El editor vim: "gnuLinux/basics-vim.md"
                  - Búsquedas: "gnuLinux/basics-search.md"
                  - Bash scripting: "gnuLinux/basics-bash-scripting.md"
                  - Tips: "gnuLinux/basics-tips.md"
              - Gestión de software:
                  - Repositorios: "gnuLinux/software-repositories.md"
                  - Paquetes: "gnuLinux/software-packages.md"
              - Sistema de permisos:
                  - Usuarios: "gnuLinux/access-users.md"
                  - Grupos: "gnuLinux/access-groups.md"
                  - Permisos: "gnuLinux/access-permissions.md"
          - Servidor web:
              - LAMP:
                  - ¿Qué es LAMP?: "lamp/index.md"
                  - Instalación: "lamp/lamp-install.md"
                  - Gestión básica: "lamp/lamp-basics.md"
                  - Web de usuario: "lamp/lamp-userdir.md"
                  - Hosts virtuales: "lamp/lamp-virtualhosts.md"
                  - Ejercicios: "lamp/lamp-exercises.md"
          - Servidor de nombres:
              - Introducción: "dns/index.md"
              - El archivo hosts: "dns/dns-hosts.md"
              - BIND:
                  - ¿Qué es BIND?: "dns/bind/index.md"
                  - Instalación: "dns/bind/bind-install.md"
                  - DNS caché:
                      - Teoría: "dns/bind/bind-cache.md"
                      - Ejemplo práctico: "dns/bind/bind-cache-example.md"
                  - DNS local:
                      - Archivos de zona: "dns/bind/bind-zones.md"
                      - Ejemplo - Zonas: "dns/bind/bind-zones-example.md"
                      - Ejemplo - Zonas múltiples: "dns/bind/bind-zones-example-multiple.md"
              - Configurando clientes: "dns/dns-clients.md"
              - Consultas DNS: "dns/dns-query.md"
          - Servidor de compartición:
              - ¿Qué es SAMBA?: "samba/index.md"
              - Instalación: "samba/samba-install.md"
              - Gestión de usuarios: "samba/samba-manage-users.md"
              - Carpeta de usuario: "samba/samba-share-user.md"
              - Carpeta de grupo: "samba/samba-share-group.md"
              - Carpeta de invitados: "samba/samba-share-guest.md"
              - Clientes Windows: "samba/samba-windows-client.md"
              - Ejercicios: "samba/samba-exercises.md"
  - Monitoreo de redes:
      - Introducción: "monitoring/monitoring-intro.md"
      - Nagios:
          - ¿Qué es Nagios?: "nagios/index.md"
          - Instalación: "nagios/nagios-install.md"
          - Configuración inicial: "nagios/nagios-configuration.md"
          - Definiendo grupos: "nagios/nagios-groups.md"
          - Definiendo servicios: "nagios/nagios-services.md"
          - Definiendo equipos: "nagios/nagios-hosts.md"
          - Agente Windows: "nagios/nagios-agent.md"
          - Definiendo íconos: "nagios/nagios-icons.md"
          - Ejercicios: "nagios/nagios-exercises.md"
          - Recursos adicionales: "nagios/nagios-resources.md"
  - DevOps:
      - Introducción: "devops/devops-intro.md"
      - Automatización:
          - Introducción: "automation/index.md"
          - Netmiko:
              - ¿Qué es netmiko?: "automation/netmiko-introduction.md"
              - Ejemplo de uso: "automation/netmiko-examples.md"
      - Contenerización:
          - Introducción: "containers/index.md"
          - Docker:
              - ¿Qué es Docker?: "containers/docker-introduction.md"
              - Instalación: "containers/docker-install.md"
              - Imágenes: "containers/docker-images.md"
              - Contenedores: "containers/docker-containers.md"
              - Docker Compose: "containers/docker-compose.md"
              - Comandos útiles: "containers/docker-utils.md"
      - Control de versiones:
          - ¿Qué es git?: "git/git-index.md"
          - Instalación: "git/git-install.md"
          - Configuración: "git/git-configure.md"
          - Fundamentos: "git/git-fundamentals.md"
          - Deshaciendo cambios: "git/git-undo.md"
          - Orígenes remotos: "git/git-remote.md"
          - Ramas: "git/git-branches.md"
          - Etiquetado: "git/git-tags.md"
          - Saber más: "git/git-resources.md"
  - Contacto: contact.md

